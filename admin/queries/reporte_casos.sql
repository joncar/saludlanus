DROP VIEW IF EXISTS reporte_casos;
CREATE VIEW reporte_casos AS SELECT 
'País' AS tipo,
IFNULL(MIN(fecha),'No registrado') AS primer_caso_confirmado,
IFNULL(SUM(serios),0) AS casos_serios,
IFNULL((SUM(fallecidos)*100)/SUM(confirmados),0) AS tasa_letalidad,
IFNULL(SUM(confirmados),0) AS infectados,
IFNULL(SUM(curados),0) as curados
FROM 
datos_epidemiologicos
WHERE tipo = 1 AND confirmados > 0
UNION ALL 
SELECT 
'PBA' AS tipo,
IFNULL(MIN(fecha),'No registrado') AS primer_caso_confirmado,
IFNULL(SUM(serios),0) AS casos_serios,
IFNULL((SUM(fallecidos)*100)/SUM(confirmados),0) AS tasa_letalidad,
IFNULL(SUM(confirmados),0) AS infectados,
IFNULL(SUM(curados),0) as curados
FROM 
datos_epidemiologicos
WHERE tipo = 2 AND confirmados > 0
UNION ALL 
SELECT 
'Lanús',
IFNULL(MIN(fecha),'No registrado') AS primer_caso_confirmado,
IFNULL(SUM(serios),0) AS casos_serios,
IFNULL((SUM(fallecidos)*100)/SUM(confirmados),0) AS tasa_letalidad,
IFNULL(SUM(confirmados),0) AS infectados,
IFNULL(SUM(curados),0) as curados
FROM 
datos_epidemiologicos
WHERE tipo = 3 AND confirmados > 0