DROP VIEW IF EXISTS pacientes;
CREATE VIEW pacientes AS SELECT 
id,
nombre,
apellido,
email,
riesgo(id) as riesgo
FROM user WHERE admin = 0