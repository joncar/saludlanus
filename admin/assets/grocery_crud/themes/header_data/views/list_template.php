<?php if(!empty($list)): ?>

    <div class="kt-portlet">
        <div class="kt-portlet__head">
            <div class="kt-portlet__head-label">
                <h1 class="kt-portlet__head-title">
                    <b>Datos de <?= $subject ?></b>
                     <a href="<?= str_replace('ajax_list','',$ajax_list_url) ?>" style="position:absolute; right: 28px; top: 10px;"><i class="fa fa-edit"></i> Cambiar</a>
                </h1>
            </div>
        </div>
        <div class="kt-portlet__body">
            <div class="kt-section">
                <form class="">  
                    <div class="row">
                        <?php foreach ($columns as $column): ?>
                            <div class="form-group col-12 col-sm-3">
                              <label for="id"><?= $column->display_as ?>: </label>
                              <input type="text" class="form-control" id="<?= $column->field_name ?>" value="<?= strip_tags($list[0]->{$column->field_name}) ?>" readonly="">
                            </div>
                        <?php endforeach ?>
                    </div>   
                </form>
            </div>
        </div>
    </div>
<?php else: ?>
Sin datos para mostrar
<?php endif ?>
