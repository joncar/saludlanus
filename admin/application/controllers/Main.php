<?php

if (!defined('BASEPATH'))
    exit('No direct script access allowed');
session_save_path('/tmp');
ob_start();
session_name('siaweb');
session_start();

class Main extends CI_Controller {
     protected $types_lang = array(
        'es'=>'spanish',
        'en'=>'english',
        'fr'=>'french',
        'ru'=>'russian',
        'it'=>'italian',
        'ca'=>'catalan',
        'ch'=>'zh_cn',
        'al'=>'german'
    );
    public $class_lang = array(
        'es'=>array('Español','spain'),
        'en'=>array('English','ingles'),
        'fr'=>array('Frances','frances'),
        'ru'=>array('Pусский','ruso'),
        'it'=>array('Italiano','italiano'),
        'ca'=>array('Catalá','catalunya'),
        'ch'=>array('中国','chino'),
        'al'=>array('Deutsch','aleman')
    );
    //public $theme = 'themes/bristol/';
    public $theme = '';
    //Insert body css & js
    public $js = [];
    public $css = [];
    //Insert head css & js
    public $hjs = [];
    public $hcss = [];
    public function __construct() {
        parent::__construct();
        $this->load->helper('url');
        $this->load->helper('html');
        $this->load->helper('h');
        $this->load->database();
        $this->load->model('user');
        $this->load->model('bdsource');
        $this->load->model('querys');
        $this->load->model('elements');
        $this->load->library('redsysAPI');
        $this->load->library('traduccion');
        $this->load->model('carrito');
        date_default_timezone_set('Europe/Madrid');
        
        //$this->theme = $_SESSION['lang'].'/';
        if(empty($_SESSION['lang'])){
            $_SESSION['lang'] = 'es';
        }
        if(file_exists(APPPATH.'language/'.$this->types_lang[$_SESSION['lang']].'/web_lang.php')){
            $this->lang->load('web',$this->types_lang[$_SESSION['lang']]);
            $this->lang->load('form_contact',$this->types_lang[$_SESSION['lang']]);
            $this->lang->load('form_validation',$this->types_lang[$_SESSION['lang']]);
            $_SESSION['default_lang'] = $this->types_lang[$_SESSION['lang']];
        }else{
            $this->lang->load('web','spanish');  
            $this->lang->load('form_contact','spanish');
            $this->lang->load('form_validation','spanish');
            $_SESSION['default_lang'] = 'spanish';          
        }       
        $this->theme = 'es/';
        $this->ajustes = $this->db->get('ajustes')->row();
        $this->ajustes->logo_login = $this->ajustes->logo;
        $this->ajustes->titulo_sistema = $this->ajustes->titulo;
    }
    
    
    
    function get_entries(){
        $blog = new Bdsource();
        $blog->limit = array('3','0');
        $blog->order_by = array('fecha','DESC');        
        $blog->init('blog');
        foreach($this->blog->result() as $n=>$b){
            $this->blog->row($n)->link = site_url('blog/'.toURL($b->id.'-'.$b->titulo));
            $this->blog->row($n)->foto = base_url('img/blog/'.$b->foto);
            $this->blog->row($n)->comentarios = $this->db->get_where('comentarios',array('blog_id'=>$b->id))->num_rows();                
            $this->blog->row($n)->categorias = $this->db->get_where('blog_categorias',array('id'=>$b->blog_categorias_id));
        }
        if($this->blog->num_rows()>0){
            $this->blog->tags = $this->blog->row()->tags;
        }
    }

    public function index() {
        $log = $this->user;
        if ($log->log)
            redirect (base_url('panel'));
        else
            $this->loadView('main');
    }

    public function success($msj) {
        return '<div class="alert alert-success">' . $msj . '</div>';
    }

    public function error($msj) {
        return '<div class="alert alert-danger">' . $msj . '</div>';
    }

    public function login() {
        if (!$this->user->log) {
            if (!empty($_POST['email']) && !empty($_POST['pass'])) {
                $this->db->where('email', $this->input->post('email'));
                $r = $this->db->get('user');
                if ($this->user->login($this->input->post('email', TRUE), $this->input->post('pass', TRUE))) {
                    if ($r->num_rows() > 0 && $r->row()->status == 1) {
                        if (!empty($_POST['remember']))
                            $_SESSION['remember'] = 1;
                        if (empty($_POST['redirect']))
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . site_url('panel') . '"</script>');
                        else
                            echo $this->success('Usuario logueado correctamente por favor espere...! <script>document.location.href="' . $_POST['redirect'] . '"</script>');
                    } else
                        $_SESSION['msj'] = $this->error('El usuario se encuentra bloqueado, comuniquese con un administrador para solucionar su problema');
                } else
                    $_SESSION['msj'] = $this->error('Usuario o contrasena incorrecta, intente de nuevo.');
            } else
                $_SESSION['msj'] = $this->error('Debe completar todos los campos antes de continuar');

            if (!empty($_SESSION['msj']))
                header("Location:" . base_url('panel'));
        } else
            header("Location:" . base_url('panel'));
    }

    public function unlog() {
        $this->user->unlog();
        header("Location:" . site_url());
    }

    function getHead($page){
        $ajustes = $this->ajustes;
        //$stocookie = '<link href="'.base_url('js/stocookie/stoCookie.css').'">';        
        $stocookie = '';
        //$page = str_replace('</head>',$stocookie.'</head>',$page);
        return $page;
    }

    function getBody($page){
        $ajustes = $this->ajustes;        
        //$stocookie = '<script src="'.base_url('js/stocookie/stoCookie.min.js').'"></script>';
        //$stocookie.= html_entity_decode($ajustes->cookies);
        //$stocookie = str_replace('{{cookiesbar}}',l('cookiesbar'),$stocookie);
        //$stocookie = str_replace('{{cookiesbtn}}',l('cookiesbtn'),$stocookie);
        //$page = str_replace('</body>',$stocookie.'</body>',$page);
        $page= str_replace('</body>',$ajustes->analytics.'</body>',$page);
        return $page;
    }

    public function loadView($param = array('view' => 'main')) {

        if (is_string($param)){
            $param = array('view' => $param);
        }
        if(is_object($param)){
            $param = (array)$param;
        }
        $ajustes = $this->ajustes;
        if(empty($param['title'])){
            $param['title'] = $ajustes->titulo_sistema;
        }
        $param['favicon'] = $ajustes->favicon;         
        $param['view'] = $this->load->view($param['view'],$param,TRUE);
        $param = array_merge($param,['css'=>$this->css,'js'=>$this->js,'hcss'=>$this->hcss,'hjs'=>$this->hjs]);        
        if(!empty($param['css_files'])){
            foreach($param['css_files'] as $p){
                $param['hcss'][] = '<link rel="stylesheet" href="'.$p.'" type="text/css">';
            }
        }
        if(!empty($param['js_files'])){
            foreach($param['js_files'] as $p){
                $param['js'][] = '<script src="'.$p.'"></script>';
            }        
        }
        
        $page = $this->load->view('template', $param,true);        
        $page = $this->getHead($page);
        $page = $this->getBody($page); 
        echo $page;
    }

    public function loadViewAjax($view, $data = null) {
        $view = $this->valid_rules($view);
        $this->load->view($view, $data);
    }

    function error404() {
        $this->loadView(array('view' => 'errors/403'));
    }

    public function traduccion($idioma = 'ca'){        
        $_SESSION['lang'] = $idioma;        
        if(empty($_GET['url'])){
            redirect($_SERVER['HTTP_REFERER']);
        }else{
            redirect($_GET['url']);
        }
    }
    
    function enviarcorreo($usuario,$idnotificacion,$destinatario = ''){
            $mensaje = $this->db->get_where('notificaciones',array('id'=>$idnotificacion))->row();
            $mensaje->texto = str_replace('usuarios.','',$mensaje->texto);
            $mensaje->titulo = str_replace('usuarios.','',$mensaje->titulo);
            foreach($usuario as $n=>$v){             
             $mensaje->texto = str_replace('{'.$n.'}',$v,$mensaje->texto);
             $mensaje->titulo = str_replace('{'.$n.'}',$v,$mensaje->titulo);   
            }            
            if(empty($destinatario)){
                correo($usuario->email,$mensaje->titulo,$mensaje->texto);
            }
            else{
                correo($destinatario,$mensaje->titulo,$mensaje->texto);
            }
        }

        function testMail(){
            correo('joncar.c@gmail.com','TEST','TEST');
        }

}
