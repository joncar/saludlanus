<!Doctype html>
<html lang="es">
    <head>       
        <title><?= empty($title)?'Nomina':$title.'' ?></title>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1.0">
        <meta http-equiv="X-UA-Compatible" content="IE=edge">
        <link rel="stylesheet" href="<?php echo base_url() ?>css/style.css">
        <link href="<?php echo base_url() ?>css/style.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>assets/plugins/global/plugins.bundle.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/style.bundle.css?v=1" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/skins/header/base/light.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/skins/header/menu/light.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/skins/brand/dark.css" rel="stylesheet" type="text/css" />
        <link href="<?php echo base_url() ?>css/skins/aside/dark.css" rel="stylesheet" type="text/css" />
        <link rel="shortcut icon" href="<?php echo base_url() ?>img/logos/<?= $favicon ?>" />
        <script>window.URI = '<?= base_url() ?>'; window.wURL = window.URL; window.afterLoad = [];</script>
        <?php foreach($hcss as $cs): ?>
            <?php echo $cs ?>
        <?php endforeach ?>
        <?php foreach($hjs as $j): ?>
            <?php echo $j ?>
        <?php endforeach ?>
    </head>  
    <!-- begin::Body -->
    <body class="kt-quick-panel--right kt-demo-panel--right kt-offcanvas-panel--right kt-header--fixed kt-header-mobile--fixed kt-subheader--enabled kt-subheader--fixed kt-subheader--solid kt-aside--enabled kt-aside--fixed kt-page--loading">

        <?php echo $view ?>

        <!-- begin::Global Config(global config for global JS sciprts) -->
        <script>
            var KTAppOptions = {
                "colors": {
                    "state": {
                        "brand": "#5d78ff",
                        "dark": "#282a3c",
                        "light": "#ffffff",
                        "primary": "#5867dd",
                        "success": "#34bfa3",
                        "info": "#36a3f7",
                        "warning": "#ffb822",
                        "danger": "#fd3995"
                    },
                    "base": {
                        "label": [
                            "#c5cbe3",
                            "#a1a8c3",
                            "#3d4465",
                            "#3e4466"
                        ],
                        "shape": [
                            "#f0f3ff",
                            "#d9dffa",
                            "#afb4d4",
                            "#646c9a"
                        ]
                    }
                }
            };
        </script>

        <!-- end::Global Config -->

        <!--begin::Global Theme Bundle(used by all pages) -->
        <script src="<?= base_url() ?>assets/plugins/global/plugins.bundle.min.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/scripts.bundle.js" type="text/javascript"></script>
        <script src="<?= base_url() ?>js/frame.js" type="text/javascript"></script>
        <?php foreach($css as $cs): ?>
            <?php echo $cs ?>
        <?php endforeach ?>
        <?php foreach($js as $j): ?>
            <?php echo $j ?>
        <?php endforeach ?>
        <script src="<?= base_url().'js/jquery-migrate.min.js' ?>"></script>
        <script>
            $(document).ready(function(){
                doAfterLoad();
            });

            function doAfterLoad(){
                for(var i in window.afterLoad){
                    window.afterLoad[i]();
                }
            }
        </script>
        <script>
            var sec = 0;
            var time = <?php echo time()*1000; ?>;
            var fecha;
            var meses = ['Ene','Feb','Mar','Abr','May','Jun','Jul','Ago','Sep','Oct','Nov','Dic'];
            function updateReloj(){
                time = time+1000;
                fecha = new Date();
                fecha.setTime(time);
                var min = fecha.getMinutes();
                min = min<10?('0'+min):min;
                $('#fechaSistema').html(fecha.getDate()+' '+meses[fecha.getMonth()]+' '+fecha.getFullYear()+' '+fecha.getHours()+':'+min+':'+fecha.getSeconds());
                setTimeout(updateReloj,1000);
            }
            updateReloj();
        </script>     
    </body>
</html>           