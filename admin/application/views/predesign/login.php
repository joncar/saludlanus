<?php if (empty($_SESSION['user'])): ?>

    <?php if (!empty($msj)) echo $msj ?>

    <?php if (!empty($_SESSION['msj'])) echo $_SESSION['msj'] ?>

    <form role="form" class="kt-form" action="<?= base_url('registro/login') ?>" method="post">
        <div class="response"></div>
        <div class="input-group">
            <input class="form-control"  type="email" autocomplete="off" placeholder="Email" name="email" /> 
        </div>
        <div class="input-group">
            <input class="form-control" type="password" type="password" autocomplete="off" placeholder="Contraseña" name="pass" /> 
        </div>
        <div class="row kt-login__extra">
            <div class="col">
                <label class="kt-checkbox">
                    <input type="checkbox" name="remember"> Recuerdame
                    <span></span>
                </label>
            </div>
            <div class="col kt-align-right">
                <a href="javascript:;" id="kt_login_forgot" class="kt-link kt-login__link">Olvidaste tu contraseña?</a>
            </div>
        </div>
        <div class="kt-login__actions">
            <button type="submit" id="kt_login_signin_submit" class="btn btn-pill kt-login__btn-primary">Entrar</button>
        </div>
    </form>
<?php else: ?>
    <div align="center"><a href="<?= base_url('panel') ?>" class="btn btn-success btn-large" style=" width: auto; padding-top: 20px">Entrar en el sistema</a></div>
<?php endif; ?>
<?php $_SESSION['msj'] = null ?>


