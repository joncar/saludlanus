<?php if($this->user->log): ?>
<!-- begin:: Content Head -->
<div class="kt-subheader   kt-grid__item" id="kt_subheader">
	<div class="kt-container  kt-container--fluid ">
		<div class="kt-subheader__main">
			<h3 class="kt-subheader__title">
				<?= empty($title)?'Escritorio':$title ?>
			</h3>
			<span class="kt-subheader__separator kt-hidden"></span>
			<div class="kt-subheader__breadcrumbs">
				<a href="<?= base_url('panel') ?>" class="kt-subheader__breadcrumbs-home"><i class="flaticon2-shelter"></i></a>		
				<span class="kt-subheader__breadcrumbs-separator"></span>		
				<!--<span class="kt-subheader__breadcrumbs-link kt-subheader__breadcrumbs-link--active"></span>-->
			</div>
		</div>
	</div>
</div>
<!-- end:: Content Head -->
<?php endif ?>