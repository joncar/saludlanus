<?php $this->load->view('includes/header'); ?>
    <div class="kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor kt-wrapper" id="kt_wrapper">
        <?php $this->load->view('includes/topbar'); ?>
            <div class="kt-content  kt-grid__item kt-grid__item--fluid kt-grid kt-grid--hor" id="kt_content">
                <?php $this->load->view('includes/breadcum'); ?>
                        <!-- begin:: Content -->
                        <div class="kt-container  kt-container--fluid  kt-grid__item kt-grid__item--fluid">

                            <!--Begin::Dashboard 1-->

                             <?php
                                if(empty($crud)):
                                //$dashboard = $this->user->pagina_principal;
                                $dashboard = empty($dashboard)?'includes/dashboard':$dashboard;
                                $dashboard = $this->load->view($dashboard,array(),TRUE,'dashboards');                                
                                endif;
                            ?>
                            <?= empty($crud) ? $dashboard: $this->load->view('cruds/' . $crud) ?>   

                            <!--End::Dashboard 1-->
                        </div>

                        <!-- end:: Content -->
                    </div>

                    <!-- begin:: Footer -->
                    <?php $this->load->view('includes/footer'); ?>
                    <!-- end:: Footer -->
                </div>
            </div>
        </div>

        <!-- end:: Page -->

        <!-- begin::Scrolltop -->
        <div id="kt_scrolltop" class="kt-scrolltop">
            <i class="fa fa-arrow-up"></i>
        </div>

        <!-- end::Scrolltop -->

<?php     
    if(empty($crud)):
    get_instance()->js[] = '
        <script src="'.base_url().'assets/plugins/custom/fullcalendar/fullcalendar.bundle.js" type="text/javascript"></script>
        <script src="'.base_url().'js/pages/dashboard.js" type="text/javascript"></script>';
    endif;
?>