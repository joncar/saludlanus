<?php
	$_GET['page'] = empty($_GET['page'])?1:$_GET['page'];
	$ant = $_GET['page']==1?base_url('blog').'?page=1':base_url('blog').'?page='.($_GET['page']-1);
	$next = $_GET['page']==1?base_url('blog').'?page=2':base_url('blog').'?page='.($_GET['page']+1);
?>
<nav aria-label="Page navigation" class="mt-5">
    <ul class="pagination justify-content-center">
        <?php if($current_page>1): ?>
            <li class="page-item">
                <a class="page-link" href="<?= $ant ?>" aria-label="Previous">
                    <i class="ti-arrow-left"></i>
                    <span class="sr-only">Previous</span>
              </a>
            </li>
        <?php endif ?>
        <?php for($i=1;$i<=$total_pages;$i++): ?>		   
		  	<li class="page-item"><a class="page-link <?= $i==$current_page?'active':'' ?>" href="<?= base_url('blog') ?>?page=<?= $i ?>"><?= $i ?></a></li>
		<?php endfor ?>
        <?php if($total_pages > 1 || $current_page!=$total_pages): ?>
            <li class="page-item">
                <a class="page-link" href="<?= $next ?>" aria-label="Next">
                    <i class="ti-arrow-right"></i>
                    <span class="sr-only">Next</span>
                </a>
            </li>
        <?php endif ?>
    </ul>
</nav>