<div class="kt-portlet">
	<div class="kt-portlet__head">
		<div class="kt-portlet__head-label">
			<h1 class="kt-portlet__head-title">
				<b>Datos generales</b>
			</h1>
		</div>
	</div>
	<div class="kt-portlet__body">
		<div class="kt-section">
<a href="#enviarmensaje" data-toggle="modal" class="btn btn-info">Enviar mensaje</a>
<?php echo $output ?>

<div id="enviarmensaje" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <div class="modal-content">
	  <form action="administracion/sendMessage" onsubmit="sendForm(this,'#respuesta'); return false;" class="form-horizontal">
	  	  <div id="respuesta"></div>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title">Enviar Mensaje a este usuario</h4>
	      </div>
	      <div class="modal-body">
	      	<div class="form-group">
	        	<label for="inputEmail3" class="col-sm-2 control-label">Título del mensaje</label>
			    <div class="col-sm-10">
			      <input type="text" name="titulo" placeholder="Título del mensaje" class="form-control">
			    </div>			    
			</div>
	        <div class="form-group">
	        	<label for="inputEmail3" class="col-sm-2 control-label">Mensaje</label>
			    <div class="col-sm-10">
			      <textarea name="mensaje" id="mensaje" cols="30" rows="10" placeholder="Escribe aquí el mensaje" class="form-control"></textarea>
			    </div>			    
			</div>
			<input type="hidden" name="user_id" value="<?php echo $user ?>">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default" data-dismiss="modal">Cancelar</button>
	        <button type="submit" class="btn btn-primary">Enviar</button>
	      </div>
	  </form>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
</div>
</div>
</div>