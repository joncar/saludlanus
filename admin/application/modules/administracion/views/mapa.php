<div id="mapa" style="width:100%; height:400px;"></div>
<?php get_instance()->js[] = '<script src="https://maps.google.com/maps/api/js?key=AIzaSyDyPEFmiS1aSSx_fpoB5US78NBVI2pmRjc&libraries=places"></script>
<script src="'.base_url('assets/grocery_crud/js/jquery_plugins/config/map.js').'"></script>'; ?>
<script>
	window.afterLoad.push(function(){
		window.pacientes = <?php 		
			$pac = [];
			$this->db->select("usermeta.*, CONCAT(user.nombre,' ',user.apellido) as paciente, riesgo(user.id) as riesgo",FALSE);
			$this->db->join('user','user.id = usermeta.user_id');
			$pacientes = $this->db->get_where('usermeta',['meta_key'=>'ubicacion']);
			foreach($pacientes->result() as $p){
				$pac[] = array_merge(explode(',',$p->meta_value),[$p->paciente,$p->user_id,$p->riesgo]);
			}
			echo json_encode($pac);
		?>;
		$(document).on('shown.bs.tab',"a[href='#pills-profile']",function(){		
			window.map = new mapa('mapa','-34.7021091','-58.4252566');
			map.initialize();
			map.marker.setMap(null);
			for(var i in pacientes){
				map.marker[i] = new google.maps.Marker({
			        position: new google.maps.LatLng(pacientes[i][0],pacientes[i][1]),
			        map: map.map,   		        
			        title: pacientes[i][2],
			        icon:{
			        	url: pacientes[i][4]=='1'?"http://maps.google.com/mapfiles/ms/icons/red-dot.png":"http://maps.google.com/mapfiles/ms/icons/blue-dot.png"
			        }
		    	});    
		    	map.marker[i].link = '<?= base_url() ?>administracion/usermeta/'+pacientes[i][3];
		    	map.marker[i].addListener('click', function(e) {	    		
			      if(confirm('¿Deseas ver los datos de  este paciente?')){
			      	document.location.href=map.marker[i].link;
			      }
			    });
			}
		});	
	});
</script>