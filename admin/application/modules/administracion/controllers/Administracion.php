<?php 
require_once APPPATH.'controllers/Panel.php'; 
class Administracion extends Panel{
	function __construct(){
		parent::__construct();
	}

	function pacientes(){
		$crud = $this->crud_function('','');
		$crud->set_primary_key('id');
		$crud->unset_add()->unset_edit()->unset_delete()->unset_read();		
		$crud->add_action('Ver datos','',base_url('administracion/usermeta').'/');
		$crud->where('id !=',6);
		$crud->field_type('riesgo','true_false',[0=>'Fuera de riesgo',1=>'<span class="label label-danger">En riesgo de infección</span>']);
		if($crud->getParameters()=='export' || $crud->getParameters()=='print'){
			$columns = ['id','nombre','apellido','email'];
			$metas = ['temperatura','tos_dolor_garganta','respiracion','mayor_de_60','embarazada','cancer','diabetes','enfermedad_hepatica','enfermedad_renal','enfermedad_respiratoria','enfermedad_cardiologica','edad','telefono','direccion','localidad'];
			$crud->callback_column('temperatura',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'temperatura')[0]['meta_value']);});
			$crud->callback_column('tos_dolor_garganta',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'tos_dolor_garganta')[0]['meta_value']);});
			$crud->callback_column('respiracion',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'respiracion')[0]['meta_value']);});
			$crud->callback_column('mayor_de_60',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'mayor_de_60')[0]['meta_value']);});
			$crud->callback_column('embarazada',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'embarazada')[0]['meta_value']);});
			$crud->callback_column('cancer',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'cancer')[0]['meta_value']);});
			$crud->callback_column('diabetes',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'diabetes')[0]['meta_value']);});
			$crud->callback_column('enfermedad_hepatica',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'enfermedad_hepatica')[0]['meta_value']);});
			$crud->callback_column('enfermedad_renal',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'enfermedad_renal')[0]['meta_value']);});
			$crud->callback_column('enfermedad_respiratoria',function($val,$row){return get_usermeta($row->id,'enfermedad_respiratoria')[0]['meta_value'];});
			$crud->callback_column('enfermedad_cardiologica',function($val,$row){return get_usermeta($row->id,'enfermedad_cardiologica')[0]['meta_value'];});
			$crud->callback_column('edad',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'edad')[0]['meta_value']);});
			$crud->callback_column('telefono',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'telefono')[0]['meta_value']);});
			$crud->callback_column('direccion',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'direccion')[0]['meta_value']);});
			$crud->callback_column('localidad',function($val,$row){return get_instance()->getTrueFalse(get_usermeta($row->id,'localidad')[0]['meta_value']);});

			$crud->columns(array_merge($columns,$metas));
		}
		$crud = $crud->render();		
		$crud->output = $this->load->view('pacientes',['output'=>$crud->output],TRUE);
		$this->loadView($crud);
	}

	function getTrueFalse($val){
		if(is_numeric($val) && ($val==1 || $val == 0)){
			return $val==1?'SI':'NO';
		}
		return (string)$val;
	}

	function quedateencasa($x = '',$y = '',$z = '',$r = ''){		
		$crud = $this->crud_function('','');
		if($crud->getParameters()!='list'){
			redirect('administracion/posts/quedateencasa/'.$x.'/'.$y);
			die();
		}
		$crud->set_primary_key('id');
		$crud->unset_print()->unset_export();

		$crud = $crud->render();
		$this->loadView($crud);
	}

	function mensajes(){
		$crud = $this->crud_function('','');
		$crud->field_type('estado','true_false',[0=>'Pendiente',1=>'Entregado']);	
		if($crud->getParameters()=='list'){
			$crud->field_type('estado','true_false',[0=>'<span class="label label-danger">Pendiente</span>',1=>'Entregado']);	
		}	
		$crud = $crud->render();
		$this->loadView($crud);
	}

	function noticias($x = '',$y = '',$z = '',$r = ''){		
		$crud = $this->crud_function('','');
		if($crud->getParameters()!='list'){
			redirect('administracion/posts/noticias/'.$x.'/'.$y);
			die();
		}
		$crud->set_primary_key('id');
		$crud->unset_print()->unset_export();

		$crud = $crud->render();
		$this->loadView($crud);
	}

	function consejos($x = '',$y = '',$z = '',$r = ''){		
		$crud = $this->crud_function('','');
		if($crud->getParameters()!='list'){
			redirect('administracion/posts/consejos/'.$x.'/'.$y);
			die();
		}
		$crud->set_primary_key('id');
		$crud->unset_print()->unset_export();

		$crud = $crud->render();
		$this->loadView($crud);
	}

	function posts($type = '',$x = '',$y = ''){
		$crud = $this->crud_function('','');
		if(!empty($type)){
			$crud->field_type('post_type','hidden',$type)
			 	 ->where('post_type',$type);

			 if($crud->getParameters()=='list'){
			 	redirect('administracion/'.$type);
			 }
		}

		if($type=='consejos'){
			$crud->field_type('post_content','image',['path'=>'img/app','width'=>'600','height'=>'600']);
			$crud->required_fields('post_title','post_date');
		}

		if($type=='noticias'){
			if($crud->getParameters(false)=='add' || $crud->getParameters(false)=='edit' || $crud->getParameters(false)=='cropper'){
				$crud->fields('post_title','meta[imagen]','meta[descripcion_corta]','post_content','meta[youtube]','post_date','post_type','post_author');
			}
			if(is_numeric($y)){
				get_instance()->y = $y;
				$default = get_postmeta($y,'imagen');
				$default = count($default)>0?$default[0]['meta_value']:'';
				if(is_json($default)){
					$default = json_decode($default);
					$default = $default->file;
				}
			}else{
				get_instance()->y = '';
				$default = '';
			}

			$crud->field_type('meta[imagen]','image',['path'=>'img/app','width'=>'500px','height'=>'500px','default'=>$default]);

			$crud->callback_field('meta[descripcion_corta]',function($val){
				$y = get_instance()->y;
				$val = get_postmeta($y,'descripcion_corta');				
				$val = count($val)>0?$val[0]['meta_value']:'';				
				return '<textarea id="field-meta[descripcion_corta]" name="meta[descripcion_corta]" class="texteditor" aria-hidden="true">'.$val.'</textarea>';
			});
			$crud->callback_field('meta[youtube]',function($val){
				$y = get_instance()->y;
				$val = get_postmeta($y,'youtube');				
				$val = count($val)>0?$val[0]['meta_value']:'';				
				return '<input type="text" id="field-meta[youtube]" name="meta[youtube]" aria-hidden="true" value="'.$val.'">';
			});
		}
		$crud->callback_after_insert([$this,'addPostMetas']);
		$crud->callback_after_update([$this,'addPostMetas']);
		$crud->field_type('post_author','hidden',$this->user->id);
		$crud->set_primary_key('id');
		$crud = $crud->render();
		$this->loadView($crud);
	}

	function addPostMetas($post,$primary){
		if(empty($_POST['meta'])){
			return;
		}
		foreach($_POST['meta'] as $n=>$p){
			$existe = count(get_postmeta($primary,$n))>0;
            if(!$existe){
                add_postmeta($primary,$n,$p);
            }else{
                update_postmeta($primary,$n,$p);
            }
		}
	}

	function usermeta($x = ''){
		$crud = $this->crud_function('','');	
		$crud->callback_column('meta_key',function($val,$row){			
			$val = str_replace('_',' ',$val);
			$val = ucfirst($val);
			return $val;
		});
		$crud->callback_column('meta_value',function($val,$row){
			$val = $val=='0'?'<span class="label label-default">NO</span>':$val;
			$val = $val==1?'<span class="label label-danger">SI</span>':$val;
			return $val;
		});
		$crud->unset_columns('user_id');
		$crud->where('user_id',$x);
		$crud->field_type('user_id','hidden',$x);
		$crud = $crud->render();
		$crud->header = new ajax_grocery_crud();
		$crud->header->set_table('pacientes')
					 ->set_subject('Usuario')
					 ->set_theme('header_data')
					 ->set_primary_key('id')
					 ->columns('nombre','apellido','email')
					 ->where('id',$x)
					 ->set_url('administracion/pacientes');
		$crud->header = $crud->header->render(1)->output;
		$crud->output = $this->load->view('usermeta',['output'=>$crud->output,'user'=>$x],TRUE);
		$this->loadView($crud);
	}

	function sendMessage(){
		$this->form_validation->set_rules('user_id','Usuario','required')
							  ->set_rules('mensaje','Usuario','required');
		if($this->form_validation->run()){
			$user = get_usermeta($_POST['user_id'],'gcm',TRUE)['meta_value'];
			$this->load->model('elements_app');
			$this->elements_app->sendPush(array($user),array('message'=>$_POST['mensaje']),$_POST['titulo']);
			$this->db->insert('mensajes',[
				'user_id'=>$_POST['user_id'],
				'titulo'=>$_POST['titulo'],
				'mensaje'=>$_POST['mensaje'],
				'estado'=>1,
				'fecha_envio'=>date("Y-m-d H:i:s")
			]);
			echo $this->success('Mensaje enviado');
		}else{
			echo $this->error($this->form_validation->error_string());
		}
	}
}
?>