<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Cron extends Main{
        protected $tipos_pago = array('1','2');
        function __construct() {
            parent::__construct();
            $this->load->model('elements_app');
        }

        function tasks(){
        	//Enviar mensajes programados
        	$mensajes = $this->db->get_where('mensajes',['estado'=>0]);
        	foreach($mensajes->result() as $m){
        		if($m->user_id=='6'){ //Enviar a todos los usuarios
        			$this->db->where('meta_key','gcm');
        			foreach($this->db->get_where('usermeta')->result() as $u){
        				$this->elements_app->sendPush(array($u->meta_value),array('message'=>$m->mensaje),$m->titulo);
        			}
        		}else{
        			$user = get_usermeta($m->user_id,'gcm',TRUE)['meta_value'];        			
					$this->elements_app->sendPush(array($user),array('message'=>$m->mensaje),$m->titulo);
        		}        
        		$this->db->update('mensajes',['estado'=>1],['id'=>$m->id]);		
        	}
        }
    }
?>
