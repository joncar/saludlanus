<div class="page-container">
    <section class="main-slider">
        <div class="slider-pro full-width-slider" id="main-slider"
            data-width="100%"
            data-height="800"
            data-fade="true"
            data-buttons="true"
            data-arrows="false"
            data-next-slide=".pro-next"
            data-previous-slide=".pro-prev"
            data-wait-for-layers="true"
            data-thumbnail-pointer="false"
            data-touch-swipe="false"
            data-autoplay="true"
            data-auto-scale-layers="true"
            data-visible-size="100%"
            data-force-size="fullWidth"
            data-autoplay-delay="5000"
            >
            <div class="sp-slides">
                <div class="sp-slide slide-1 b-home-big-text">
                    <img class="sp-image" src="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg"
                    data-src="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg"
                    data-retina="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg" alt="/" />
                    <div class="slider-filter-holder">
                        <div class="cutBox cut-bottom">
                        </div>
                        <div class="custom-filter">
                            <div class="slider-icon">
                                <img src="<?= base_url() ?>img/slider-icon.png" alt="/">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="sp-layer layer-1-0 slide-tex-1 hidden-xs hidden-sm hidden-md"
                            data-vertical="25%"
                            data-horizontal="25%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <div class="rotate-block">
                                <p class="font-secondary text-uppercase">
                                    Actuem i Transformem
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-1 slide-tex-1"
                            data-vertical="18%"
                            data-horizontal="53%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <h5 class="first-heading font-secondary">
                            Treballem per a generar
                            </h5>
                        </div>
                        <div class="sp-layer layer-1-2 slide-tex-1"
                            data-vertical="22%"
                            data-horizontal="53%"
                            data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                            <h4 class="first-heading font-secondary">
                            <strong>canvis en la nostra realitat.</strong>
                            </h4>
                            <div class="b-text">
                                <p>
                                    Dissenyem, gestionem i desenvolupem serveis d’atenció i suport a dones, <br>joves i famílies, i innovem amb programes de prevenció i sensibilització social, <br>per a la transformació cap a una societat més sostenible i justa, sense <br>desigualtats per raons de gènere.
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-3 slide-button"
                            data-vertical="60%"
                            data-horizontal="54%"
                            data-show-transition="bottom" data-hide-transition="up" data-show-delay="1000">
                            <a href="<?= base_url() ?>servei/1-atencio-a-les-dones" class="btn btn-default-arrow btn-sm btn-clear">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                llegir més
                            </a>
                        </div>
                    </div>
                </div>
                <div class="sp-slide slide-1 b-home-big-text">
                    <img class="sp-image" src="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg"
                    data-src="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg"
                    data-retina="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg" alt="/" />
                    <div class="slider-filter-holder">
                        <div class="cutBox cut-bottom">
                        </div>
                        <div class="custom-filter">
                            <div class="slider-icon">
                                <img src="<?= base_url() ?>img/slider-icon.png" alt="/">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="sp-layer layer-1-0 slide-tex-1 hidden-xs hidden-sm hidden-md"
                            data-vertical="25%"
                            data-horizontal="25%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <div class="rotate-block">
                                <p class="font-secondary text-uppercase">
                                    SABEM I INNOVEM
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-1 slide-tex-1"
                            data-vertical="18%"
                            data-horizontal="53%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <h5 class="first-heading font-secondary">
                            Tenim experiència
                            </h5>
                        </div>
                        <div class="sp-layer layer-1-2 slide-tex-1"
                            data-vertical="22%"
                            data-horizontal="53%"
                            data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                            <h4 class="first-heading font-secondary">
                            <strong>i la volem compartir</strong>
                            </h4>
                            <div class="b-text">
                                <p>
                                    Som entitat de referència. Oferim formació, assessorament <br>
                                    i consultoria professional. Generem continguts i processos <br>
                                    d’atenció a partir de la pròpia experiència i del treball en <br>
                                    xarxa amb la transferència de sabers. Impulsem nous espais <br>
                                    d’actuació i col·laborem amb iniciatives comunitàries.
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-3 slide-button"
                            data-vertical="60%"
                            data-horizontal="54%"
                            data-show-transition="bottom" data-hide-transition="up" data-show-delay="1000">
                            <a href="<?= base_url() ?>p/filosofia.html" class="btn btn-default-arrow btn-sm btn-clear">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                llegir més
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-home-primary">
        <div class="b-home-about relative">
            <div class="vertical-container-top vertical-left home-about-side-title hidden-md">
                <span class="vertical-text-main color-add text-uppercase">dae</span>
                <span class="vertical-text-additional color-main text-uppercase">Qui som</span>
                <span class="vertical-number color-main font-primary pull-right">01</span>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="b-mod-heading wow fadeInDown">
                            <p class="first-heading font-secondary">L'Associació</p>
                            <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong>Dones amb empenta</strong>
                            </h2>
                        </div>
                        <div class="b-blockquote-holder">
                            <div class="b-blockquote-left-border">
                                <p class="quote-text">
                                    “El feminisme és la idea radical que sosté que les dones som persones”, Angela Davis.
                                </p>
                                <p class="quote-author">
                                    <!-- <span class="ef icon_quotations quote-icon"></span> -->
                                    <span class="author-name"></span>
                                    <span class="author-role"></span>
                                </p>
                            </div>
                        </div>
                        <div class="b-text">
                            <p>
                                Dones Amb Empenta és una entitat oberta a les inquietuds i necessitats de les persones. Treballem per a generar canvis en la nostra realitat i construir una societat equitativa i lliure de violències masclistes.
                                Tenim reconeguda la Declaració d’Utilitat Pública i garantim la màxima qualitat de les nostres activitats i serveis. Treballem amb rigor, de forma eficaç i eficient, per donar a la ciutadania una resposta integral, especialitzada, àgil, propera i coordinada.
                                Treballem en xarxa i participem en els espais de construcció i reivindicació del teixit associatiu. Formem part del Consell Nacional de Dones de Catalunya.
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="img-brd-mod">
                            <div class="brd"></div>
                            <div class="img-cut">
                                <div class="cut"></div>
                                <img src="<?= base_url() ?>assets/template/media/home/home-about-1.jpg" class="img-responsive center-block" alt="/">
                            </div> 
                        </div>
                        <a href="<?= base_url() ?>files/7d35c-memoria20anys.pdf" target="_new" class="btn btn-default-arrow btn-sm btn-clear">
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            Conèixer la nostra història
                        </a>
                        <a href="<?= base_url() ?>files/c104b-memo2017.pdf" target="_new" class="btn btn-default-arrow btn-sm btn-clear">
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            Última memòria
                        </a>
                    </div>
                </div>
            </div>
        </div>
        <div class="b-home-services">
            <div class="container-fluid">
                <div class="b-services-holder clearfix">
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/V.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                DESIGUALTATS I VIOLÈNCIES
                                </h6>
                                <p>
                                Treballem per aconseguir una societat més justa i sostenible, posant en el centre la vida i en valor la diversitat, la cura, el plaer, la cooperació.  </p>
                            </p>
                            <p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item wow fadeInRight">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/p.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                PERSPECTIVA DE GÈNERE
                                </h6>
                                <p>
                                    Tots el nostres projectes i serveis són sensibles a les diferents formes en que les persones experimenten les situacions.
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item wow fadeInLeft">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/a.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                APODERAMENT
                                </h6>
                                <p>
                                    Donem visibilitat i valor al treball de sosteniment de la vida, el treball de cures, que entenem com a responsabilitat col·lectiva. <p>
                                    </p> <br>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4">
                            <div class="b-services-item wow fadeInRight">
                                <div class="services-icon">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/i.png" alt="/">
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    INTERSECCIONALITAT
                                    </h6>
                                    <p>
                                    Tenim en compte la diversitat de les persones, la complexitat de les situacions  i les diferents variables que la conformen.  </p>
                                </div>
                            </div>
                        </div>
                        
                        <div class="b-home-services">
                            <div class="container-fluid">
                                <div class="b-services-holder clearfix">
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="b-services-item">
                                            <div class="services-icon">
                                                <img src="<?= base_url() ?>assets/template/media/services-icons/f.png" alt="/">
                                            </div>
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                INNOVACIÓ
                                                </h6>
                                                <p>
                                                    Garantim la formació i actualització professional continua de les nostres  professionals.<p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-4">
                                                <div class="b-services-item wow fadeInRight">
                                                    <div class="services-icon">
                                                        <img src="<?= base_url() ?>assets/template/media/services-icons/comunity.png" alt="/">
                                                    </div>
                                                    <div class="services-text">
                                                        <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                        COMUNITAT
                                                        </h6>
                                                        <p>
                                                        Promovem la participació i la dinamització del teixit social i associatiu. Només amb la implicació de tots i totes podrem assolir una societat millor.  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>
                                <div class="b-home-works relative">
                                    <div class="vertical-container-top vertical-left home-works-side-title hidden-md">
                                        <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                        <span class="vertical-text-additional color-main text-uppercase">QUÈ FEM</span>
                                        <span class="vertical-number color-main font-primary pull-right">02</span>
                                    </div>
                                    
                                    <div class="b-tabs-holder wow fadeInDown">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-2">
                                                    
                                                    
                                                    <ul class="sidebarmenuserveis2" style="margin-bottom: 30px;">
                                                        <?php
                                                        $this->db->order_by('servicios_categorias.orden','ASC');
                                                        foreach($this->db->get_where('servicios_categorias')->result() as $c):
                                                        ?>
                                                            <li><a class="fucsia" href="#Serv<?= $c->id ?>" data-toggle="tab" aria-controls="all" role="tab" onclick="$('.servLis li, .servLis li a').removeClass('active'); $('#servLi<?= $c->id ?>').addClass('active');"><?= $c->nombre ?></a></li>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-9 col-md-offset-1">
                                                    
                                                    <div class="tab-content">
                                                        <!--- Comienzan servicios contnido --->
                                                        <?php
                                                            $this->db->order_by('servicios_categorias.orden','ASC');
                                                            foreach($this->db->get_where('servicios_categorias')->result() as $n=>$c):
                                                                $fotos = explode(',',$c->fotos);
                                                                if(empty($fotos[count($fotos)-1])){
                                                                array_pop($fotos);
                                                            }
                                                        ?>
                                                        
                                                        <div role="tabpanel" class="sliderWithoutOverflow tab-custom <?= $n==0?'active':'' ?>" id="Serv<?= $c->id ?>">
                                                            <div class="clearfix">
                                                                <div class="col-xs-12 col-sm-8 tab-img">
                                                                    <ul class="list-unstyled enable-bx-slider" data-counter="true" data-pager-custom="#bx-pager-all" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="555" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                                                                        <li>
                                                                            <div class="img-brd-mod">
                                                                                <div class="brd"></div>
                                                                                <div class="img-cut">
                                                                                    <div class="cut"></div>
                                                                                    <img src="<?= base_url() ?>img/servicios/<?= $fotos[0] ?>" class="img-responsive" alt="/">
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="tab-caption">
                                                                        <div class="b-mod-heading">
                                                                            <!--    <p class="first-heading font-secondary">“És justícia i no caritat el que el món necessita” Mary Wollstonecraft</p> -->
                                                                            <h2 class="heading-line line-right customColor customPseudoElBg">
                                                                            <strong><?= $c->nombre ?></strong>
                                                                            </h2>
                                                                        </div>
                                                                        <p class="tab-caption-text">
                                                                            <?= $c->descripcion ?>
                                                                        </p>
                                                                        <a href="<?= base_url() ?>servei/<?= toUrl($c->id.'-'.$c->nombre) ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                            Llegir més
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <?php endforeach ?>
                                                    </div>
                                                    
                                                </div>

                                                <div class="col-xs-12 col-sm-12 hidden-xs" style="margin-top: 40px;">
                                                        <div class="b-member-list">
                                                            <div class="additional-images-thumbs">
                                                                <ul id="bx-pager-all" role="tablist" class="servLis pager-custom enable-bx-slider member-list-item" data-pager-custom="null" data-controls="false" data-min-slides="2" data-max-slides="6" data-slide-width="115" data-slide-margin="22" data-pager="false" data-mode="horizontal" data-infinite-loop="false">
                                                                    <?php
                                                                        $this->db->order_by('servicios_categorias.orden','ASC');
                                                                        foreach($this->db->get_where('servicios_categorias')->result() as $n=>$c):
                                                                            $fotos = explode(',',$c->fotos);
                                                                            if(empty($fotos[count($fotos)-1]))
                                                                            array_pop($fotos);
                                                                    ?>

                                                                    <li id="servLi<?= $c->id ?>">
                                                                        <a href="#Serv<?= $c->id ?>" data-toggle="tab" aria-controls="all" role="tab">
                                                                            <div class="member-img noafter">
                                                                                <img src="<?= base_url() ?>img/servicios/<?= $fotos[0] ?>" alt="/" />
                                                                            </div>
                                                                        </a>
                                                                    </li>
    
                                                                    <?php endforeach ?>
                                                                        
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--- Finaliza contenido ---->
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>
                                <div class="b-home-offers relative">
                                    <div class="container-bg-additional relative clearfix">
                                        <div class="b-services-bg-filter customBgColor">
                                        </div>
                                        <div class="vertical-container-top vertical-left home-offers-side-title hidden-md">
                                            <span class="vertical-text-main color-white text-uppercase">DAE</span>
                                            <span class="vertical-text-additional color-white text-uppercase">Territori</span>
                                            <span class="vertical-number color-white font-primary pull-right">03</span>
                                        </div>
                                        <div class="b-featured-services">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                                        
                                                    </div>
                                                    <div class="col-xs-11 col-sm-6 col-md-6 col-lg-6" style="margin-left: -240px;">
                                                        <div class="featured-services-left wow slideInLeft">
                                                            <div class="b-f-s-info" style="width: 780px;padding-left: 300px;">
                                                                <h2 class="f-s-title" style="font-size:50px">
                                                                Territori
                                                                <br>
                                                                d'actuació
                                                                <br>
                                                                
                                                                </h2>
                                                                <p>
                                                                    El territori d’actuació de Dones Amb Empenta és Catalunya, essent les comarques de la Catalunya Central i de Lleida el seu espai de màxima influència. La seu de l’entitat es troba a Igualada.
                                                                </p>
                                                                <a href="<?= base_url() ?>p/territorideactuacio.html" class="btn btn-default-arrow btn-sm btn-clear">
                                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                    punt d'atenció i activitat
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-home-team relative">
                                    <div class="b-team">
                                        <div class="vertical-container-top vertical-left home-team-side-title hidden-md">
                                            <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                            <span class="vertical-text-additional color-main text-uppercase">l'equip</span>
                                            <span class="vertical-number color-main font-primary pull-right">04</span>
                                        </div>
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-sm-6 col-md-6 wow fadeInLeft">
                                                    <div class="row">
                                                        <div id="bx-team-page" class="b-member-list enable-bx-slider"
                                                            data-pager-custom="null"
                                                            data-controls="true"
                                                            data-min-slides="1"
                                                            data-max-slides="1"
                                                            data-slide-width="0"
                                                            data-slide-margin="0"
                                                            data-pager="false"
                                                            data-mode="horizontal"
                                                            data-infinite-loop="false"
                                                            >
                                                            <?php
                                                                $x = 0;
                                                                $this->db->order_by('orden','ASC');
                                                                $equipo = $this->db->get('equipo');
                                                                foreach($equipo->result() as $n=>$e):
                                                                if($x==0):
                                                            ?>
                                                            <div class="member-list-item clearfix">
                                                                <?php endif ?>
                                                                <a data-slide-index="<?= $n ?>" href="#">
                                                                    <div class="col-xs-6 col-sm-6 text-center">
                                                                        <div class="member-img">
                                                                            <img src="<?= base_url('img/servicios/'.$e->foto) ?>" alt="/">
                                                                        </div>
                                                                    </div>
                                                                </a>
                                                                
                                                                <?php $x++; if($x==4 || $n==$equipo->num_rows()-1): $x=0;?>
                                                            </div>
                                                            <?php endif; endforeach ?>
                                                            
                                                        </div>
                                                        <div class="bx-outside-controls">
                                                            <p>
                                                                <span id="bx-prev"></span><span id="bx-next"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-6 col-md-6 wow fadeInRight">
                                                    <div class="b-member-info-slide enable-bx-slider" data-pager-custom="#bx-team-page" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="0" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                                                        
                                                        <?php foreach($equipo->result() as $n=>$e): ?>
                                                        <div class="b-member-info">
                                                            <div class="b-mod-heading">
                                                                <p class="first-heading font-secondary member-prof"><?= $e->cargo ?></p>
                                                                <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                                                <strong><?= $e->nombre ?></strong>
                                                                </h2>
                                                                <div class="member-socials">
                                                                    <ul class="list-inline">
                                                                        <li>
                                                                            <a href="mailto:<?= $e->email ?>">
                                                                                <i class="fa fa-envelope fa-fw" aria-hidden="true"></i>
                                                                                <?= $e->email ?>
                                                                            </a>
                                                                        </li>
                                                                        
                                                                    </ul>
                                                                </div>
                                                            </div>
                                                            <div class="b-member-caption">
                                        
                                                                <?php 
                                                                    foreach(explode('--',$e->areas) as $a): if(!empty($a)):
                                                                ?>
                                                                    <div class="b-member-caption">
                                                                        <h6 class="contact-info-title customPseudoElBg font-secondary" style="text-transform: none;"><?= explode(':',$a)[0] ?></h6>
                                                                        <?php if(count(explode(':',$a))==2 && !empty(explode(':',$a)[1]) && count(explode(',',$a))>0): ?>
                                                                            <div class="b-member-progress">
                                                                                <?php foreach(explode(',',$a) as $ep): if(!empty($ep)): ?>
                                                                                    <div class="progress progress-label2">
                                                                                        <span class="prog-title" style="text-transform: none;">
                                                                                            <?= count(explode(':',$ep))==2?explode(':',$ep)[1]:$ep ?>
                                                                                        </span>
                                                                                        <div class="bar-holder" style="width:100%">
                                                                                            <div class="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                                                        </div>                                                            
                                                                                    </div>
                                                                                <?php endif; endforeach ?>                                                    
                                                                            </div>
                                                                        <?php endif; ?>
                                                                    </div>
                                                                <?php endif; endforeach ?>


                                                            </div>
                                                            
                                                        </div>
                                                        <?php endforeach ?>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="franja">
                                    
                                </div>
                                <div class="b-home-news relative">
                                    <div class="vertical-container-top vertical-left home-news-side-title hidden-md">
                                        <span class="vertical-text-main color-add text-uppercase">dae</span>
                                        <span class="vertical-text-additional color-main text-uppercase">notícies</span>
                                        <span class="vertical-number color-main font-primary pull-right">05</span>
                                    </div>
                                    <div class="container">
                                        <?php if($blog->num_rows()>0): ?>
                                        <div class="primary-home-news wow fadeInDown">
                                            <div class="row">
                                                <div class="b-blog-item clearfix">
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="img-brd-mod">
                                                            <div class="brd"></div>
                                                            <div class="b-blog-img img-cut">
                                                                <div class="cut"></div>
                                                                <img src="<?= $blog->row(0)->foto ?>" class="img-responsive center-block" alt="/">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6">
                                                        <div class="b-blog-caption">
                                                            <div class="caption-data customPseudoElBg">
                                                                <span class="date"><?= strftime("%d-%b-%Y",strtotime($blog->row(0)->fecha)) ?></span>
                                                                <span class="author"><?= $blog->row(0)->user ?></span>
                                                            </div>
                                                            <h4 class="caption-title">
                                                            <?= $blog->row(0)->titulo ?>
                                                            </h4>
                                                            <div class="caption-preview-text">
                                                                <?= substr(strip_tags($blog->row(0)->texto),0,80) ?>
                                                            </div>
                                                            <a href="<?= base_url('blog/'.toUrl($blog->row(0)->id.'-'.$blog->row(0)->titulo)) ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                llegir més
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif ?>
                                        <div class="secondary-home-news">
                                            <div class="row">
                                                <?php for($i=1; $i<$blog->num_rows(); $i++): ?>
                                                <div class="col-xs-4 col-sm-4 wow fadeInLeft">
                                                    <a href="<?= $blog->row($i)->link ?>">
                                                        <div class="b-blog-item clearfix">
                                                            <div class="b-blog-caption">
                                                                <div class="caption-data customPseudoElBg">
                                                                    <span class="date"><?= strftime("%d-%b-%Y",strtotime($blog->row($i)->fecha)) ?></span>
                                                                    <span class="author"><?= $blog->row($i)->user ?></span>
                                                                </div>
                                                                <h4 class="caption-title">
                                                                <?= substr(strip_tags($blog->row($i)->texto),0,80) ?>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php endfor ?>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="franja">
                                    
                                </div>
                                <div class="b-home-about relative">
                                    <div class="vertical-container-top vertical-left home-about-side-title hidden-md">
                                        <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                        <span class="vertical-text-additional color-main text-uppercase">xarxes</span>
                                        <span class="vertical-number color-main font-primary pull-right">06</span>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-12">
                                                <ul id="portfolio-filter" class="filter" data-related-grid="portfolio-grid">
                                                    <li class="active">
                                                        <a href="#" data-filter="*" id="allSocial">
                                                            <i class="fa fa-globe fa-2x"></i> <br/>
                                                            Totes
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-filter=".instagram" href="#" title="Feed Instagram">
                                                            <i class="fa fa-2x fa-instagram"></i><br>
                                                            Instagram
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-filter=".twitter" href="#" title="Feed Twitter">
                                                            <i class="fa fa-2x fa-twitter"></i><br>
                                                            Twitter
                                                        </a>
                                                    </li>
                                                    <li>
                                                        <a data-filter=".facebook" href="#" title="Feed Facebook">
                                                            <i class="fa fa-2x fa-facebook-square"></i><br/>
                                                            Facebook
                                                        </a>
                                                    </li>
                                                </ul>
                                                <div id="portfolio-grid" class="social-feed-container isotope-grid gallery-container style-column-4 isotope-spaced clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        <div class="b-home-video-block">
                            <div class="container-fluid">
                                <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 left-side wow slideInLeft">
                                    
                                </div>
                                <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 right-side wow slideInRight">
                                    <div class="content">
                                        <div class="b-mod-heading">
                                            <p class="first-heading customPseudoElBg font-secondary">Si vols col·laborar i/o </p>
                                            <h2 class="heading-line line-right">
                                            <strong>Treballar amb nosaltres </strong>
                                            </h2>
                                            
                                            <a href="<?= base_url() ?>p/traballar.html" class="btn btn-default-arrow">
                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                Veure ofertes de treball
                                            </a>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </section>

<?php if(!empty($this->db->get('ajustes')->row()->popup)): ?>
    <script>
        alert("");
        window.onload = function(){
            jQuery.get('paginas/frontend/popup',function(data){
                    $("body").append(data);
                    $("body").find('#popup').modal('show');                                    
            });
        };
    </script>
<?php endif ?>