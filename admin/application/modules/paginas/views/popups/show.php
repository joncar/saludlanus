<?php 
    $fields = explode('--',$popup->campos);
    $hiddens = !empty($fields[1])?$fields[1]:'';
    $fields = explode(';',$fields[0]);
    $hiddens = explode(';',$hiddens);
?>  

<div class="modal fade modal-xs" tabindex="-1" role="dialog" id="popup">
  <div class="modal-dialog modal-md" role="document">
    <div class="modal-content">
	<button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
      <?php if(!empty($popup->titulo)): ?>
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title"><?= $popup->titulo ?></h4>
	      </div>
  	  <?php endif ?>
      <div class="modal-body">
      	<?php if(!empty($popup->foto) && !empty($popup->enlace)): ?>
      		<a href="<?= base_url($popup->enlace) ?>">
      			<img src="<?= base_url().'img/popups/'.$popup->foto ?>" alt="" style="width:100%;">
      		</a>
      	<?php endif ?>
      	<?php if(!empty($popup->campos)): ?>
	        <form action="paginas/frontend/contacto" onsubmit="return sendForm(this,'#response');" method="post" style="display: inline-block; ">
	            <div style="text-align:center;">
	                <h4 id="mce_203" class="" style="margin-bottom: 0"><?= $popup->titulo ?></h4>
	                <p style="color:#707070"><?= $popup->subtitulo ?></p>
	            </div>
	            <!-- Layout 50x50 -->
	            <div class="template-clear-fix">
	                <!-- Left column -->
	                <!-- Name -->
	                <?php foreach($fields as $f): if(!empty($f)): ?>
	                    <div class="template-form-line template-state-block">
	                        <?= $f ?>
	                    </div>
	                <?php endif; endforeach ?>                    
	                <div class="template-form-line template-state-block" style="margin-top: 10px; text-align: center;">
	                    <div id="response" style="max-width: 200px; display: none;"></div>
	                </div>
	                <div class="template-form-line template-align-center">
	                    <div class="template-state-block">
	                        <!-- Submit button -->
	                        <input class="template-component-button template-component-button-style-1" type="submit" name="contact-form-submit" id="contact-form-submit" value="Enviar">
	                    </div>
	                </div>
	            </div>
	            <?php foreach($hiddens as $f): ?>
	                <?= $f ?>
	            <?php endforeach ?>        
	            <input type="hidden" name="echo" value="1">                        
	        </form>
    	<?php endif ?>
      </div>
    </div><!-- /.modal-content -->
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->