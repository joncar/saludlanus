<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/7.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Lequip') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Qui som') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Lequip') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div id='lequip'>                
                <div class="container">
                    <div class="row">
                        <div class="col-xs-4 col-sm-6 col-md-6 wow fadeInLeft">
                            <div class="row">
                                <div id="bx-team-page" class="b-member-list enable-bx-slider"                                     
                                     data-controls="true"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="false"
                                     data-counter="true"
                                     data-mode="horizontal"
                                     data-infinite-loop="false"
                                     data-pager-short-separator=" / "
                                     data-content-pager=".paginador"
                                     >
                                     <?php 
                                        $x = 0;
                                        $this->db->order_by('orden','ASC');
                                        $equipo = $this->db->get('equipo');
                                        foreach($equipo->result() as $n=>$e):
                                        $e = $this->traduccion->traducirobj($e);
                                        if($x==0): 
                                    ?>
                                    <div class="member-list-item clearfix">
                                    <?php endif ?>

                                            <a data-slide-index="<?= $n ?>" href="#">
                                                <div class="col-xs-6 col-sm-6 col-md-4 text-center">
                                                    <div class="member-img">
                                                        <img src="<?= base_url('img/servicios/'.$e->foto) ?>" alt="/">
                                                    </div>
                                                </div>
                                            </a>
                                            
                                    <?php $x++; if($x==9 || $n==$equipo->num_rows()-1): $x=0;?>
                                    </div>
                                    <?php endif; endforeach ?>
                                    
                                </div>
                                <div class=""><p></p></div>
                                <div id="paginador" class="paginador slide-counter bx-outside-controls" style="text-align:center">
                                    <span id="bx-prev"></span>
                                    <strong class="current-index"></strong>  /
                                    <span class="current-qty" style="margin: 0;color: 0;border: 0;"></span>
                                    <span id="bx-next"></span>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-8 col-sm-6 col-md-6 wow fadeInRight">
                            <div class="b-member-info-slide enable-bx-slider" data-pager-custom="#bx-team-page" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="0" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                               
                                <?php foreach($equipo->result() as $n=>$e): ?>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof"><?= $e->cargo ?></p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong><?= $e->nombre ?></strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="mailto:<?= $e->email ?>">
                                                        <i class="fa fa-envelope fa-fw" aria-hidden="true"></i>
                                                        <?= $e->email ?>
                                                    </a>
                                                    <?php if(!empty($e->linkedin)): ?>
                                                        | 
                                                        <a href="<?= $e->linkedin ?>">
                                                            <i class="fa fa-linkedin fa-fw" aria-hidden="true"></i>                                                            
                                                        </a>
                                                    <?php endif ?>
                                                    <?php if(!empty($e->ficha)): ?>                                                        
                                                        <a href="<?= base_url().'files/'.$e->ficha ?>" target="_new" style="margin-left:15px">
                                                            <i class="fa fa-print fa-fw" aria-hidden="true"></i>
                                                            <?= l('veurecurriculum') ?>
                                                        </a>
                                                    <?php endif ?>
                                                    <?php if(!empty($e->telefono)): ?>                                                        
                                                        <a href="tel:<?= $e->telefono ?>" style="margin-left:15px">
                                                            <i class="fa fa-mobile fa-fw" aria-hidden="true"></i>
                                                            <?= $e->telefono ?>
                                                        </a>
                                                    <?php endif ?>
                                                </li>
                                                
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        
                                        <?php 
                                            foreach(explode('--',$e->areas) as $a): if(!empty($a)):
                                        ?>
                                            <div class="b-member-caption">
                                                <h6 class="contact-info-title customPseudoElBg font-secondary" style="text-transform: none;"><?= explode(':',$a)[0] ?></h6>
                                                <?php if(count(explode(':',$a))==2 && !empty(explode(':',$a)[1]) && count(explode(',',$a))>0): ?>
                                                    <div class="">
                                                        <?php foreach(explode(',',$a) as $ep): if(!empty($ep)): ?>
                                                            <div class="">
                                                                <span class="prog-title" style="text-transform: none;">
                                                                    <?= count(explode(':',$ep))==2?explode(':',$ep)[1]:$ep ?>
                                                                </span>                                                         
                                                            </div>
                                                        <?php endif; endforeach ?>                                                    
                                                    </div>
                                                <?php endif; ?>
                                            </div>
                                        <?php endif; endforeach ?>


                                    </div>
                                    
                                </div>
                                <?php endforeach ?>
                        </div>
                    </div>
                </div>
            </div>
            </div>
        </div>
    </div>




    </section>
    <div class="b-home-video-block">
        <div class="container-fluid">
            <div class="col-xs-12 col-sm-5 col-md-6 col-lg-6 left-side wow slideInLeft">
                
            </div>
            <div class="col-xs-12 col-sm-7 col-md-6 col-lg-6 right-side wow slideInRight">
                <div class="content">
                    <div class="b-mod-heading">
                        <p class="first-heading customPseudoElBg font-secondary"><?= l('Collaboraio') ?> </p>
                        <h2 class="heading-line line-right">
                            <strong><?= l('Treballa amb nosaltres') ?> </strong>
                        </h2>
                        <br/>
                        <a href="<?= base_url() ?>treballar.html" class="btn btn-default-arrow btn-sm btn-clear">
                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                            <?= l('Veure ofertes de treball') ?>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
