<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/2.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Què fem2') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="01_home-1.html"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Què fem2') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-services">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-mod-heading text-center">
                        <p class="first-heading font-secondary wow fadeInDown"><?= l('Lluitem contra la violència i discriminació') ?></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg wow fadeIn">
                            <strong><?= l('Promovem la igualtat') ?></strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('oferim') ?>
                        </p>
                    </div>
                </div>
            </div>
            <div class="b-services-holder">
                <div class="row">
                    <div class="col-xs-6 col-sm-4 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/A.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Atenció a les Dones') ?>
                                </h6>
                                <p>
                                    <?= l('atenciotext') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 wow fadeInDown">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/P.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Prevenció i sensibilització') ?>
                                </h6>
                                <p>
                                   <?= l('La coeducació, l’amor romàntic i els
estereotips de gènere, la violència masclista, l’educació per la salut, la sexualitat…') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/R.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Repr. Institucional i Act. Associativa') ?>
                                </h6>
                                <p>
                                    <?= l('reptext') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/f.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Formació i assessorament') ?>
                                </h6>
                                <p>
                                    <?= l('foramtext') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 wow fadeInUp">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/g.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Gestió i finanament') ?>
                                </h6>
                                <p>
                                    <?= l('getext') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/A.jpg" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('Altres') ?>
                                </h6>
                                <p>
                                    <?= l('Altres serveis') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        
        <!--- REpeticion --->
        <div class="container-bg-additional relative clearfix">
            <div class="b-services-bg-filter customBgColor">
            </div>
            <div class="b-featured-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow slideInLeft">
                            <div class="featured-services-left">
                                <div class="b-f-s-info">
                                    <h2 class="f-s-title">
                                        <?= l('Atenció') ?>
                                    </h2>
                                    <p>
                                        <?= l('atext') ?>
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        <?= l('Més informació') ?>
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
                            <div class="featured-services-right">
                                <div class="b-services-slides-holder enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="false"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="true"
                                     data-mode="vertical"
                                     data-infinite-loop="true"
                                     data-auto="true"
                                     data-pause="5000"
                                     data-auto-hover="true"
                                     >
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    <?= l('equd') ?>
                                                </h6>
                                                <p>
                                                    <?= l('equdtext') ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    <?= l('projecte') ?>
                                                </h6>
                                                <p>
                                                    <?= l('projectext') ?>
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Programa ONA
                                                </h6>
                                                <p>
                                                    Programa dirigit a la millora de l’ocupabilitat de les dones des d’una perspectiva de gènere.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Servei d’Informació i Atenció a les Dones
                                                </h6>
                                                <p>
                                                    S’ofereix assessorament i atenció psicològica, educativa i jurídica amb perspectiva de gènere.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Serveis d’Intervenció Especialitzada en Violència Masclista de la Catalunya Central i de Lleida (SIEs)
                                                </h6>
                                                <p>
                                                    Serveis públics especialitzats, que depenen administrativament del Departament de Treball, Afers Socials i Famílies, Direcció General de Famílies de la Generalitat de Catalunya.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Equip d’Atenció Familiar (EAF)
                                                </h6>
                                                <p>
                                                    El projecte ofereix a les famílies eines i estratègies per gestionar conflictes generats per situacions de crisi, i és també una estratègia de prevenció i detecció de la violència familiar.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Compartir i créixer
                                                </h6>
                                                <p>
                                                    El programa acull famílies amb infants en edats primerenques per tal d’afavorir el seu benestar.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Servei d’atenció a joves i famílies
                                                </h6>
                                                <p>
                                                    El Servei ofereix assessorament i acompanyament en la prevenció i la gestió de situacions de crisi i conflicte personal i familiar.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        
        <div class="container-bg-additional relative clearfix">
            <div class="b-services-bg-filter customBgColor">
            </div>
            <div class="b-featured-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow slideInLeft">
                            <div class="featured-services-left">
                                <div class="b-f-s-info">
                                    <h2 class="f-s-title">
                                        Prevenció
                                        <br>
                                        i sensibilització
                                    </h2>
                                    <p>
                                        Des dels seus inicis, Dones amb Empenta desenvolupa la seva tasca de prevenció i sensibilització a través de tallers a mida per
a joves, infants i adults. A partir d’una mirada multidisciplinària s’abasten temes com la coeducació, l’amor romàntic i els estereotips de gènere, la violència masclista, l’educació per la salut, la sexualitat…
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        Més informació
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
                            <div class="featured-services-right">
                                <div class="b-services-slides-holder enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="false"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="true"
                                     data-mode="vertical"
                                     data-infinite-loop="true"
                                     data-auto="true"
                                     data-pause="5000"
                                     data-auto-hover="true"
                                     >
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Tallers per a infants
                                                </h6>
                                                <p>
                                                    L’objectiu prioritari és fomentar els valors igualitaris i no discriminatoris per raó de sexe/gènere en educació
infantil i primària a través d’activitats i dinàmiques que afavoreixen la detecció d’estereotips i discriminacions per raó de gènere.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Tallers per a joves
                                                </h6>
                                                <p>
                                                    Són una eina essencial de contacte amb la població jove i de detecció precoç de situacions de violència masclista i models de relació de risc.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Programa “Connecta’t amb amor”
                                                </h6>
                                                <p>
                                                    L’objectiu prioritari del programa és fomentar la coeducació, especialment en població adolescent.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        
        
        
        <div class="container-bg-additional relative clearfix">
            <div class="b-services-bg-filter customBgColor">
            </div>
            <div class="b-featured-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow slideInLeft">
                            <div class="featured-services-left">
                                <div class="b-f-s-info">
                                    <h2 class="f-s-title">
                                        Representació Institucional
                                        <br>
                                        i Activitat Associativa
                                    </h2>
                                    <p>
                                        DAE desenvolupa un programa lúdic i cultural per promoure la participació
de les dones de la comarca de l’Anoia. Aquest eix està gestionat
per les sòcies de l’entitat de forma voluntària.
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        Més informació
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
                            <div class="featured-services-right">
                                <div class="b-services-slides-holder enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="false"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="true"
                                     data-mode="vertical"
                                     data-infinite-loop="true"
                                     data-auto="true"
                                     data-pause="5000"
                                     data-auto-hover="true"
                                     >
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Concurs de fotografia
                                                </h6>
                                                <p>
                                                    Neix l’any 2002 amb motiu de la Commemoració del 8 de març, Dia Internacional de les Dones, com a iniciativa de les sòcies i voluntàries de l’Entitat.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Taller de ioga
                                                </h6>
                                                <p>
                                                    Des de l’any 2003 l’associació organitza un taller de ioga com a espai de trobada i de cura per a dones. Enguany participen del grup 14 dones.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
        
        <div class="container-bg-additional relative clearfix">
            <div class="b-services-bg-filter customBgColor">
            </div>
            <div class="b-featured-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow slideInLeft">
                            <div class="featured-services-left">
                                <div class="b-f-s-info">
                                    <h2 class="f-s-title">
                                        Formació i
                                        <br>
                                        assessorament
                                    </h2>
                                    <p>
                                        DAE transmet el coneixement i l’experiència adquirida mitjançant
la formació. L’entitat forma i assessora a professionals
mitjançant convenis de col·laboració amb institucions i
universitats o bé a demanda.
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        Més informació
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
                            <div class="featured-services-right">
                                <div class="b-services-slides-holder enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="false"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="true"
                                     data-mode="vertical"
                                     data-infinite-loop="true"
                                     data-auto="true"
                                     data-pause="5000"
                                     data-auto-hover="true"
                                     >
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Entitat formadora
                                                </h6>
                                                <p>
                                                    DAE manté un conveni de col·laboració amb les Universitats
i CFGS per tal d’acollir alumnes en concepte de pràctiques
i ofereix informació i orientació a l’alumnat dels
instituts del territori en els treballs de recerca.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Entitat assessora
                                                </h6>
                                                <p>
                                                    Des de tots els programes de l’entitat es realitzen assessoraments
personalitzats a professionals de la xarxa com
arallevadores, professorat, professionals de l’educació,
serveis socials, centres de salut mental, entitats i associacions,
espais cívics, etcètera.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="container-bg-additional relative clearfix">
            <div class="b-services-bg-filter customBgColor">
            </div>
            <div class="b-featured-services">
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 wow slideInLeft">
                            <div class="featured-services-left">
                                <div class="b-f-s-info">
                                    <h2 class="f-s-title">
                                        Gestió i
                                        <br>
                                        finançament
                                    </h2>
                                    <p>
                                        Dones Amb Empenta vetlla per la correcta administració
dels recursos econòmics i humans, amb l’objectiu de millorar
la qualitat dels serveis prestats, optimitzar els recursos i
garantir la transparència en la gestió.
                                    </p>
                                    <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        Més informació
                                    </a>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-11 col-sm-5 col-md-5 col-lg-5">
                            <div class="featured-services-right">
                                <div class="b-services-slides-holder enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="false"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="true"
                                     data-mode="vertical"
                                     data-infinite-loop="true"
                                     data-auto="true"
                                     data-pause="5000"
                                     data-auto-hover="true"
                                     >
                                    <div class="b-services-slide">
                                        <div class="b-services-item">                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Recursos econòmics
                                                </h6>
                                                <p>
                                                    Des de la seva creació, DAE col·labora estretament
amb diverses administracions públiques gestionant i
realitzant projectes de diferent caire mitjançant
subvencions, convenis i contractes.
                                                </p>
                                            </div>
                                        </div>
                                        <div class="b-services-item">
                                            
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                    Recursos humans
                                                </h6>
                                                <p>
                                                    Disposem d’un equip professional implicat a assolir
la missió, visió i valors de l’entitat.
                                                </p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        
    </section>
</div>
