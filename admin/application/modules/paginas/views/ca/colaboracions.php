<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/escala2.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Collaboracions') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>

                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Com ho fem') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Collaboracions')?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail colaboracio">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    
                    <?php foreach($this->elements->colaboradores()->result() as $e): ?>
                        <div class="b-mod-heading text-center wow fadeInDown">                        
                            <h2 class="heading-line line-right customColor customPseudoElBg">
                                <strong><?= $e->nombre ?></strong>
                            </h2>
                            <p class="second-heading font-additional">
                                <?= $e->descripcion ?>
                            </p>                       
                        </div>

                        <div class="row">
                            <?php foreach($e->fotos->result() as $f): if(!empty($f)): ?>
                                <div class="col-xs-6 col-sm-4 col-md-2">
                                    <a href="<?= $f->link ?>" target="_new"><img src="<?= $f->foto ?>" style="width:100%"></a>
                                </div>
                            <?php endif; endforeach ?>
                        </div>
                    <?php endforeach ?>


                </div>
            </div>
        </div>
    </section>
</div>
