<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/4.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Resultado de busqueda') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Resultado de busqueda') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12">
                	<?php foreach($resultados->result() as $r): ?>
	                    <div class="row">
	                    	<h2 style="margin-bottom: 0px; padding-bottom:10px;"><a href="<?= base_url($r->url.'/'.toUrl($r->id.'-'.$r->texto1)) ?>" style="color:#e5027d"><?= $r->texto1 ?></a></h2>
	                    	<p><?= strip_tags($r->texto2) ?></p>
	                    	<p><a href="<?= base_url($r->url.'/'.toUrl($r->id.'-'.$r->texto1)) ?>" style="color:#333; text-decoration: underline;"><?= l('Mès informaciò') ?></a></p>
	                    </div>
                	<?php endforeach ?>
                </div>
            </div>
        </div>
    </section>
</div>
