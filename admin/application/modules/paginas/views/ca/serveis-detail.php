<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/2.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b>EAD</b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="01_home-1.html"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="07_works.html"><?= l('què fem') ?> </a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('atenció') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                
                <div class="col-xs-12 col-sm-6">
                    <?= l('Menu') ?>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <?= l('Informacion') ?>
                </div>
        </div>
    </section>
</div>
