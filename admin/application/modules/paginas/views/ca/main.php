<div class="page-container">
    <section class="main-slider">
        <div class="slider-pro full-width-slider" id="main-slider"
            data-width="100%"
            data-height="800"
            data-fade="true"
            data-buttons="true"
            data-arrows="false"
            data-next-slide=".pro-next"
            data-previous-slide=".pro-prev"
            data-wait-for-layers="true"
            data-thumbnail-pointer="false"
            data-touch-swipe="false"
            data-autoplay="true"
            data-auto-scale-layers="true"
            data-visible-size="100%"
            data-force-size="fullWidth"
            data-autoplay-delay="5000"
            data-autoplay-on-hover="true"
            >
            <div class="sp-slides">
                <div class="sp-slide slide-1 b-home-big-text">
                    <img class="sp-image" src="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg"
                    data-src="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg"
                    data-retina="<?= base_url() ?>assets/template/media/home/home-sld-2.jpg" alt="/" />
                    <div class="slider-filter-holder">
                        <div class="cutBox cut-bottom">
                        </div>
                        <div class="custom-filter">
                            <div class="slider-icon">
                                <img src="<?= base_url() ?>img/slider-icon.png" alt="/">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="sp-layer layer-1-0 slide-tex-1 hidden-xs hidden-sm hidden-md"
                            data-vertical="25%"
                            data-horizontal="25%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <div class="rotate-block">
                                <p class="font-secondary text-uppercase">
                                    <?= l('Actuem i Transformem') ?>
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-1 slide-tex-1"
                            data-vertical="18%"
                            data-horizontal="53%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <h5 class="first-heading font-secondary">
                            <?= l('Treballem per a generar') ?>
                            </h5>
                        </div>
                        <div class="sp-layer layer-1-2 slide-tex-1"
                            data-vertical="22%"
                            data-horizontal="53%"
                            data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                            <h4 class="first-heading font-secondary">
                            <strong><?= l('canvis en la nostra realitat') ?></strong>
                            </h4>
                            <div class="b-text">
                                <p>
                                    <?= l('canvis_text') ?>
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-3 slide-button"
                            data-vertical="60%"
                            data-horizontal="54%"
                            data-show-transition="bottom" data-hide-transition="up" data-show-delay="1000">
                            <a href="<?= base_url() ?>servei/1-atencio-a-les-dones" class="btn btn-default-arrow btn-sm btn-clear">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                <?= l('llegir més') ?>
                            </a>
                        </div>
                    </div>
                </div>
                <div class="sp-slide slide-1 b-home-big-text">
                    <img class="sp-image" src="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg"
                    data-src="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg"
                    data-retina="<?= base_url() ?>assets/template/media/home/home-sld-1.jpg" alt="/" />
                    <div class="slider-filter-holder">
                        <div class="cutBox cut-bottom">
                        </div>
                        <div class="custom-filter">
                            <div class="slider-icon">
                                <img src="<?= base_url() ?>img/slider-icon.png" alt="/">
                            </div>
                        </div>
                    </div>
                    <div class="container">
                        <div class="sp-layer layer-1-0 slide-tex-1 hidden-xs hidden-sm hidden-md"
                            data-vertical="25%"
                            data-horizontal="28%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <div class="rotate-block">
                                <p class="font-secondary text-uppercase">
                                    <?= l('SABEM I INNOVEM') ?>
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-1 slide-tex-1"
                            data-vertical="18%"
                            data-horizontal="53%"
                            data-show-transition="top"
                            data-hide-transition="up"
                            data-show-delay="600"
                            data-hide-delay="100"
                            >
                            <h5 class="first-heading font-secondary">
                            <?= l('Tenim experiencia') ?>
                            </h5>
                        </div>
                        <div class="sp-layer layer-1-2 slide-tex-1"
                            data-vertical="22%"
                            data-horizontal="53%"
                            data-show-transition="left" data-hide-transition="up" data-show-delay="600" data-hide-delay="100">
                            <h4 class="first-heading font-secondary">
                            <strong><?= l('i la volem compartir') ?></strong>
                            </h4>
                            <div class="b-text">
                                <p>
                                   <?= l('ilabolemtext') ?>
                                </p>
                            </div>
                        </div>
                        <div class="sp-layer layer-1-3 slide-button"
                            data-vertical="60%"
                            data-horizontal="54%"
                            data-show-transition="bottom" data-hide-transition="up" data-show-delay="1000">
                            <a href="<?= base_url() ?>que-ens-mou.html" class="btn btn-default-arrow btn-sm btn-clear">
                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                <?= l('llegir més') ?>
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="section-home-primary" style="padding-bottom:0">

        <!-- Eventos -->
            <?php 
                $this->db->order_by('fecha','ASC');
                $eventos = $this->elements->getEventos(array('fecha >='=>date("Y-m-d"))); 
            ?>
            <?php if($eventos->num_rows()>0): ?>
                <div class="b-home-about relative">                
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 eventsHome">
                            
                            <h2 style="padding: 0;">
                                <img src="<?= base_url() ?>img/calendar.svg" style="width: 60px;">
                            </h2>
                            <h2><?= l('proximevent') ?></h2>
                            <div class="b-text">
                                <ul>
                                    <?php foreach($eventos->result() as $e): ?>
                                        <li><a href="<?= base_url() ?>events.html"><?= $e->tipo_evento ?> <span style=""><?= $e->fechaFormat ?>_</span> <b><?= $e->nombre ?></b></a></li>
                                    <?php endforeach ?>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        <?php endif ?>
       <!-- Eventos -->

        <div class="b-home-about relative">
            <div class="vertical-container-top vertical-left home-about-side-title">
                <span class="vertical-text-main color-add text-uppercase">dae</span>
                <span class="vertical-text-additional color-main text-uppercase"><?= l('Qui som') ?></span>
                <span class="vertical-number color-main font-primary pull-right">01</span>
            </div>
            <div class="container">
                <div class="row">
                    <div class="col-xs-12 col-sm-5 col-sm-offset-1" style="margin-bottom: 65px">
                        <div class="b-mod-heading wow fadeInDown">
                            <p class="first-heading font-secondary"><?= l('LAssociacio') ?></p>
                            <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong><?= l('Dones Amb Empenta') ?></strong>
                            </h2>
                        </div>
                        <div class="b-blockquote-holder">
                            <div class="b-blockquote-left-border">
                                <p class="quote-text">
                                    <?= l('elfeminisme') ?>
                                </p>
                                <p class="quote-author">
                                    <!-- <span class="ef icon_quotations quote-icon"></span> -->
                                    <span class="author-name"></span>
                                    <span class="author-role"></span>
                                </p>
                            </div>
                        </div>
                        <div class="b-text">
                            <p>
                                <a href="<?= base_url() ?>files/7d35c-memoria20anys.pdf" target="_new" class="btn btn-default-arrow btn-sm btn-clear">
                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                    <?= l('La nostra historia') ?>
                                </a>                                
                            </p>
                            <p>
                                <?php 
                                    $this->db->order_by('orden','ASC');
                                    $ultima = $this->db->get_where('multimedia',array('tipo'=>2));
                                    if($ultima->num_rows()>0):
                                ?>
                                    <a href="<?= base_url() ?>files/<?= $ultima->row()->url ?>" target="_new" class="btn btn-default-arrow btn-sm btn-clear" style="padding-right: 50px !important;">
                                        <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                        <?= l('Memoria 2017') ?>
                                    </a>
                                <?php endif ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 left-side wow slideInLeft">
                        <div class="img-brd-mod">
                            <div class="brd"></div>
                            <div class="img-cut">
                                <div class="cut"></div>
                                <img src="<?= base_url() ?>assets/template/media/home/home-about-1.jpg" class="img-responsive center-block" alt="/">
                            </div> 
                        </div>
                        
                    </div>
                </div>
            </div>
        </div>
        <div class="franja" style="height:0"></div>
        <div class="b-home-services" style="margin-bottom: 0;">
            <div class="container-fluid">
                <div class="b-services-holder clearfix" style="margin-bottom: 0;">
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/V.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                <?= l('DESIGUALTATS I VIOLENCIES') ?>
                                </h6>
                                <p>
                                <?= l('desitext') ?>  </p>
                            </p>
                            <p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item wow fadeInRight">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/p.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                <?= l('PERSPECTIVA DE GENERE') ?>
                                </h6>
                                <p>
                                    <?= l('perspective') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-6 col-sm-4">
                        <div class="b-services-item wow fadeInLeft">
                            <div class="services-icon">
                                <img src="<?= base_url() ?>assets/template/media/services-icons/a.png" alt="/">
                            </div>
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                <?= l('APODERAMENT') ?>
                                </h6>
                                <p>
                                    <?= l('apotext') ?> <p>
                                    </p> <br>
                                </div>
                            </div>
                        </div>
                        
                        <div class="col-xs-6 col-sm-4">
                            <div class="b-services-item wow fadeInRight">
                                <div class="services-icon">
                                    <img src="<?= base_url() ?>assets/template/media/services-icons/i.png" alt="/">
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <?= l('INTERSECCIONALITAT') ?>
                                    </h6>
                                    <p>
                                    <?= l('intertext') ?>  </p>
                                </div>
                            </div>
                        </div>


                        
                        <div class="b-home-services" style="margin-bottom:0">
                            <div class="container-fluid">
                                <div class="b-services-holder clearfix" style="margin-bottom:0">
                                    <div class="col-xs-6 col-sm-4">
                                        <div class="b-services-item">
                                            <div class="services-icon">
                                                <img src="<?= base_url() ?>assets/template/media/services-icons/f.png" alt="/">
                                            </div>
                                            <div class="services-text">
                                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                <?= l('INNOVACIO') ?>
                                                </h6>
                                                <p>
                                                    <?= l('inotext') ?><p>
                                                    </div>
                                                </div>
                                            </div>
                                            <div class="col-xs-6 col-sm-4">
                                                <div class="b-services-item wow fadeInRight">
                                                    <div class="services-icon">
                                                        <img src="<?= base_url() ?>assets/template/media/services-icons/comunity.png" alt="/">
                                                    </div>
                                                    <div class="services-text">
                                                        <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                                        <?= l('COMUNITAT') ?>
                                                        </h6>
                                                        <p>
                                                        <?= l('comutext') ?>  </p>
                                                    </div>
                                                </div>
                                            </div>
                                            
                                            
                                            
                                        </div>
                                    </div>
                                </div>

                                <div class="franja" style="height:0"></div>


                                <div class="b-home-works relative">
                                    <div class="vertical-container-top vertical-left home-works-side-title">
                                        <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                        <span class="vertical-text-additional color-main text-uppercase"><?= l('QUÈ FEM') ?></span>
                                        <span class="vertical-number color-main font-primary pull-right">02</span>
                                    </div>
                                    
                                    <div class="b-tabs-holder wow fadeInDown">
                                        <div class="container">
                                            <div class="row">
                                                <div class="col-xs-12 col-md-offset-1 col-md-2">
                                                    
                                                    
                                                    <ul class="sidebarmenuserveis2" style="margin-bottom: 30px;">
                                                        <?php
                                                        $this->db->order_by('servicios_categorias.orden','ASC');
                                                        foreach($this->db->get_where('servicios_categorias')->result() as $c):
                                                            $c = $this->traduccion->traducirObj($c);
                                                        ?>
                                                            <li><a class="fucsia" style="font-size:15px" href="#Serv<?= $c->id ?>" data-toggle="tab" aria-controls="all" role="tab" onclick="$('.servLis li, .servLis li a').removeClass('active'); $('#servLi<?= $c->id ?>').addClass('active');"><?= $c->nombre ?></a></li>
                                                        <?php endforeach ?>
                                                    </ul>
                                                </div>
                                                <div class="col-xs-12 col-md-8 col-md-offset-1">
                                                    
                                                    <div class="tab-content">
                                                        <!--- Comienzan servicios contnido --->
                                                        <?php
                                                            $this->db->order_by('servicios_categorias.orden','ASC');
                                                            foreach($this->db->get_where('servicios_categorias')->result() as $n=>$c):
                                                                $c = $this->traduccion->traducirObj($c);
                                                                $fotos = explode(',',$c->fotos);
                                                                if(empty($fotos[count($fotos)-1])){
                                                                array_pop($fotos);
                                                            }
                                                        ?>
                                                        
                                                        <div role="tabpanel" class="sliderWithoutOverflow tab-custom <?= $n==0?'active':'' ?>" id="Serv<?= $c->id ?>">
                                                            <div class="clearfix">
                                                                <div class="col-xs-12 col-sm-8 tab-img">
                                                                    <ul class="list-unstyled enable-bx-slider" data-counter="true" data-pager-custom="#bx-pager-all" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="555" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                                                                        <li>
                                                                            <div class="img-brd-mod">
                                                                                <div class="brd"></div>
                                                                                <div class="img-cut">
                                                                                    <div class="cut"></div>
                                                                                    <img src="<?= base_url() ?>img/servicios/<?= $fotos[0] ?>" class="img-responsive" alt="/">
                                                                                </div>
                                                                            </div>
                                                                        </li>
                                                                    </ul>
                                                                </div>
                                                                <div class="col-xs-12 col-sm-4">
                                                                    <div class="tab-caption">
                                                                        <div class="b-mod-heading">
                                                                            <!--    <p class="first-heading font-secondary">“És justícia i no caritat el que el món necessita” Mary Wollstonecraft</p> -->
                                                                            <h2 class="heading-line line-right customColor customPseudoElBg">
                                                                            <strong><?= $c->nombre ?></strong>
                                                                            </h2>
                                                                        </div>
                                                                        <p class="tab-caption-text">
                                                                            <?= $c->descripcion ?>
                                                                        </p>
                                                                        <a href="<?= base_url() ?>servei/<?= toUrl($c->id.'-'.$c->nombre) ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                            <?= l('llegir més') ?>
                                                                        </a>
                                                                    </div>
                                                                </div>
                                                                
                                                            </div>
                                                        </div>
                                                        <?php endforeach ?>
                                                    </div>
                                                    
                                                </div>

                                                <div class="col-xs-12 col-sm-12 hidden-xs" style="margin-top: 40px;">
                                                        <div class="b-member-list">
                                                            <div class="additional-images-thumbs">
                                                                <ul id="bx-pager-all" role="tablist" class="servLis pager-custom enable-bx-slider member-list-item" data-pager-custom="null" data-controls="false" data-min-slides="2" data-max-slides="6" data-slide-width="115" data-slide-margin="22" data-pager="false" data-mode="horizontal" data-infinite-loop="false">
                                                                    <?php
                                                                        $this->db->order_by('servicios_categorias.orden','ASC');
                                                                        foreach($this->db->get_where('servicios_categorias')->result() as $n=>$c):
                                                                            $c = $this->traduccion->traducirObj($c);
                                                                            $fotos = explode(',',$c->fotos);
                                                                            if(empty($fotos[count($fotos)-1]))
                                                                            array_pop($fotos);
                                                                    ?>

                                                                    <li id="servLi<?= $c->id ?>">
                                                                        <a href="#Serv<?= $c->id ?>" data-toggle="tab" aria-controls="all" role="tab">
                                                                            <div class="member-img noafter">
                                                                                <img src="<?= base_url() ?>img/servicios/<?= $fotos[0] ?>" alt="/" />
                                                                            </div>
                                                                        </a>
                                                                    </li>
    
                                                                    <?php endforeach ?>
                                                                        
                                                                    
                                                                </ul>
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <!--- Finaliza contenido ---->
                                            </div>
                                        </div>
                                    </div>
                                    
                                </div>

                                <div class="franja" style="height:0">
                                    
                                </div>
                                <div class="b-home-offers relative mainTerritori">
                                    <div class="container-bg-additional relative clearfix">
                                        <div class="b-services-bg-filter customBgColor">
                                        </div>
                                        <div class="vertical-container-top vertical-left home-offers-side-title">
                                            <span class="vertical-text-main color-white text-uppercase">DAE</span>
                                            <span class="vertical-text-additional color-white text-uppercase"><?= l('Territori') ?></span>
                                            <span class="vertical-number color-white font-primary pull-right">03</span>
                                        </div>
                                        <div class="b-featured-services">
                                            <div class="container">
                                                <div class="row">
                                                    <div class="col-xs-12 col-sm-offset-1 col-sm-5 col-md-5 col-lg-5">
                                                        
                                                    </div>
                                                    <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6 cuadro" style="">
                                                        <div class="featured-services-left wow slideInLeft">
                                                            <div class="b-f-s-info" style="">
                                                                <h2 class="f-s-title" style="">
                                                                <?= l('Territori') ?>
                                                                <br>
                                                                <?= l('dactuació') ?>
                                                                <br>
                                                                
                                                                </h2>
                                                                <p>
                                                                    <?= l('tertext') ?>
                                                                </p>
                                                                <a href="<?= base_url() ?>territoridactuacio.html" class="btn btn-default-arrow btn-sm btn-clear">
                                                                    <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                    <?= l('Allà on som') ?>
                                                                </a>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>





                                


                               



            

                                <div class="b-home-team relative equipoMain">
                                    <div>
                                        <div class="vertical-container-top vertical-left home-team-side-title">
                                            <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                            <span class="vertical-text-additional color-main text-uppercase"><?= l('Lequip') ?></span>
                                            <span class="vertical-number color-main font-primary pull-right">04</span>
                                        </div>
                                        <div class="container" style="background:url(<?= base_url() ?>assets/template/media/home/equipo.jpg); background-size:cover; margin-right: 0; width: 100%;">
                                            <div class="row">
                                                
                                                <div class="col-xs-12 col-sm-12 col-md-5 col-md-offset-1 colleft">
                                                    <div class="b-mod-heading wow fadeInDown">
                                                        <p class="first-heading font-secondary"></p>
                                                        <h2 class="heading-line line-right customColor customPseudoElBg">
                                                        <strong><?= l('Lequip de treball') ?></strong>
                                                        </h2>
                                                    </div>
                                                    <div class="b-blockquote-holder">
                                                        <div class="">
                                                            <p class="quote-text" style="font-size:17px">
                                                                <?= l('Persones compromeses amb el canvi.') ?>  
                                                            </p>
                                                            <p class="quote-author">
                                                                <!-- <span class="ef icon_quotations quote-icon"></span> -->
                                                                <span class="author-name"></span>
                                                                <span class="author-role"></span>
                                                            </p>
                                                        </div>
                                                    </div>
                                                    <div class="b-text">
                                                        <p>
                                                            <a href="<?= base_url() ?>lequip.html" class="btn btn-default-arrow btn-sm btn-clear">
                                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                <?= l('Coneix lequip') ?>
                                                            </a>                                
                                                        </p>
                                                    </div>
                                                </div>
                                                <div class="col-xs-12 col-sm-12 col-md-6 colright">
                                                    <div class="b-home-video-block">
                                                        <div class="container-fluid">
                                                            <div class="col-xs-12 left-side wow slideInLeft">
                                                                
                                                            </div>
                                                            <div class="col-xs-12 right-side wow slideInRight">
                                                                <div class="content">
                                                                    <div class="b-mod-heading">
                                                                        <p class="first-heading customPseudoElBg font-secondary"><?= l('Collabora io') ?> </p>
                                                                        <h2 class="heading-line line-right" style="margin:0; margin-bottom:40px;">
                                                                        <strong><?= l('Treballa amb nosaltres') ?> </strong>
                                                                        </h2>
                                                                        <div>
                                                                            <a href="<?= base_url() ?>treballar.html" class="btn btn-default-arrow btn-sm btn-clear">
                                                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                                <?= l('Veure ofertes de treball') ?>
                                                                            </a>
                                                                        </div>
                                                                    </div>
                                                                </div>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                                
                                            </div>

                                            
                                        </div>
                                    </div>
                                </div>




            
                                
                                
                                <div class="franja" style="height:0">
                                    
                                </div>
                                <div class="b-home-news relative">
                                    <div class="vertical-container-top vertical-left home-news-side-title">
                                        <span class="vertical-text-main color-add text-uppercase">dae</span>
                                        <span class="vertical-text-additional color-main text-uppercase"><?= l('notícies') ?></span>
                                        <span class="vertical-number color-main font-primary pull-right">05</span>
                                    </div>
                                    <div class="container">
                                        <?php 
                                            $this->db->limit(4);
                                            $blog = $this->elements->blog(array('blog_categorias_id'=>1)); 
                                        ?>
                                        <?php if($blog->num_rows()>0): ?>
                                        <div class="primary-home-news wow fadeInDown">
                                            <div class="row">
                                                <div class="b-blog-item clearfix">
                                                    <div class="col-xs-12 col-md-5 col-md-offset-1">
                                                        <div class="img-brd-mod">
                                                            <div class="brd"></div>
                                                            <div class="b-blog-img img-cut">
                                                                <div class="cut"></div>
                                                                <img src="<?= $blog->row(0)->foto ?>" class="img-responsive center-block" alt="/">
                                                            </div>
                                                        </div>
                                                    </div>
                                                    <div class="col-xs-12 col-md-6">
                                                        <div class="b-blog-caption">
                                                            <div class="caption-data customPseudoElBg">
                                                                <span class="date"><?= strftime("%d-%b-%Y",strtotime($blog->row(0)->_fecha)) ?></span>                                                                
                                                            </div>
                                                            <h4 class="caption-title">
                                                            <?= $blog->row(0)->titulo ?>
                                                            </h4>
                                                            <div class="caption-preview-text">
                                                                <?= cortar_palabras(strip_tags($blog->row(0)->texto),80) ?>
                                                            </div>
                                                            <a href="<?= base_url('blog/'.toUrl($blog->row(0)->id.'-'.$blog->row(0)->titulo)) ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                                                <?= l('llegir més') ?>
                                                            </a>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <?php endif ?>
                                        <div class="secondary-home-news">
                                            <div class="row">
                                                <?php for($i=1; $i<$blog->num_rows(); $i++): ?>
                                                <div class="col-xs-4 col-sm-4 wow fadeInLeft">
                                                    <a href="<?= $blog->row($i)->link ?>">
                                                        <div class="b-blog-item clearfix" style="background:url('<?= $blog->row($i)->foto ?>'); background-size:cover; ">
                                                            <div class="b-blog-caption">
                                                                <div class="caption-data customPseudoElBg" style="text-transform: uppercase;">
                                                                    <span class="date"><?= strftime("%d-%b-%Y",strtotime($blog->row($i)->_fecha)) ?></span>                                                                    
                                                                </div>
                                                                <h4 class="caption-title">
                                                                <?= $blog->row($i)->titulo ?>
                                                                </h4>
                                                            </div>
                                                        </div>
                                                    </a>
                                                </div>
                                                <?php endfor ?>
                                            </div>
                                            
                                        </div>
                                        <div style="text-align:center;margin-top: 50px;">
                                            <a href="<?= base_url('noticies') ?>" class="btn btn-default-arrow btn-sm btn-clear">
                                                <i class="fa fa-long-arrow-right" aria-hidden="true"></i><?= l('Veure totes les notícies') ?>
                                            </a>
                                        </div>
                                    </div>
                                </div>
                                <div class="franja" style="height:0">
                                    
                                </div>
                                <div class="b-home-about relative bglight redes">
                                    <div class="vertical-container-top vertical-left home-about-side-title" style="top: 280px;">
                                        <span class="vertical-text-main color-add text-uppercase">DAE</span>
                                        <span class="vertical-text-additional color-main text-uppercase"><?= l('xarxes') ?></span>
                                        <span class="vertical-number color-main font-primary pull-right">06</span>
                                    </div>
                                    <div class="container">
                                        <div class="row">
                                            <div class="col-xs-12 col-sm-11 col-sm-offset-1">
                                                <ul id="portfolio-filter" class="filter list-inline b-items-sort" data-related-grid="portfolio-grid">
                                                    <li class="active-filt" data-filter="*" id="allSocial">                                                        
                                                        <i class="fa fa-globe fa-2x"></i> <br/>
                                                        <?= l('Totes') ?>
                                                    </li>
                                                    <li data-filter=".instagram" href="#" title="Feed Instagram">
                                                        <i class="fa fa-2x fa-instagram"></i><br>
                                                        Instagram
                                                    </li>
                                                    <li data-filter=".twitter" href="#" title="Feed Twitter">
                                                        <i class="fa fa-2x fa-twitter"></i><br>
                                                        Twitter
                                                    </li>
                                                    <li data-filter=".facebook" href="#" title="Feed Facebook">
                                                        <i class="fa fa-2x fa-facebook-square"></i><br/>
                                                        Facebook
                                                    </li>
                                                </ul>
                                                <div id="portfolio-grid" class="social-feed-container isotope-grid gallery-container style-column-4 isotope-spaced clearfix"></div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </section>
                        </div>
                        
                    </section>

<?php if(!empty($this->db->get('ajustes')->row()->popup)): ?>
    <script>        
        window.onload = function(){
            jQuery.get('paginas/frontend/popup',function(data){
                    $("body").append(data);
                    $("body").find('#popup').modal('show');                                    
            });
        };
    </script>
<?php endif ?>