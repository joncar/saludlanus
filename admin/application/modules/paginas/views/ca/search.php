<style>
    .buscado a {
    color: #e5027d !important;
    text-decoration: none;
    cursor: pointer;
}

.ZINbbc{
  margin-bottom: 30px !important;
}

.g .r{
    padding: 0 !important;
  margin: 0 !important;
}
</style>
<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/buscador.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Resultats de la búsqueda') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Resultats de la búsqueda') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 buscado">                	
					<?= $resultado ?>
                </div>
            </div>
        </div>
    </section>
</div>
