<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/eventsBg.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Events') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Contacte') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Events') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>

    <section class="section-blog trabajo">
		<div class="container">
			<div class="row trabajo">
				
				<?php 
					$this->db->order_by('fecha','DESC');
					foreach($this->elements->getEventos()->result() as $o): 
				?>
					<div class="b-blog-item clearfix wow slideInLeft">
						<div class="b-blog-data text-right hidden-xs">
							<span class="blog-data-category font-secondary text-uppercase"><?= $o->tipo_evento ?></span>
							<span class="blog-data-date font-primary text-uppercase"><?= $o->fechaabr ?></span>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="b-blog-img img-cut">
								<div class="cut"></div>
								<img src="<?= $o->foto ?>" class="img-responsive center-block" alt="/">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="b-blog-caption">

								<div class="caption-data customPseudoElBg">
									<span class="date"><?= $o->fecha ?></span>								
								</div>
								<h4 class="caption-title">
									<?= $o->nombre ?>
								</h4>
								<div class="caption-preview-text">
									<?= cortar_palabras(strip_tags($o->descripcion),10) ?>
								</div>
								<div class="caption-preview-text">																		
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  
										  <div class="panel panel-default trabajo">
											    <div class="panel-heading" role="tab" id="headingOne">
											      <h4 class="panel-title">
											        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $o->id ?>" aria-expanded="true" aria-controls="collapse<?= $o->id ?>">
											          <?= ucfirst(l('llegir més')) ?>
											        </a>
											      </h4>
											    </div>
											    <div id="collapse<?= $o->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											      <div class="panel-body">
											        <?php if(!empty($o->lugar)): ?>
												        <div class="row">												
															<div class="col-xs-6 col-md-4">
																<b><?= l('Lloc') ?>: </b>
															</div>
															<div class="col-xs-6 col-md-8">
															  <?= $o->lugar ?>
															</div>
														</div>	
													<?php endif ?>													
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Tipo de evento') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->tipo_evento ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('descripcion') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->descripcion ?>
														</div>
													</div>													
											      </div>
											    </div>
											  </div>
										  </div>
										</div>






								</div>
								<?php if($o->admitir_inscripciones): ?>
									<a href="javascript:ofertar('<?= $o->id ?>','<?= str_replace("'","\\'",$o->nombre) ?>')" class="btn btn-default-arrow btn-sm btn-clear">
										<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
										<?= l('Inscriu-te') ?>
									</a>
								<?php endif ?>
							</div>
						</div>
					</div>

					
				<?php endforeach ?>
		</div>
	</section>

</div>

<div id="ofertar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form action="paginas/frontend/eventos" onsubmit="return sendForm(this,'#response');">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" style="padding: 0 !important;"><?= l('Inscriures a') ?> <span class="tituloOferta"></span></h4>
	      </div>
	      <div class="modal-body">
	        
	        <div class="form-group">
			    <label for="nombre"><?= l('Nom i cognom') ?></label>
			    <input type="text" class="form-control" name="name" id="nombre" placeholder="<?= l('Nom i cognom') ?>">
		    </div>

		    <div class="form-group">
			    <label for="telefono"><?= l('Telèfon') ?></label>
			    <input type="text" class="form-control" name="telefono" id="telefono" placeholder="<?= l('Telèfon') ?>">
		    </div>

		    <div class="form-group">
			    <label for="email"><?= l('Correu electrònic') ?></label>
			    <input type="email" class="form-control" name="email" id="email" placeholder="<?= l('Correu electrònic') ?>">
		    </div>
			
		    
		    <div class="checkbox">
		    	<label for="">
		    		
		    		<?= l('politicas-treballar') ?><br/><br/>
		    		<input type="checkbox" value="1" name="politicas">
		    		<?= l('treballar-btn-politicas') ?>
		    	</label>
		    </div>
		    <div id="response"></div>
		    <input type="hidden" id="tituloInp" name="nombre" value="0">
		    <input type="hidden" id="idOferta" name="eventos_id" value="0">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default-arrow btn-sm" data-dismiss="modal"><?= l('Tancar') ?></button>
	       	<button type="submit" class="btn btn-default-arrow btn-sm"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <?= l('Enviar') ?></button>
	      </div>
	    </div><!-- /.modal-content -->
    </form>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="politicas2" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">    
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" style="padding: 0 !important;"> <span class="tituloOferta"><?= l('Política de Privacitat') ?></span></h4>
	      </div>
	      <div class="modal-body">
	      	<?= l('politicas-treballar') ?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default-arrow btn-sm" data-dismiss="modal"><?= l('Tancar') ?></button>	       	
	      </div>
	    </div><!-- /.modal-content -->    
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	function ofertar(id,nombre){
		$("#ofertar .tituloOferta").html(nombre);
		$("#ofertar #tituloInp").val(nombre);
		$("#ofertar #idOferta").val(id);
		$("#ofertar").modal('toggle');
	}
</script>