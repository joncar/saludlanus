<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/16.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Transparència') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('com_ho_fem') ?> </a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Transparència') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-mod-heading text-center wow fadeInDown">
                        <p class="first-heading font-secondary"></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong><?= l('Entitat transparent') ?></strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('entitext') ?>
                        </p>
                    </div>

                    <div class="b-items-works text-uppercase text-center clearfix">
                        <ul class="list-inline b-items-sort">
                            <li>
                                
                            </li>                            
                        </ul>
                    </div>
                </div>
                <div class="col-xs-12">
                    <div class="row" style="margin-left: 0; margin-right: 0">
                        <?php            
                            $this->db->order_by('orden','ASC');             
                            $pdf = $this->db->get_where('transparencia',array('destino'=>1));
                            if($pdf->num_rows()>0):
                            foreach($pdf->result() as $p): 
                                $p = $this->traduccion->traducirObj($p);
                        ?>
                            <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12 clearfix wow zoomIn docPdfItem" data-wow-delay="0.3s" style="visibility: visible; animation-delay: 0.3s; animation-name: zoomIn;">
                                <div class="demopage-preview_item">
                                    <a href="<?= empty($p->url)?base_url('files/'.$p->fichero):$p->url ?>" target="_new" class="demopage-preview_inner">
                                        <img class="noscroll" src="<?= base_url('img/servicios/'.$p->foto) ?>" alt="Preview">
                                    </a>
                                </div>

                                <div style="text-align: center;">
                                    <h3 class="font-additional font-weight-bold" style="margin-bottom: 0;padding-bottom: 0;padding-top: 40px;">
                                        <?= $p->nombre ?>
                                    </h3>
                                    <p style="margin: 0; margin-bottom:20px"><?= $p->subtitulo ?></p>
                                    <a href="<?= empty($p->url)?base_url('files/'.$p->fichero):$p->url ?>" target="_new" class="btn btn-default-arrow btn-sm btn-clear"><?= l('Veure') ?></a>
                                </div>
                            </div>
                        <?php endforeach ?>
                        <?php else: ?>
                            <?= l('Sin dates a mostrar en este momento.') ?>
                        <?php endif ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
</div>
