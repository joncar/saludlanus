<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/9.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Qui_som') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= base_url() ?>"><?= l('home') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Qui_som') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>

    <section class="section-about" id='filosofia'>
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-6">
                    <div class="b-mod-heading wow fadeInDown">
                        <p class="first-heading font-secondary"><?= l('Dones_Empenta')?></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong>DAE</strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('D_A_E') ?>
                        </p>
                    </div>
                </div>
                <div class="col-xs-12 col-sm-6">
                    <div class="img-brd-mod">
                        <div class="brd"></div>
                        <div class="img-cut">
                            <div class="cut"></div>
                            <img src="<?= base_url() ?>assets/template/media/about/about1.jpg" class="img-responsive center-block" alt="/">
                        </div>
                    </div>
                </div>
            </div>
            <div class="row">
                <div class="b-services-holder clearfix">
                    <div class="col-xs-12 col-sm-6 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <i class="demo-icon icon-facts-forma1" style="font-size:56px"></i><?= l('Objectius') ?>
                                </h6>
                                <p>
                                    <?= l('objectiu general') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <i class="demo-icon icon-facts-forma1" style="font-size:56px"></i><?= l('Missio') ?>
                                </h6>
                                <p>
                                   <?= l('articular_acciones') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            
            <div class="row">
                <div class="b-services-holder clearfix">
                    <div class="col-xs-12 col-sm-6 wow slideInLeft">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <i class="demo-icon icon-facts-forma1" style="font-size:56px"></i><?= l('Visio') ?>
                                </h6>
                                <p>
                                   <?= l('serveis_atencio') ?>         
                                </p>
                            </div>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 wow slideInRight">
                        <div class="b-services-item">
                            <div class="services-text">
                                <h6 class="services-title customColor customPseudoElBg font-secondary text-uppercase">
                                    <i class="demo-icon icon-facts-forma1" style="font-size:56px"></i><?= l('Valors') ?>
                                </h6>
                                <p>
                                    <?= l('valors_feminista') ?> 
                                    <?= l('lista_valors_feminista') ?>
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        

        <div class="container-bg-additional relative clearfix" id='territorideactuacio'>
            <div class="b-services-bg-filter customBgColor">
            
            </div>
            <div class="b-featured-services">
                    <div class="container">
                        <div class="b-home-offers relative">
            <div class="container-bg-additional relative clearfix">
                <div class="b-services-bg-filter customBgColor">
                </div>
                <div class="vertical-container-top vertical-left home-offers-side-title hidden-md">
                    <span class="vertical-text-main color-white text-uppercase">DAE</span>
                    <span class="vertical-text-additional color-white text-uppercase"><?= l('Territori') ?></span>
                    <span class="vertical-number color-white font-primary pull-right">03</span>
                </div>
                <div class="b-featured-services">
                    <div class="container">
                        <div class="row">
                            <div class="col-xs-12 col-sm-6 col-md-6 col-lg-6">
                                
                            </div>
                            <div class="col-xs-11 col-sm-6 col-md-6 col-lg-6" style="margin-left:-60px">

                                <div class="featured-services-left wow slideInLeft">
                                    <div class="b-f-s-info">
                                        <h2 class="f-s-title" style="font-size:50px">
                                            <?= l('territoriactuacio') ?>
                                          
                                        </h2>
                                        <p>
                                            <?= l('territoriactuaciotext') ?>
                                        </p>
                                        <a href="javascript:void(0);" class="btn btn-default-arrow btn-sm">
                                            <i class="fa fa-long-arrow-right" aria-hidden="true"></i>
                                            <?= l('puntoatencio') ?>
                                        </a>
                                    </div>
                                </div>

                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
                    </div>
                
        </div>
        </div>
       <div class="b-team" id='lequip'>                
                <div class="container">
                    <div class="row">
                        <div class="col-xs-12 col-sm-6 col-md-6 wow fadeInLeft">
                            <div class="row">
                                <div id="bx-team-page" class="b-member-list enable-bx-slider"
                                     data-pager-custom="null"
                                     data-controls="true"
                                     data-min-slides="1"
                                     data-max-slides="1"
                                     data-slide-width="0"
                                     data-slide-margin="0"
                                     data-pager="false"
                                     data-mode="horizontal"
                                     data-infinite-loop="false"
                                     >
                                    <div class="member-list-item clearfix">
                                        <a data-slide-index="0" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-1.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="1" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-2.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="2" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-3.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="3" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-4.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                    <div class="member-list-item clearfix">
                                        <a data-slide-index="4" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-2.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="5" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-4.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="6" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-1.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                        <a data-slide-index="7" href="01_home-1.html#">
                                            <div class="col-xs-6 col-sm-6 text-center">
                                                <div class="member-img">
                                                    <img src="<?= base_url() ?>assets/template/media/about/team/member-3.jpg" alt="/">
                                                </div>
                                            </div>
                                        </a>
                                    </div>
                                </div>
                                <div class="bx-outside-controls">
                                    <p>
                                        <span id="bx-prev"></span><span id="bx-next"></span>
                                    </p>
                                </div>
                            </div>
                        </div>
                        <div class="col-xs-12 col-sm-6 col-md-6 wow fadeInRight">
                            <div class="b-member-info-slide enable-bx-slider" data-pager-custom="#bx-team-page" data-controls="false" data-min-slides="1" data-max-slides="1" data-slide-width="0" data-slide-margin="0" data-pager="true" data-mode="horizontal" data-infinite-loop="false">
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof"><?= l('Directora') ?></p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Mar Granados</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary"><?= l('Perfil') ?></h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">Funcions</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Directora</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Nati Veraguas</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">funcions</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Gestió</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Mariajose Galera</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">funcions</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Formadora</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Denis Jiménez</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">funcions</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Designer</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Martin Hill</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">profile</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Designer</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Martin Hill</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">profile</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Designer</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Martin Hill</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">profile</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="b-member-info">
                                    <div class="b-mod-heading">
                                        <p class="first-heading font-secondary member-prof">Designer</p>
                                        <h2 class="heading-line line-right customColor customPseudoElBg member-name">
                                            <strong>Martin Hill</strong>
                                        </h2>
                                        <div class="member-socials">
                                            <ul class="list-inline">
                                                <li>
                                                    <a href="https://twitter.com/">
                                                        <i class="fa fa-twitter fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.facebook.com/">
                                                        <i class="fa fa-facebook fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                                <li>
                                                    <a href="https://www.behance.net/">
                                                        <i class="fa fa-behance fa-fw" aria-hidden="true"></i>
                                                    </a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">profile</h6>
                                        <p>
                                            Anim idm est laborum sed ut perspiciatis unde omnis iste natus ervolsau
                                            ptatem acsit cusa ntium doloremque laudantium totam incididunt sed lab
                                            dolore magna aliqua enim ad minim veniam quis.
                                        </p>
                                    </div>
                                    <div class="b-member-caption">
                                        <h6 class="contact-info-title customPseudoElBg font-secondary">perfil</h6>
                                        <div class="b-member-progress">
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 100%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    photoshop cc
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 85%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    html5 / css3
                                                </span>
                                            </div>
                                            <div class="progress progress-label2">
                                                <div class="bar-holder">
                                                    <div class="progress-bar" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style="width: 92%; min-width: 2em;"></div>
                                                </div>
                                                <span class="prog-title">
                                                    wordpress
                                                </span>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
                <div class="row" id='collaboradors'>
            <div class="col-xs-12 col-sm-12">
                <div class="b-mod-heading text-center">
                    <p class="first-heading font-secondary wow fadeInDown" style="visibility: visible; animation-name: fadeInDown;">L'entitat en xifres</p>
                </div>
            </div>
        </div>
        
        
        
        
        <div class="b-about-facts">            
            <div class="container-fluid">
                <div class="col-xs-6 col-sm-4">
                    <div class="about-facts-item">
                        <div class="facts-content">
                            <div class="facts-icon">
                                <i class="demo-icon icon-facts-clients"></i>
                            </div>
                            <div class="facts-caption chart" data-percent="31">
                                <p class="font-primary number percent"></p>
                                <p class="font-secondary text-uppercase customPseudoElBg">
                                    nombre de professionals 2016
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="about-facts-item">
                        <div class="facts-content">
                            <div class="facts-icon">
                                <i class="demo-icon icon-facts-awards"></i>
                            </div>
                            <div class="facts-caption chart" data-percent="512118,68">
                                <p class="font-primary number percent"></p>
                                <p class="font-secondary text-uppercase customPseudoElBg">
                                    volum econòmic 2016
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="col-xs-6 col-sm-4">
                    <div class="about-facts-item">
                        <div class="facts-content">
                            <div class="facts-icon">
                                <i class="demo-icon icon-facts-forma1"></i>
                            </div>
                            <div class="facts-caption chart" data-percent="5774">
                                <p class="font-primary number percent"></p>
                                <p class="font-secondary text-uppercase customPseudoElBg">
                                    Persones ateses
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>




</div>
