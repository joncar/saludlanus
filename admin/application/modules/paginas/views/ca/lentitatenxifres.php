<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/10.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('lentitat') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><a href="#"><?= l('com_ho_fem') ?> </a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('lentitat') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>
    <section class="section-work-detail">
        <div class="container">
            <div class="row">
                <div class="col-xs-12 col-sm-12">
                    <div class="b-mod-heading text-center wow fadeInDown">
                        <p class="first-heading font-secondary"><?= l('lesxifres') ?></p>
                        <h2 class="heading-line line-right customColor customPseudoElBg">
                            <strong><?= l('elsfan') ?></strong>
                        </h2>
                        <p class="second-heading font-additional">
                            <?= l('elsfanstext') ?>
                            
                        </p>
                    </div>               
                </div>

                <?php 
                    $this->db->order_by('anio','DESC');
                    foreach($this->db->get_where('xifres')->result() as $d): 
                ?>
                    <div class="b-about-facts">            
                        <div class="container-fluid">
                            <div class="col-xs-6 col-sm-4">
                                <div class="about-facts-item">
                                    <div class="facts-content">
                                        <div class="facts-icon">
                                            <i class="demo-icon icon-users"></i>
                                        </div>
                                        <div class="facts-caption chart" data-percent="<?= $d->profesionales ?>">
                                            <p class="font-primary">
                                                <span class="number percent"></span>
                                            </p>
                                            <p class="font-secondary text-uppercase customPseudoElBg">
                                                <?= str_replace('2017',$d->anio,l('nprofesionales')) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="about-facts-item">
                                    <div class="facts-content">
                                        <div class="facts-icon">
                                            <i class="demo-icon icon-wallet"></i>
                                        </div>
                                        <div class="facts-caption chart" data-percent="<?= substr($d->volumen_economico,0,3) ?>">
                                            <p class="font-primary">
                                                <span class="number percent"></span>.<?= substr($d->volumen_economico,3) ?>€
                                            </p>
                                            <p class="font-secondary text-uppercase customPseudoElBg">
                                                <?= str_replace('2017',$d->anio,l('volum')) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="col-xs-6 col-sm-4">
                                <div class="about-facts-item">
                                    <div class="facts-content">
                                        <div class="facts-icon" style=" transform: rotate(-45deg)">
                                            <i class="demo-icon icon-handshake-o"></i>
                                        </div>
                                        <div class="facts-caption chart" data-percent="<?= substr($d->personas_atendidas,0,1) ?>">
                                            <p class="font-primary">
                                                <span class="number percent"></span>.<?= substr($d->personas_atendidas,1) ?>
                                            </p>
                                            <p class="font-secondary text-uppercase customPseudoElBg">
                                                <?= str_replace('2017',$d->anio,l('Persones ateses 2017')) ?>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <?php endforeach ?>
                </div>

            </div>
    </section>
</div>
