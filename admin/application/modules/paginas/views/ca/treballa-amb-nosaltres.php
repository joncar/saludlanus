<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/46.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('Treballa amb nosaltres') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Contacte') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Treballa amb nosaltres') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>

    <section class="section-blog trabajo">
		<div class="container">
			<div class="row trabajo">
				
				<?php foreach($this->elements->getOfertas()->result() as $o): ?>
					<div class="b-blog-item clearfix wow slideInLeft">
						<div class="b-blog-data text-right hidden-xs">
							<span class="blog-data-category font-secondary text-uppercase"><?= $o->direccion ?></span>
							<span class="blog-data-date font-primary text-uppercase"><?= $o->fechaabr ?></span>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="b-blog-img img-cut">
								<div class="cut"></div>
								<img src="<?= $o->foto ?>" class="img-responsive center-block" alt="/">
							</div>
						</div>
						<div class="col-xs-12 col-sm-6">
							<div class="b-blog-caption">
								<div class="caption-data customPseudoElBg">
									<span class="date"><?= $o->fecha ?></span>								
								</div>
								<h4 class="caption-title">
									<?= $o->titulo ?>
								</h4>
								<!<div class="caption-preview-text">
									<?php /*strip_tags($o->titulo)*/ ?>
									&nbsp;
								</div>
								<div class="caption-preview-text">
									<?= $o->funciones ?>
									

	
									<div class="panel-group" id="accordion" role="tablist" aria-multiselectable="true">
					  
										  <div class="panel panel-default trabajo">
											    <div class="panel-heading" role="tab" id="headingOne">
											      <h4 class="panel-title">
											        <a role="button" data-toggle="collapse" data-parent="#accordion" href="#collapse<?= $o->id ?>" aria-expanded="true" aria-controls="collapse<?= $o->id ?>">
											          <?= ucfirst(l('llegir més')) ?>
											        </a>
											      </h4>
											    </div>
											    <div id="collapse<?= $o->id ?>" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne">
											      <div class="panel-body">
											        <div class="row">												
														<div class="col-xs-6 col-md-4">
															<b><?= l('Lloc') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->lugar ?>
														</div>
													</div>														
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Número de vacants') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->nro_vacantes ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Àrea') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->area ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Horari') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->horario ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Sou') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->salario ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Tipus de contracte') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->tipo_contrato ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Durada contracte') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->duracion ?>
														</div>
													</div>												
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Data Incorporació') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->fecha_incorporacion ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Titulació') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->titulacion ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Imprescindible') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->imprescindible ?>
														</div>
													</div>
													<div class="row">
														<div class="col-xs-6 col-md-4">
															<b><?= l('Es valorarà') ?>: </b>
														</div>
														<div class="col-xs-6 col-md-8">
														  <?= $o->valoracion ?>
														</div>
													</div>
											      </div>
											    </div>
											  </div>
										  </div>
										</div>






								</div>
								<a href="javascript:ofertar('<?= $o->id ?>','<?= $o->titulo ?>')" class="btn btn-default-arrow btn-sm btn-clear">
									<i class="fa fa-long-arrow-right" aria-hidden="true"></i>
									<?= l('Inscriu-te') ?>
								</a>
							</div>
						</div>
					</div>

					
				<?php endforeach ?>
		</div>
	</section>

</div>

<div id="ofertar" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">
    <form action="paginas/frontend/oferta_trabajo" onsubmit="return sendForm(this,'#response');">
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" style="padding: 0 !important;"><?= l('Inscriures a') ?> <span class="tituloOferta"></span></h4>
	      </div>
	      <div class="modal-body">
	        
	        <div class="form-group">
			    <label for="nombre"><?= l('Nom i cognom') ?></label>
			    <input type="text" class="form-control" name="nombre" id="nombre" placeholder="Nom i cognom">
		    </div>

		    <div class="form-group">
			    <label for="telefono"><?= l('Telèfon') ?></label>
			    <input type="text" class="form-control" name="telefono" id="telefono" placeholder="Telèfon">
		    </div>

		    <div class="form-group">
			    <label for="email"><?= l('Correu electrònic') ?></label>
			    <input type="email" class="form-control" name="email" id="email" placeholder="Correu electrònic">
		    </div>
		    <div class="form-group">
			    <label for="email"><?= l('mensaje') ?></label>
			    <textarea class="form-control" name="mensaje" id="mensaje" placeholder="<?= l('treballar_message') ?>"></textarea>
		    </div>

		    <div class="form-group">
			    <label for="archivo"><?= l('Arxiu Adjunt') ?></label>
			    <input type="file" name="adjunto" value="" id="archivo">
		    </div>
			
		    
		    <div class="checkbox">
		    	<label for="">
		    		
		    		<?= l('politicas-treballar') ?><br/><br/>
		    		<input type="checkbox" value="1" name="politicas">
		    		<?= l('treballar-btn-politicas') ?>
		    	</label>
		    </div>
		    <div id="response"></div>
		    <input type="hidden" id="tituloInp" name="titulo" value="0">
		    <input type="hidden" id="idOferta" name="ofertas_id" value="0">
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default-arrow btn-sm" data-dismiss="modal"><?= l('Tancar') ?></button>
	       	<button type="submit" class="btn btn-default-arrow btn-sm"><i class="fa fa-long-arrow-right" aria-hidden="true"></i> <?= l('Enviar') ?></button>
	      </div>
	    </div><!-- /.modal-content -->
    </form>
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->

<div id="politicas2" class="modal fade" tabindex="-1" role="dialog">
  <div class="modal-dialog" role="document">    
	    <div class="modal-content">
	      <div class="modal-header">
	        <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
	        <h4 class="modal-title" style="padding: 0 !important;"> <span class="tituloOferta"><?= l('Política de Privacitat') ?></span></h4>
	      </div>
	      <div class="modal-body">
	      	<?= l('politicas-treballar') ?>
	      </div>
	      <div class="modal-footer">
	        <button type="button" class="btn btn-default-arrow btn-sm" data-dismiss="modal"><?= l('Tancar') ?></button>	       	
	      </div>
	    </div><!-- /.modal-content -->    
  </div><!-- /.modal-dialog -->
</div><!-- /.modal -->
<script>
	function ofertar(id,nombre){
		$("#ofertar .tituloOferta").html(nombre);
		$("#ofertar #tituloInp").val(nombre);
		$("#ofertar #idOferta").val(id);
		$("#ofertar").modal('toggle');
	}
</script>