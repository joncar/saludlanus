<div class="page-container">
    <div class="breadcrumbs-header paralax" style="background-image: url('<?= base_url() ?>assets/template/media/filter-bg/map.jpg');">
        <ul class="half-filter">
            <li class="dark-filter relative">
                <h1 class="pull-right color-1 text-right"><b><?= l('territoridactuacion') ?></b></h1>
                <ul class="breadcrumbs pull-right">
                    <li><a href="<?= site_url() ?>"><?= l('inici') ?></a></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('Qui som') ?></span></li>
                    <li><span class="separate">/</span></li>
                    <li><span><?= l('territoridactuacion') ?></span></li>
                </ul>
                <div class="cutBox cut-bottom"></div>
            </li>
            <li class="custom-filter"></li>
        </ul>
    </div>


   

    <section class="section-work-detail territorioPage">
        <div class="">

            <div class="container" style="margin-bottom: 40px">
                <div class="row">
                    <div class="col-xs-12 col-sm-6">
                        <div class="b-mod-heading wow fadeInDown">
                            <p class="first-heading font-secondary">DAE</p>
                            <h2 class="heading-line line-right customColor customPseudoElBg">
                                <strong><?= l('Zones de treball') ?></strong>
                            </h2>
                            <p class="second-heading font-additional">
                                <?= l('zonatext') ?>
                            </p>
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6">
                        <div class="img-brd-mod">
                            <div class="brd"></div>
                            <div class="img-cut">
                                <div class="cut"></div>
                                <img src="<?= base_url() ?>theme/media/about/map-territori.jpg" class="img-responsive center-block" alt="/">
                            </div>
                        </div>
                    </div>
                </div>
            </div>


             <div id="territoriactuaciomapa" class="row container-bg-additional relative clearfix" style="position: relative; ">
                 <div class="col-xs-12 col-md-6 hidden-xs hidden-sm" style="position:relative; background: url(<?= base_url() ?>assets/template/media/filter-bg/mapa2.jpg) no-repeat; margin-bottom: -18px;">
                    <?php echo file_get_contents(base_url().'img/catalunya.svg'); ?>
                    <div class="featured-services-left wow slideInLeft" style="position: absolute;top: 16%;width: 80%;right: -10%">
                        <div class="b-f-s-info" style="border: 6px solid #e5027d;">
                            <h2 class="f-s-title">
                                <?= l('Allà on som') ?>
                            </h2>
                            <h2 class="f-s-title f-s-title2" id="regionsLabels"></h2>
                        </div>
                    </div>
                </div>
                <div class="col-xs-12 col-md-6" style="padding-top:120px; padding-left:80px">
                    <div class="featured-services-right wow fadeIn" style="margin-top: 0">
                        <div class="b-work-steps">
                            

                            <div class="b-services-item" data-regions="[anoia,bages]" data-label="Anoia, Bages">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            1.
                                        </span>
                                        <?= l('Atenció a les dones2') ?>
                                        <div class="visible-xs visible-sm">
                                            <span style="font-size: 10px;margin-left: 16px;">Anoia, Bages</span>
                                        </div>
                                    </h6>                                                
                                </div>
                            </div>
                            <div class="b-services-item" data-regions="[bages,path4070,zona_2_6_,zona_2_3_,zona_2_2_,zona_2,zona_2_1_,zona_2_4_,zona_2_5_,path4018,anoia,osona]"  data-label="<?= l('Comarques de Lleida i Catalunya Central') ?>">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            2.
                                        </span>
                                        <?= l('Atenció espec. en viólència masclista') ?>
                                        <div class="visible-xs visible-sm"><span style="font-size: 10px;margin-left: 16px;"><?= l('Comarques de Lleida i Catalunya Central') ?></span></div>
                                    </h6>                                                
                                </div>
                            </div>
                            <div class="b-services-item"  data-regions="[anoia,path4070]" data-label="<?= l('Anoia i Osona') ?>">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            3.
                                        </span>
                                        <?= l('Atencifam') ?>
                                        <div class="visible-xs visible-sm"><span style="font-size: 10px;margin-left: 16px;"><?= l('Anoia i Osona') ?></span></div>
                                    </h6>                                                
                                </div>
                            </div>
                            <div class="b-services-item"  data-regions="[all]" data-label="<?= l('Tota Catalunya') ?>">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            4.
                                        </span>
                                        <?= l('Prevenció i sensibilització2') ?>
                                        <div class="visible-xs visible-sm"><span style="font-size: 10px;margin-left: 16px;"><?= l('Tota Catalunya') ?></span></div>
                                    </h6>                                                
                                </div>
                            </div>

                            <div class="b-services-item" data-regions="[all]"   data-label="<?= l('Tota Catalunya') ?>">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            5.
                                        </span>
                                        <?= l('Formació, assessorament i consultoria') ?>
                                        <div class="visible-xs visible-sm"><span style="font-size: 10px;margin-left: 16px;"><?= l('Tota Catalunya') ?></span></div>
                                    </h6>                                                
                                </div>
                            </div>

                            <div class="b-services-item" data-regions="[all]" data-label="<?= l('Tota Catalunya') ?>">
                                <div class="services-icon">
                                    <div class="circle-0">
                                        <div class="circle-1">
                                            <div class="circle-2">
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="services-text">
                                    <h6 class="services-title  customPseudoElBg font-secondary text-uppercase">
                                        <span class="customColor">
                                            6.
                                        </span>
                                        <?= l('Dinamització i participació ciutadana') ?>
                                        <div class="visible-xs visible-sm"><span style="font-size: 10px;margin-left: 16px;"><?= l('Tota Catalunya') ?></span></div>
                                    </h6>                                                
                                </div>
                            </div>



                        </div>
                    </div>
                </div>
             </div>
            





            




        </section>
        
    </div>

<script>
    $(document).on('ready',function(){
        $(document).on('mouseover','.b-services-item',function(){
            var regions = $(this).data('regions');
            regions = regions.replace('[','');
            regions = regions.replace(']','');
            regions = regions.split(',');
            var lab = $(this).data('label');
            if(regions.length==1 && regions[0]=='all'){
                $("#svg29869 > path").addClass('active');
            }else{
                for(var i in regions){
                    $('#svg29869 #'+regions[i]).addClass('active');
                }
            }
            $(".b-f-s-info").addClass('active');

            $("#regionsLabels").html(lab);
        });

        $(document).on('mouseout','.b-services-item',function(){
            $("#regionsLabels").html('');
            $("#svg29869 path").removeClass('active');
            $(".b-f-s-info").removeClass('active');
        });
    })
</script>