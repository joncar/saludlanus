<?php 
    require_once APPPATH.'/controllers/Panel.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();
            $this->load->model('querys');
            $this->load->library('form_validation');
            $this->lang->load('contacto', $this->fullLangs[$_SESSION['lang']]);        
        }        
        
        public function loadView($param = array('view' => 'main')) {
            if($this->router->fetch_method()!='editor'){
                $param['page'] = $this->querys->fillFields($param['page']);
            }
            parent::loadView($param);
        }
        
        function read($url){
            $theme = $this->theme;
            $params = $this->uri->segments;
            $this->load->model('querys');
            
            $title = ucfirst(str_replace('-',' ',$url));
            $title = l('title_tab_'.strtolower($title));
            $this->loadView(
                array(
                    'view'=>'read',
                    'page'=>$this->load->view($theme.$url,array('link'=>$url),TRUE),
                    'link'=>$url,
                    'title'=>$title
                )
            );
        }
        
        function getFormReg($x = '2'){                    
            return $this->querys->getFormReg($x);
        }
        
        function editor($url){            
            $this->load->helper('string');
            if(!empty($_SESSION['user']) && $this->user->admin==1){                
                $page = file_get_contents('application/modules/paginas/views/'.$url.'.php');
                $page = str_replace('<?php','[?php',$page);
                $page = str_replace('<?=','[?=',$page);
                $page = str_replace('&gt;','>',$page);
                $page = str_replace('&lt;','<',$page);
                $this->loadView(array('view'=>'cms/edit','scripts'=>true,'name'=>$url,'edit'=>TRUE,'page'=>$page,'title'=>'Editar '.ucfirst(str_replace('-',' ',$url))));
            }else{
                redirect(base_url());
            }
        }
        
        function contacto(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nom','Nombre','required');
            $this->form_validation->set_rules('message','Comentario','required');
            $this->form_validation->set_rules('politicas','Politícas','required');            
            if($this->form_validation->run()){
                $this->load->library('recaptcha');
                if(!$this->recaptcha->recaptcha_check_answer($_POST['token'])){
                    $_SESSION['msj'] = $this->error($this->lang->line('error_response_captcha_form'));
                }else{ 
                    $datos = $_POST;
                    if(!empty($_POST['extras'])){
                        unset($datos['extras']);
                        $datos['extras'] = '';
                        foreach($_POST['extras'] as $n=>$v){
                            $datos['extras'].= '<p class="MsoNormal"><span style="font-family: arial, helvetica, sans-serif;" data-mce-style="font-family: arial, helvetica, sans-serif;"><strong><span style="color: #808080;" data-mce-style="color: #808080;">'.$n.':</span></strong> '.$v.'</span></p>';                            
                        }
                    }else{
                        $datos['extras'] = '';
                    }

                    if(empty($_POST['asunto'])){
                        $datos['asunto'] = '';
                    }
                    $this->db->insert('contacto',array(
                        'nombre'=>$datos['nom'],
                        'email'=>$datos['email'],
                        'message'=>$datos['message'],
                        'extras'=>$datos['extras'],
                        'fecha'=>date("Y-m-d H:i:s"),
                        'atendido'=>0
                    ));
                    $this->enviarcorreo((object)$datos,1,'donesambempenta@dae.cat');                    
                    $_SESSION['msj'] = $this->success($this->lang->line('success_response_contact_form'));
                }
            }else{                
                $_SESSION['msj'] = $this->error($this->lang->line('error_response_contact_form'));
            }
            echo $_SESSION['msj'];
            unset($_SESSION['msj']);
        }
        
        function subscribir(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            if($this->form_validation->run()){
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()==0?TRUE:FALSE;
                if($success){
                    $this->db->insert('subscritos',array('email'=>$_POST['email']));    
                    echo $this->success('Subscrito satisfactoriamente');
                }else{
                    echo $this->error('Correo ya existente');
                }
            }else{
                echo $this->error($this->form_validation->error_string());
            }
        }
        
        function unsubscribe(){
            if(empty($_POST)){
                $this->loadView('includes/template/unsubscribe');
            }else{
                $emails = $this->db->get_where('subscritos',array('email'=>$_POST['email']));
                $success = $emails->num_rows()>0?TRUE:FALSE;
                if($success){
                    $this->db->delete('subscritos',array('email'=>$_POST['email']));
                    echo $this->success('Correo desafiliado al sistema de noticias');
                }            
                $this->loadView(array('view'=>'includes/template/unsubscribe','success'=>$success));
            }
        }

        function oferta_trabajo(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nom','required');
            $this->form_validation->set_rules('telefono','Telèfon','required');
            $this->form_validation->set_rules('politicas','Politicas','required');
            $this->form_validation->set_rules('ofertas_id','Oferta','required');              
            if($this->form_validation->run()){
                if(!empty($_FILES['adjunto']['tmp_name'])){
                    $file = explode('.',$_FILES['adjunto']['name'],2);
                    if(count($file)==2){
                        get_instance()->load->library('mailer');
                        get_instance()->mailer->mail->AddAttachment($_FILES['adjunto']['tmp_name'],$_FILES['adjunto']['name']);
                        $this->enviarcorreo((object)$_POST,2,'donesambempenta@dae.cat');                        
                        $name = date("dmyis").'.'.$file[1];
                        move_uploaded_file($_FILES['adjunto']['tmp_name'],'curriculums/'.$name);                                                
                        $this->db->insert('ofertas_postulaciones',array(
                            'ofertas_id'=>$_POST['ofertas_id'],
                            'nombre'=>$_POST['nombre'],
                            'email'=>$_POST['email'],
                            'telefono'=>$_POST['telefono'],
                            'adjunto'=>$name,
                            'fecha'=>date("Y-m-d"),
                            'mensaje'=>$_POST['mensaje']
                        ));
                        echo  $this->success(l('gracias-por-contactar'));
                    }else{
                        echo $this->error(l('complete-los-datos'));
                    }
                }
                else{
                    echo $this->error(l('complete-los-datos'));
                }
                
                
            }else{                
                echo $this->error(l('complete-los-datos'));
            }
        }

        function eventos(){
            $this->form_validation->set_rules('email','Email','required|valid_email');
            $this->form_validation->set_rules('nombre','Nom','required');
            $this->form_validation->set_rules('name','Nom','required');
            $this->form_validation->set_rules('telefono','Telèfon','required');
            $this->form_validation->set_rules('politicas','Politicas','required');
            $this->form_validation->set_rules('eventos_id','Evento','required');              
            if($this->form_validation->run()){                
                $this->enviarcorreo((object)$_POST,4,'donesambempenta@dae.cat');
                $this->db->insert('eventos_inscritos',array(
                    'eventos_id'=>$_POST['eventos_id'],
                    'nombre'=>$_POST['name'],
                    'email'=>$_POST['email'],
                    'telefono'=>$_POST['telefono'],                    
                    'fecha_solicitud'=>date("Y-m-d")                    
                ));
                echo  $this->success(l('gracias-por-contactar'));
            }else{                
                echo $this->error(l('complete-los-datos'));
            }
        }

        function buscar(){
            if(!empty($_GET['q'])){
                $this->db->or_like('texto1',$_GET['q']);
                $this->db->or_like('texto2',$_GET['q']);
            }
            $blog = $this->db->get_where('buscador');
            $this->loadView(array('view'=>'buscador','resultados'=>$blog));
        }

        function popup(){
            $popup = $this->db->get('ajustes')->row()->popup;
            if(!empty($popup)){
                $popup = $this->db->get_where('popups',array('id'=>$popup))->row();
                $this->load->view('popups/show',array('popup'=>$popup));
            }
        }






        function sitemap(){
            $pages = array(
                '',
                'filosofia.html',
                'lequip.html',
                'territorideactuacio.html',                
                'noticies',                
                'transparencia.html',
                'lentitatenxifres.html',
                'recursos_economics.html',
                'cura-de-les-persones.html',
                'colaboracions.html',
                'cartells',
                'memories',
                'videos',
                'publicacions',
                'contacte.html',
                'treballa-amb-nosaltres.html',
                'aviso_legal.html',
                'politica_privacitat.html',
                'cookies.html',
                'politica-de-xarxes-socials.html',
                'events.html'
            );

            //Blogs
            foreach($this->db->get_where('blog',array('blog_categorias_id',1))->result() as $b){
                $pages[] = 'noticie/'.toURL($b->id.'-'.$b->titulo);
            }

            //Publicacions
            foreach($this->db->get_where('blog',array('blog_categorias_id',2))->result() as $b){
                $pages[] = 'publicacio/'.toURL($b->id.'-'.$b->titulo);
            }

            //Que fem
            $this->db->order_by('servicios_categorias.orden','ASC'); 
            $serveis = $this->db->get_where('servicios_categorias'); 
            foreach($serveis->result() as $n=>$c){
                $pages[] = 'servei/'.toURL($c->id.'-'.$c->nombre);
                $this->db->order_by('servicios.orden','ASC'); 
                foreach($this->db->get_where('servicios',array('servicios_categorias_id'=>$c->id))->result() as $s){
                    $pages[] = 'serveis/'.toURL($s->id.'-'.$s->titulo);
                }
            }

            //Cura            
            $pdf = $this->db->get_where('transparencia',array('destino'=>2));            
            foreach($pdf->result() as $p){
                $pages[] = 'files/'.$p->fichero;
            } 

            //Transparencia            
            $pdf = $this->db->get_where('transparencia',array('destino'=>1));            
            foreach($pdf->result() as $p){
                $pages[] = 'files/'.$p->fichero;
            } 

            //Recursos economicos            
            $pdf = $this->db->get_where('transparencia',array('destino'=>3));            
            foreach($pdf->result() as $p){
                $pages[] = 'files/'.$p->fichero;
            } 
            
            
            $site = '<?xml version="1.0" encoding="UTF-8"?>
            <urlset xmlns="http://www.sitemaps.org/schemas/sitemap/0.9" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" xsi:schemaLocation="http://www.sitemaps.org/schemas/sitemap/0.9 http://www.sitemaps.org/schemas/sitemap/0.9/sitemap.xsd">';
            foreach($pages as $p){
                $p = base_url().$p;
                $site.= '
                <url>
                      <loc>'.trim($p).'</loc>
                      <lastmod>'.date("Y-m-d").'T11:43:00+00:00</lastmod>
                      <priority>1.00</priority>
                </url>';
            }
            $site.= '</urlset>';
            ob_end_clean();
            ob_end_flush();
            header('Content-Type: application/xml');
            echo $site;
        }

        function search(){
            $domain = base_url();
            $domain = explode('/',$domain);
            $domain = str_replace('www.','',$domain[2]);
            if(!empty($_GET['q'])){
                if(empty($_SESSION[$_GET['q']])){
                    $_SESSION[$_GET['q']] = file_get_contents('http://www.google.es/search?ei=meclXLnwCNma1fAPgbCYCA&q=site%3A'.$domain.'+'.urlencode($_GET['q']).'&oq=site%3A'.$domain.'+'.urlencode($_GET['q']).'&gs_l=psy-ab.3...10613.16478..16743...0.0..0.108.1753.20j3......0....1..gws-wiz.0XRgmCZL0TA');                
                }

                $result = $_SESSION[$_GET['q']];                
                preg_match_all('@<div class=\"ZINbbc xpd O9g5cc uUPGi\">(.*)</div>@si',$result,$result);
                $resultado = $result[0][0];
                $resultado = explode('<footer>',$resultado);
                $resultado = $resultado[0];
                $resultado = str_replace('/url?q=','',$resultado);
                $resultado = explode('<div class="ZINbbc xpd O9g5cc uUPGi">',$resultado);                
                foreach($resultado as $n=>$r){
                    if(strpos($r,'/search?q=site:')){
                        unset($resultado[$n]);
                        continue;
                    }
                    $resultado[$n] = $r;                    
                    $resultado[$n] = substr($r,0,strpos($r,'&'));
                    $pos = strpos($r,'">')+2;
                    $rr = substr($r,$pos);
                    $pos = strpos($rr,'">');
                    $rr = substr($rr,$pos);                    
                    $resultado[$n].= $rr;
                    $resultado[$n] = '<div class="ZINbbc xpd O9g5cc uUPGi">'.$resultado[$n];
                    $resultado[$n] = utf8_encode($resultado[$n]);
                }
                $resultado = implode('',$resultado);
                $this->loadView(array(
                    'view'=>'read',
                    'page'=>$this->load->view($this->theme.'search',array('resultado'=>$resultado),TRUE,'paginas'),
                    'result'=>$resultado,
                    'title'=>'Resultado de busqueda'
                ));

            }
        }


    }
