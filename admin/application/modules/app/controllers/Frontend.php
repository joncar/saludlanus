<?php 
    require_once APPPATH.'controllers/Main.php';    
    class Frontend extends Main{        
        function __construct() {
            parent::__construct();            
        }

        function politicas(){
        	$politicas = $this->ajustes->politicas;
        	$this->loadView([
        		'output'=>$politicas,
        		'title'=>'Políticas de privacidad',
        		'crud'=>'user',
        		'view'=>'panel'
        	]);
        }
    }
?>
