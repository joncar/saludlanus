<?php

require_once APPPATH . '/controllers/Panel.php';

class Api extends Main {

    function __construct() {
        parent::__construct();
        $this->load->library('grocery_crud');
        $this->load->library('ajax_grocery_crud');
        $this->load->model('elements_app');
        if (isset($_SERVER['HTTP_ORIGIN'])) {
            header("Access-Control-Allow-Origin: *");
            header('Access-Control-Allow-Credentials: true');
            header('Access-Control-Max-Age: 86400');    // cache for 1 day
        }

        // Access-Control headers are received during OPTIONS requests
        if ($_SERVER['REQUEST_METHOD'] == 'OPTIONS') {

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_METHOD'])) {
                header("Access-Control-Allow-Methods: GET, POST, OPTIONS");
            }

            if (isset($_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS'])) {
                header("Access-Control-Allow-Headers:{$_SERVER['HTTP_ACCESS_CONTROL_REQUEST_HEADERS']}");
            }

            exit(0);
        }

        $postdata = file_get_contents("php://input");
        if (isset($postdata)) {
            $_POST = (array) json_decode($postdata);
        } else {
            $_POST = array();
        }

        date_default_timezone_set('Europe/Madrid');
        setlocale(LC_ALL, 'Spanish');
    }

    function printjson($object) {
        //header('Content-Type: application/json');
        echo json_encode($object);
    }

    /*     * **** LOGIN **** */

    function registro($act = 0) {
        $response = array('success' => false, 'msj' => 'Complete los datos requeridos');
        $this->form_validation->set_rules('password', 'Password', 'required')
                              /*->set_rules('terminos', 'terminos', 'required')*/;
        if ($this->form_validation->run()) {

            if (!$act) {
                $this->form_validation->set_rules('email', 'Email', 'required|valid_email|is_unique[user.email]');
                if ($this->form_validation->run()) {
                    $_POST['status'] = 1;
                    $_POST['admin'] = 0;
                    $_POST['password'] = md5($_POST['password']);
                    $_POST['fecha_registro'] = date("Y-m-d H:i");
                    $_POST['fecha_actualizacion'] = date("Y-m-d H:i");
                    $this->db->insert('user', $_POST);
                    $response['primary_key'] = $this->db->insert_id();
                    $response['success'] = true;
                    $response['msj'] = 'Usuario añadido con éxito';
                } else {
                    $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
                }
            } else {
                $user = $this->elements_app->user(array('email' => $_POST['email']));
                if ($user->num_rows() > 0) {
                    $user = $user->row();
                    if ($user->password != $_POST['password']) {
                        $_POST['password'] = md5($_POST['password']);
                    }
                    $_POST['fecha_actualizacion'] = date("Y-m-d H:i");
                    $this->db->update('user', $_POST, array('id' => $_POST['id']));
                    $response['success'] = true;
                    $response['msj'] = 'Usuario actualizado con éxito';
                } else {
                    $response['msj'] = 'Ocurrió un error al actualizar';
                }
            }
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }

        $this->printjson($response);
    }

    function login() {
        $response = array('success' => false, 'msj' => 'Complete los datos requeridos');
        $this->form_validation->set_rules('email', 'Email', 'required|valid_email')
                ->set_rules('password', 'Password', 'required');
        if ($this->form_validation->run()) {
            $user = $this->elements_app->user(array('email' => $_POST['email'], 'password' => md5($_POST['password'])));
            if ($user->num_rows() == 0) {
                $response['msj'] = 'Usuario o contraseña incorrecta';
            } elseif (!$user->row()->status) {
                $response['msj'] = 'Su usuario se encuentra bloqueado, por favor comunícate con el administrador del sistema para más información';
            } else {
                $response = array(
                    'success' => true,
                    'msj' => $user->row()
                );
            }
        } else {
            $response['msj'] = !empty($_POST) ? $this->form_validation->error_string() : $response['msj'];
        }

        $this->printjson($response);
    }

    function recover() {
        $response = array('success' => false, 'msj' => 'Completa los datos para recuperar');
        if (!empty($_POST)) {
            if (!empty($_POST['email'])) {
                $usuario = $this->db->get_where('user', array('email' => $_POST['email']));
                if ($usuario->num_rows() == 0) {
                    $response['msj'] = 'El email ingresado no existe';
                } else {
                    $response['success'] = true;
                    $response['msj'] = 'Los pasos para restauración han sido enviados a su Email';
                    $post = $usuario->row();
                    $post->link = base_url('registro/recuperar_app/' . base64_encode($post->email));
                    //echo $post->link;
                    //$this->enviarcorreo($post,15);
                }
            } else {
                $response['msj'] = 'Debe indicar los datos para realizar su conexión';
            }
        } else {
            $response['msj'] = 'Debe indicar los datos para realizar su conexión';
        }

        echo json_encode($response);
    }
    /*     * ********* END LOGIN *********** */
    

    function posts($type = 'noticias',$limit = 10,$order = 'post_date-desc') {        
        $response = array('success' => false, 'msj' => 'Completa los datos para recuperar');
        if(!empty($limit)){
            $this->db->limit(10);
        }
        if(!empty($order)){
            list($field,$order) = explode('-',$order);
            $this->db->order_by($field,$order);
        }
        $this->db->select("posts.*, DATE_FORMAT(post_date,'%d/%m/%Y %H:%i') as post_date_format");
        $usuario = $this->db->get_where('posts',['post_type'=>$type]);                
        foreach($usuario->result() as $n=>$u){
            $meta = get_postmeta($u->id);
            foreach($meta as $k=>$m){
                $meta[$k] = is_json($m)?json_decode($m):$m;
            }
            $usuario->row($n)->meta = $meta;
        }

        if($type=='consejos'){
            foreach($usuario->result() as $n=>$u){
                $usuario->row($n)->post_content = base_url('img/app/'.$u->post_content);
            }
        }

        if($type=='noticias'){
            foreach($usuario->result() as $n=>$u){
                if(is_string($u->meta['imagen']) || (!is_object($u->meta['imagen']) && !is_json($u->meta['imagen']))){
                    $u->meta['imagen'] = ['file'=>$u->meta['imagen']];
                }
                if(!empty($u->meta['youtube'])){
                    $youtube = $u->meta['youtube'];
                    $youtube = str_replace('watch?v=','embed/',$youtube).'?rel=0';
                    $u->meta['youtube'] = $youtube;                    
                }
            }
        }
        echo json_encode(sqlToArray($usuario));
    }

    function usermeta($user_id,$key = '') {        
        $response = array('success' => false, 'msj' => 'Completa los datos para recuperar');
        $response = get_usermeta($user_id,$key);    
        echo json_encode($response);
    }

    function diagnosticar() {
        $response = array('success' => false, 'msj' => 'Completa los datos requeridos');        
        $this->form_validation->set_rules('edad','Edad','required|numeric')
                              ->set_rules('localidad','Localidad','required')
                              ->set_rules('direccion','Dirección','required')
                              ->set_rules('telefono','Teléfono','required')
                              ->set_rules('localidad','Localidad','required')
                              ->set_rules('temperatura','Temperatura','required|numeric')
                              ->set_rules('user_id','Usuario','required');
        if($this->form_validation->run()){
            foreach($_POST as $n=>$v){
                if($n!='user_id'){
                    $existe = count(get_usermeta($_POST['user_id'],$n))>0;
                    if(!$existe){
                        add_usermeta($_POST['user_id'],$n,$v);
                    }else{
                        update_usermeta($_POST['user_id'],$n,$v);
                    }
                }
            }
            $this->db->update('user',['fecha_actualizacion'=>date("Y-m-d H:i")],['id'=>$_POST['user_id']]);
            $response['success'] = true;
            $response['msj'] = 'Sus datos han sido almacenados con éxito';
        }else{
            $response['msj'] = $this->form_validation->error_string();
        }
        echo json_encode($response);
    }

    function addusermeta() {
        $response = array('success' => false, 'msj' => 'Completa los datos requeridos');        
        $this->form_validation->set_rules('user_id','Usuario','required');
        if($this->form_validation->run()){
            foreach($_POST as $n=>$v){
                if($n!='user_id'){
                    $existe = count(get_usermeta($_POST['user_id'],$n))>0;
                    if(!$existe){
                        add_usermeta($_POST['user_id'],$n,$v);
                    }else{
                        update_usermeta($_POST['user_id'],$n,$v);
                    }
                }
            }
            $response['success'] = true;
            $response['msj'] = 'Sus datos han sido almacenados con éxito';
        }else{
            $response['msj'] = $this->form_validation->string_error();
        }
        echo json_encode($response);
    }

    function ajustes() {
        $response = array('success' => false, 'msj' => 'Completa los datos requeridos');        
        $response = $this->db->get('ajustes')->row();
        echo json_encode($response);
    }

    function estadisticas() {
        $response = array('success' => false, 'msj' => 'Completa los datos requeridos');        
        $casos = $this->db->get_where('reporte_casos',['tipo'=>'Lanús']);
        if($this->ajustes->habilitar_estadisticas_en_app==1){
            $response = sqlToArray($casos);
        }else{
            $response = [];
        }
        echo json_encode($response);
    }
}
