<div class="kt-portlet">
  <div class="kt-portlet__head">
    <div class="kt-portlet__head-label">
      <h1 class="kt-portlet__head-title">
        <b>Datos generales</b>
      </h1>
    </div>
  </div>
  <div class="kt-portlet__body">
    <div class="kt-section">

      <div class="row">
        <div class="col-3">
          <div class="nav flex-column nav-pills" id="v-pills-tab" role="tablist" aria-orientation="vertical">
            <a class="nav-link active" id="v-pills-home-tab" data-toggle="pill" href="#v-pills-home" role="tab" aria-controls="v-pills-home" aria-selected="true">País</a>
            <a class="nav-link" id="v-pills-profile-tab" data-toggle="pill" href="#v-pills-profile" role="tab" aria-controls="v-pills-profile" aria-selected="false">PBA</a>
            <a class="nav-link" id="v-pills-messages-tab" data-toggle="pill" href="#v-pills-messages" role="tab" aria-controls="v-pills-messages" aria-selected="false">Lanús</a>            
            <a class="nav-link" id="v-pills-sier-tab" data-toggle="pill" href="#v-pills-sier" role="tab" aria-controls="v-pills-sier" aria-selected="false">Modelo Sier</a>            
            <a class="nav-link" id="v-pills-r0-tab" data-toggle="pill" href="#v-pills-r0" role="tab" aria-controls="v-pills-r0" aria-selected="false">Posibles comportamientos</a>            
            <a class="nav-link" id="v-pills-proyeccion_automatica-tab" data-toggle="pill" href="#v-pills-proyeccion_automatica" role="tab" aria-controls="v-pills-proyeccion_automatica" aria-selected="false">Proyección automática</a>            
          </div>
        </div>
        <div class="col-9">
          <div class="tab-content" id="v-pills-tabContent">
            <div class="tab-pane fade show active" id="v-pills-home" role="tabpanel" aria-labelledby="v-pills-home-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('datos_epidemiologicos')
                     ->set_subject('Datos epidemiologicos')
                     ->set_theme('bootstrap')
                     ->where('tipo',1)
                     ->set_url('estadisticas/datos_epidemiologicos/1/');
                $crud->unset_columns('tipo');
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div>
            <div class="tab-pane fade" id="v-pills-profile" role="tabpanel" aria-labelledby="v-pills-profile-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('datos_epidemiologicos')
                     ->set_subject('Datos epidemiologicos')
                     ->set_theme('bootstrap')
                     ->where('tipo',2)
                     ->set_url('estadisticas/datos_epidemiologicos/2/');
                $crud->unset_columns('tipo');
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div>
            <div class="tab-pane fade" id="v-pills-messages" role="tabpanel" aria-labelledby="v-pills-messages-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('datos_epidemiologicos')
                     ->set_subject('Datos epidemiologicos')
                     ->set_theme('bootstrap')
                     ->where('tipo',3)
                     ->set_url('estadisticas/datos_epidemiologicos/3/');
                $crud->unset_columns('tipo');
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div>  
            <div class="tab-pane fade" id="v-pills-sier" role="tabpanel" aria-labelledby="v-pills-sier-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('poblacion')
                     ->set_subject('Población')
                     ->set_theme('bootstrap')                     
                     ->set_url('estadisticas/poblacion/');
                $crud->unset_columns('tipo');
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div> 
            <div class="tab-pane fade" id="v-pills-r0" role="tabpanel" aria-labelledby="v-pills-r0-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('R0')
                     ->set_subject('R0')
                     ->set_theme('bootstrap')                     
                     ->set_url('estadisticas/R0/');

                $crud->unset_add()->unset_delete()->unset_print()->unset_export()->unset_read();
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div>      
            <div class="tab-pane fade" id="v-pills-proyeccion_automatica" role="tabpanel" aria-labelledby="v-pills-proyeccion_automatica-tab">
              <?php 
                $crud = new ajax_grocery_crud();
                $crud->set_table('proyeccion_automatica')
                     ->set_subject('Proyección Automática')
                     ->set_theme('bootstrap')                     
                     ->set_url('estadisticas/proyeccion_automatica/');

                $crud->unset_print()->unset_export()->unset_read();
                $crud = $crud->render();
                echo $crud->output;
              ?>
            </div>          
          </div>
        </div>
      </div>
    </div>
  </div>
</div>