<!--Begin::Row-->
<div class="row">

	<?php foreach($this->db->get('reporte_casos')->result() as $d): ?>
		<div class="col-xl-4 col-lg-6 order-lg-3 order-xl-1">
			<!--begin:: Widgets/New Users-->
			<div class="kt-portlet kt-portlet--tabs kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							<?= $d->tipo ?>
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						<div class="tab-pane active" id="kt_widget4_tab1_content">
							<div class="kt-widget4">
								

								<div class="kt-widget4__item">
									<div class="kt-widget4__info">
										<span class="kt-widget4__username">
											Primer caso positivo
										</span>
									</div>
									<div class="btn btn-sm btn-label-brand btn-bold">
										<?= $d->primer_caso_confirmado ?>
									</div>
								</div>

								<div class="kt-widget4__item">
									<div class="kt-widget4__info">
										<span class="kt-widget4__username">
											Casos serios
										</span>
									</div>
									<div class="btn btn-sm btn-label-warning btn-bold">
										<?= $d->casos_serios ?>
									</div>
								</div>

								<div class="kt-widget4__item">
									<div class="kt-widget4__info">
										<span class="kt-widget4__username">
											Tasa de Letalidad
										</span>
									</div>
									<div class="btn btn-sm btn-label-danger btn-bold">
										<?= $d->tasa_letalidad ?>
									</div>
								</div>

								<div class="kt-widget4__item">
									<div class="kt-widget4__info">
										<span class="kt-widget4__username">
											Numero actual de infectados
										</span>
									</div>
									<div class="btn btn-sm btn-label-info btn-bold">
										<?= $d->infectados ?>
									</div>
								</div>

								<div class="kt-widget4__item">
									<div class="kt-widget4__info">
										<span class="kt-widget4__username">
											Numero actual de curados
										</span>
									</div>
									<div class="btn btn-sm btn-label-success btn-bold">
										<?= $d->curados ?>
									</div>
								</div>


							</div>
						</div>
					</div>
				</div>
			</div>

			<!--end:: Widgets/New Users-->
		</div>
	<?php endforeach ?>











	<?php 
		$graficosLabels = [
			'confirmados'=>'Casos confirmados en total',
			'curados'=>'Casos curados en total',
			'serios'=>'Total de casos serios',
			'fallecidos'=>'Total de fallecidos'
		];
		$graficosColores = [
			'confirmados'=>'success',
			'curados'=>'info',
			'serios'=>'warning',
			'fallecidos'=>'danger'
		];
		$graficos = [];
		$this->db->order_by('fecha','ASC');
		$this->db->limit(30);
		$this->db->where('MONTH(fecha)',date("m"));
		$this->db->where('YEAR(fecha)',date("Y"));
		$graficos[] = (object)['tipo'=>'País','data'=>$this->db->get_where('datos_epidemiologicos',['tipo'=>1])];
		$this->db->order_by('fecha','ASC');
		$this->db->limit(30);
		$this->db->where('MONTH(fecha)',date("m"));
		$this->db->where('YEAR(fecha)',date("Y"));
		$graficos[] = (object)['tipo'=>'PBS','data'=>$this->db->get_where('datos_epidemiologicos',['tipo'=>2])];
		$this->db->order_by('fecha','ASC');
		$this->db->limit(30);
		$this->db->where('MONTH(fecha)',date("m"));
		$this->db->where('YEAR(fecha)',date("Y"));
		$graficos[] = (object)['tipo'=>'Lanús','data'=>$this->db->get_where('datos_epidemiologicos',['tipo'=>3])];
	?>
		<div class="col-xl-12 col-lg-12 order-lg-12 order-xl-1">

			<!--begin:: Widgets/Best Sellers-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Gráfico Estadístico
						</h3>
					</div>
					<div class="kt-portlet__head-toolbar">
						<ul class="nav nav-pills nav-pills-sm nav-pills-label nav-pills-bold" role="tablist">
							<?php foreach($graficos as $n=>$g): ?>
								<li class="nav-item">
									<a class="nav-link <?= $n==0?'active':'' ?>" data-toggle="tab" href="#tipo_<?= $n ?>" role="tab">
										<?= $g->tipo ?>
									</a>
								</li>
							<?php endforeach ?>
						</ul>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">
						
						<?php foreach($graficos as $n=>$g): ?>
							<div class="tab-pane <?= $n==0?'active':'' ?>" id="tipo_<?= $n ?>" aria-expanded="true">
								


								<div class="row">
									<?php if($g->data->num_rows()>0): ?>
									<?php foreach($g->data->row() as $nn=>$gg): if($nn!='id' && $nn!='fecha' && $nn!='tipo'): ?>
										<div class="col-xl-3 col-lg-3 order-lg-2 order-xl-1">

											<!--begin:: Widgets/Daily Sales-->
											<div class="kt-portlet kt-portlet--height-fluid">
												<div class="kt-widget14">
													<div class="kt-widget14__header kt-margin-b-30">
														<h3 class="kt-widget14__title">
															<?= $graficosLabels[$nn] ?>
														</h3>
														<span class="kt-widget14__desc">
															Incremento diario
														</span>
													</div>
													<div class="kt-widget14__chart" style="height:120px;">
														<canvas id="grafico-<?= $nn ?>-<?= $g->data->row()->tipo ?>"></canvas>
													</div>
												</div>
											</div>

											<!--end:: Widgets/Daily Sales-->
										</div>


										<script>
											<?php 
												$labels = [];
												$data = [];
												foreach($g->data->result() as $datos){
													$labels[] = date("d",strtotime($datos->fecha)).'.'.meses(date("m",strtotime($datos->fecha)));
													$data[] = $datos->{$nn};
												}
											?>
											window.afterLoad.push(function(){												
												
										        var chartContainer = KTUtil.getByID('grafico-<?= $nn ?>-<?= $g->data->row()->tipo ?>');

										        if (!chartContainer) {
										            return;
										        }

										        var chartData = {
										            labels: <?= json_encode($labels) ?>,
										            datasets: [{
										                //label: 'Dataset 1',
										                backgroundColor: KTApp.getStateColor('<?= $graficosColores[$nn] ?>'),
										                data:<?= json_encode($data); ?>
										            }]
										        };

										        var chart = new Chart(chartContainer, {
										            type: 'bar',
										            data: chartData,
										            options: {
										                title: {
										                    display: false,
										                },
										                tooltips: {
										                    intersect: false,
										                    mode: 'nearest',
										                    xPadding: 10,
										                    yPadding: 10,
										                    caretPadding: 10
										                },
										                legend: {
										                    display: false
										                },
										                responsive: true,
										                maintainAspectRatio: false,
										                barRadius: 4,
										                scales: {
										                    xAxes: [{
										                        display: false,
										                        gridLines: false,
										                        stacked: true
										                    }],
										                    yAxes: [{
										                        display: false,
										                        stacked: true,
										                        gridLines: false
										                    }]
										                },
										                layout: {
										                    padding: {
										                        left: 0,
										                        right: 0,
										                        top: 0,
										                        bottom: 0
										                    }
										                }
										            }
										        });
											    
											});
										</script>





									<?php endif; endforeach ?>
									<?php else: ?>
										Aún no se han registrado valores
									<?php endif ?>
			

								</div>
							</div>
						<?php endforeach ?>


					</div>
				</div>
			</div>

			<!--end:: Widgets/Best Sellers-->
		</div>











		<div class="col-xl-12 col-lg-12 order-lg-12 order-xl-1">

			<!--begin:: Widgets/Best Sellers-->
			<div class="kt-portlet kt-portlet--height-fluid">
				<div class="kt-portlet__head">
					<div class="kt-portlet__head-label">
						<h3 class="kt-portlet__head-title">
							Proyección Automática
						</h3>
					</div>
				</div>
				<div class="kt-portlet__body">
					<div class="tab-content">

						<div class="row">
							<div class="col-12 col-md-4">
								<div class="kt-portlet kt-portlet--height-fluid">
									<div class="kt-widget14">
										<div class="kt-widget14__header kt-margin-b-30">
											<h3 class="kt-widget14__title">
												Proyección Estadística Lanús
											</h3>
											<span class="kt-widget14__desc">
												Modelo SIER - Posibles comportamientos
											</span>
										</div>
										<div class="kt-widget14__chart">
											<?php 
												$datos = $this->db->get('R0');
												echo sqlToHtml($datos);
											?>
										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-4">
								<div class="kt-portlet kt-portlet--height-fluid">
									<div class="kt-widget14">
										<div class="kt-widget14__header kt-margin-b-30">
											<h3 class="kt-widget14__title">
												Sospechoso VS Infectados
											</h3>
											<span class="kt-widget14__desc">
												Incremento diario
											</span>
										</div>
										<div class="kt-widget14__chart" style="height:120px;">
											<canvas id="grafico-sospechososvsinfectados"></canvas>
											<script>
												<?php 
													$labels = [];
													$data = [];
													$data2 = [];
													$data3 = [];
													$this->db->limit(30);
													$this->db->where('MONTH(fecha)',date("m"));
													$this->db->where('YEAR(fecha)',date("Y"));
													$proyectados = $this->db->get('proyeccion_automatica');
													foreach($proyectados->result() as $datos){
														$labels[] = date("d",strtotime($datos->fecha)).'.'.meses(date("m",strtotime($datos->fecha)));
														$data[] = $datos->sospechosos;
														$data2[] = $datos->infectados_reales;

														$data3[] = $datos->infectados_proyectado;
													}
												?>
												window.afterLoad.push(function(){												
													
											        var chartContainer = KTUtil.getByID('grafico-sospechososvsinfectados');

											        if (!chartContainer) {
											            return;
											        }

											        var chartData = {
											            labels: <?= json_encode($labels) ?>,
											            datasets: [{
											                label: 'Sospechosos',
											                backgroundColor: KTApp.getStateColor('info'),
											                data:<?= json_encode($data); ?>
											            },{
											                label: 'Infectados',
											                backgroundColor: KTApp.getStateColor('success'),
											                data:<?= json_encode($data2); ?>
											            }]
											        };

											        var chart = new Chart(chartContainer, {
											            type: 'bar',
											            data: chartData,
											            options: {
											                title: {
											                    display: false,
											                },
											                tooltips: {
											                    intersect: false,
											                    mode: 'nearest',
											                    xPadding: 10,
											                    yPadding: 10,
											                    caretPadding: 10
											                },
											                legend: {
											                    display: false
											                },
											                responsive: true,
											                maintainAspectRatio: false,
											                barRadius: 4,
											                scales: {
											                    xAxes: [{
											                        display: false,
											                        gridLines: false,
											                        stacked: true
											                    }],
											                    yAxes: [{
											                        display: false,
											                        stacked: true,
											                        gridLines: false
											                    }]
											                },
											                layout: {
											                    padding: {
											                        left: 0,
											                        right: 0,
											                        top: 0,
											                        bottom: 0
											                    }
											                }
											            }
											        });
												    
												});
											</script>

										</div>
									</div>
								</div>
							</div>
							<div class="col-12 col-md-4">
								<div class="kt-portlet kt-portlet--height-fluid">
									<div class="kt-widget14">
										<div class="kt-widget14__header kt-margin-b-30">
											<h3 class="kt-widget14__title">
												Infectados proyectados VS Infectados reales
											</h3>
											<span class="kt-widget14__desc">
												Incremento diario
											</span>
										</div>
										<div class="kt-widget14__chart" style="height:120px;">
											<canvas id="grafico-infectadosproyectadosreales"></canvas>
											<script>												
												window.afterLoad.push(function(){												
													
											        var chartContainer = KTUtil.getByID('grafico-infectadosproyectadosreales');

											        if (!chartContainer) {
											            return;
											        }

											        var chartData = {
											            labels: <?= json_encode($labels) ?>,
											            datasets: [{
											                label: 'Infectados Proyectados',
											                backgroundColor: KTApp.getStateColor('info'),
											                data:<?= json_encode($data3); ?>
											            },{
											                label: 'Infectados Reales',
											                backgroundColor: KTApp.getStateColor('success'),
											                data:<?= json_encode($data2); ?>
											            }]
											        };

											        var chart = new Chart(chartContainer, {
											            type: 'bar',
											            data: chartData,
											            options: {
											                title: {
											                    display: false,
											                },
											                tooltips: {
											                    intersect: false,
											                    mode: 'nearest',
											                    xPadding: 10,
											                    yPadding: 10,
											                    caretPadding: 10
											                },
											                legend: {
											                    display: false
											                },
											                responsive: true,
											                maintainAspectRatio: false,
											                barRadius: 4,
											                scales: {
											                    xAxes: [{
											                        display: false,
											                        gridLines: false,
											                        stacked: true
											                    }],
											                    yAxes: [{
											                        display: false,
											                        stacked: true,
											                        gridLines: false
											                    }]
											                },
											                layout: {
											                    padding: {
											                        left: 0,
											                        right: 0,
											                        top: 0,
											                        bottom: 0
											                    }
											                }
											            }
											        });
												    
												});
											</script>
										</div>
									</div>
								</div>
							</div>
						</div>

					</div>
				</div>
			</div>

		<!--end:: Widgets/Best Sellers-->
		</div>


</div>

<!--End::Row-->

<?php 
	get_instance()->js[] = '<script src="'.base_url().'js/pages/dashboard.js" type="text/javascript"></script>';
?>