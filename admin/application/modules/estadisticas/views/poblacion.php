<?php echo $output ?>
<script>
	window.afterLoad.push(function(){
		$("#field-r0,#field-gamma").change(function(){
			let r0,gamma,beta;
			r0 = parseFloat($("#field-r0").val());
			gamma = parseFloat($("#field-gamma").val());
			if(!isNaN(r0) && !isNaN(gamma)){
				beta = r0*gamma*100;
				$("#field-beta").val(beta);
			}
		});	
	});
</script>