<?php 
require_once APPPATH.'controllers/Panel.php'; 
class Estadisticas extends Panel{
	function __construct(){
		parent::__construct();
	}

	function datos_epidemiologicos($x = ''){
		$crud = $this->crud_function('','');		
		if(is_numeric($x)){
			$crud->where('tipo',$x)
				 ->field_type('tipo','hidden',$x)
				 ->unset_columns('tipo');
		}
		$out = $crud->render();	
		if($crud->getParameters()=='list'){
			$out->output = $this->load->view('datos_epidemiologicos',['output'=>$out->output],TRUE);
		}
		$this->loadView($out);
	}

	function poblacion($x = ''){
		$crud = $this->crud_function('','');
		$crud->callback_column('fecha',function($val,$row){
			return meses(date('m',strtotime($val)));
		});
		$out = $crud->render();	
		if($crud->getParameters()!='list'){
			$out->output = $this->load->view('poblacion',['output'=>$out->output],TRUE);
		}		
		$this->loadView($out);
	}

	function R0($x = ''){
		$crud = $this->crud_function('','');		
		$out = $crud->render();
		$this->loadView($out);
	}

	function proyeccion_automatica($x = ''){
		$crud = $this->crud_function('','');		
		$out = $crud->render();
		$this->loadView($out);
	}

	function datos_estadisticos($x = ''){		
		$this->loadView([
			'view'=>'panel',
			'crud'=>'user',
			'output'=>$this->load->view('estadisticas',[],TRUE),
			'title'=>'Datos estadísticos epidemiologicos'
		]);
	}

}
?>