<?php
class Carrito extends CI_Model{
    
    function __construct()
    {
        parent::__construct();
    }  
    
    function getProductos(){
        /*if(!empty($_GET['categoria']) && is_numeric($_GET['categoria']) && $_GET['categoria']>0){
            $this->db->where('categorias_id',$_GET['categoria']);
        }
        if(!empty($_GET['producto'])){
            $this->db->like('producto_nombre',$_GET['producto']);
        }*/
        //$this->db->order_by('priority','ASC');
        //$this->db->where('disponible',1);
        //return $this->db->get('productos');
    }
    
    function getCarrito(){
        if(empty($_SESSION['carrito'])){
            return array();
        }
        else{
            return $_SESSION['carrito'];
        }
    }
    
    function setCarrito($item,$sumarcantidad = TRUE){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }
        $existente = false;
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$item->id){
                if($sumarcantidad){
                    $_SESSION['carrito'][$n]->cantidad+=$item->cantidad;
                }else{
                    $_SESSION['carrito'][$n]->cantidad=$item->cantidad;
                }
                $existente = true;
            }
        }
        if(!$existente){
            array_push($_SESSION['carrito'],$item);
        }        
    }
    
    function delCarrito($producto_id){
        if(empty($_SESSION['carrito'])){
            $_SESSION['carrito'] = array();
        }        
        foreach($_SESSION['carrito'] as $n=>$c){
            if($c->id==$producto_id){
                unset($_SESSION['carrito'][$n]);
            }
        }                
    }
}
