<?php

class Elements_app extends CI_Model{
	function __construct(){
		parent::__construct();
	}

	function user($where = array()){
		$data = $this->db->get_where('user',$where);
		foreach($data->result() as $n=>$v){
			//$data->row($n)->foto = base_url();
		}
		return $data;
	}

	protected $apiKeyOneSignal = 'MDM0ZmVjZGUtNTNlOC00OTNkLWEwNzktY2U3YjM4YjQyM2Y3';
    protected $appIdOneSignal = '6ab3d0c3-ab46-4db4-b762-a5c9f8b8ae99';

    function sendPush($registrationIdsArray,$messageData,$title = 'Mensaje de recordatorio'){
        $headers = array("Content-Type:" . "application/json", "Authorization:" . "Basic " . $this->apiKeyOneSignal);
        $data = array(
        'app_id' => $this->appIdOneSignal,
        'headings'=>array('en'=>$title,'es'=>$title),
        'data'=>array('param1'=>'parametro'),        
        'contents' => array('en'=>$messageData['message'],'es'=>$messageData['message']),
        'android_background_layout'=>array("image"=>"https://domain.com/background_image.jpg", "headings_color"=>"FFFF0000", "contents_color"=>"FF00FF00"),
        'include_player_ids'=>$registrationIdsArray,
        'ios_badgeType'=>'Increase',
        'ios_badgeCount'=>'1',
        'large_icon'=>base_url('img/notifIcon.png')
        );

        $ch = curl_init();
        curl_setopt( $ch, CURLOPT_HTTPHEADER, $headers );
        curl_setopt( $ch, CURLOPT_URL, "https://onesignal.com/api/v1/notifications" );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYHOST, 0 );
        curl_setopt( $ch, CURLOPT_SSL_VERIFYPEER, 0 );
        curl_setopt( $ch, CURLOPT_RETURNTRANSFER, true );
        curl_setopt( $ch, CURLOPT_POSTFIELDS, json_encode($data) );
        $response = curl_exec($ch);
        curl_close($ch);
        print_r($response);
        return $response;
    }
	
}