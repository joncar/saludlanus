
    <!-- Header -->
    <header id="header" class="light">
        <div class="container">
            <div class="row">
                <div class="col-md-3">
                    <!-- Logo -->
                    <div class="module module-logo light">
                        <a href="[base_url]">
                            <img src="[base_url]theme/theme/assets/img/logo-dark.svg" alt="" width="88">
                        </a>
                    </div>
                </div>
                <div class="col-md-8">
                    <!-- Navigation -->
                    <nav class="module module-navigation left mr-4">
                        <ul id="nav-main" class="nav nav-main">
                            <li><a href="[base_url]">Inicio</a></li>
                            <li class="has-dropdown">
                                <a href="#">Nosaltres</a>
                                <div class="dropdown-container">
                                    <ul>
                                        <li><a href="[base_url]p/nosaltres">Sobre Nosaltres</a></li>
                                        <li><a href="[base_url]p/testimonis">Testimonis</a></li>
                                        <li><a href="[base_url]p/faq">Faq</a></li>
                                        <li><a href="[base_url]p/tienda">Tienda</a></li>
                                    </ul>
                                </div>
                            </li>
                            <li><a href="[base_url]p/serveis">Serveis</a></li>
                            <li><a href="[base_url]p/talleres">Talleres</a></li>
                            <li><a href="[base_url]galeria">Galeria</a></li>
                            <li><a href="[base_url]blog">Blog</a></li>
                            <li><a href="[base_url]p/contacto">Contacto</a></li>
                        </ul>
                    </nav>
                    <div class="module left">
                        <a href="menu-list-navigation.html" class="btn btn-outline-secondary"><span>Order</span></a>
                    </div>
                </div>
                <div class="col-md-1">
                    <a href="#" class="module module-cart right" data-toggle="panel-cart">
                        <span class="cart-icon">
                            <i class="ti ti-shopping-cart"></i>
                            <span class="notification">2</span>
                        </span>
                        <span class="cart-value">$32.98</span>
                    </a>
                </div>
            </div>
        </div>
    </header>
    <!-- Header / End -->