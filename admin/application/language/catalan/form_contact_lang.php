<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
$lang['success_response_contact_form'] = 'Gràcies per contactar-nos, en breu ens comunicarem amb vostè.';
$lang['error_response_contact_form'] = 'Si us plau completi les dades sol·licitades  <script>$("#guardar").attr("disabled",false); </script>';
$lang['error_response_captcha_form'] = 'Captcha introduït incorrectament';

$lang['asunto2'] = 'ha sol·licitat contacte';
$lang['titulo'] = 'FORMULARI CONTACTE WEB';
$lang['Nom'] = 'Nom';
$lang['asunto'] = 'Assumpte';
$lang['telefono'] = 'Telèfon';
$lang['message'] = 'Missatge';

$lang['subscribe'] = 'Gràcies per subscriure\'t';
$lang['subscribeText'] = 'Hola {email}, gràcies per subscriure\'t a la nostra newsletter';
$lang['subscribeBaja'] = 'Si desitges donar-te de baixa clicka en aquest <a href="'.base_url().'paginas/frontend/unsubscribe/{email}">enllaç</a>';
$lang['subscribeBajaTitulo'] = 'Usuari vol donar-se de baixa dels butlletins';
$lang['subscribeBajaText'] = 'Hola, et contactem desde la web per informar-te que l’usuari {email} no desitja rebre més butlletins desde la web.';
$lang['subscribeBajaSuccess'] = 'Donat de baixa correctament';

$lang['contacto_email_subs_error'] = 'Aquest correu ja està registrat';
$lang['contactO_email_subs_success'] = 'La subscripció s\'ha realitzat correctament';

$lang['medicacio_success'] = 'Sol·licitud enviada. En breu ens posarem en contacte.';

$lang['solicitud_medicacio'] = 'Sol·licitud de fórmula';
$lang['solicitud_asunto2'] = 'Sol·licita una fórmula';

$lang['solicitud_formula_nombre'] =  'Nom';
$lang['solicitud_formula_telefono'] =  'Telèfon';
$lang['solicitud_formula_email'] =  'Email';
$lang['solicitud_formula_cp'] =  'C.P.';
$lang['solicitud_formula_direccion'] =  'Adreça';
$lang['solicitud_formula_ciudad'] =  'Ciutat';
$lang['solicitud_formula_provincia'] =  'Província';
$lang['solicitud_formula_receta1'] = 'Recepta1';
$lang['solicitud_formula_receta2'] = 'Recepta2';
$lang['solicitud_formula_receta3'] = 'Recepta3';
$lang['solicitud_formula_comentarios'] = 'Comentaris';

$lang['solicitud_gracias'] = ' 
<p>gràcies per la teva sol·licitud de fórmula. En breu ens posarem en contacte per concretar la recollida. </p>
<p>A continuació el detall de la teva sol·licitud:</p>';