<?php    
class Traduccion{    
    function __construct(){
        $this->db = get_instance()->db;
    }
    function traducirObj($obj){
        $idioma = $_SESSION['lang'];
        $idiomas = $this->db->get_where('ajustes')->row()->idiomas;
        $idiomas = explode(', ',$idiomas);
        if($idioma==$idiomas[0]){
            return $obj;
        }
        if(empty($obj->idiomas)){
            return $obj;
        }
        if(!is_json($obj->idiomas)){
            return $obj;
        }
        $obj->idiomas = json_decode($obj->idiomas);
        if(empty($obj->idiomas->{$idioma})){
            return $obj;
        }
        $traducciones = $obj->idiomas->{$idioma};
        foreach($obj as $n=>$v){
            if(!empty($traducciones->{$n})){
                $obj->{$n} = $traducciones->{$n};
            }
        }
        $obj->idioma = $idioma;
        
        return $obj;
    }

    public function transform($obj){
        if(isset($obj->result)){
            foreach($obj->result() as $n=>$o){
                $obj->result_object[$n] = $this->traducirObj($o);
            }
        }elseif(isset($obj->row)){
            $obj->result_object[0] = $this->traducirObj($obj->row());
        }

        foreach($obj->result_object as $n=>$v){
            if(is_object($v)){
                $obj->result_object[$n] = $this->traducirObj($v);
            }
        }
        return $obj;
    }


    public function traducir($view,$idioma = ''){   
        /*$this->db = get_instance()->db;     
        $traducciones = $this->db->get_where('traducciones',array('idioma'=>$idioma));
        $lang = array();
        if($traducciones->num_rows()>0){
            foreach($traducciones->result() as $v){
                $lang[$v->original] = $v->traduccion;
            }
        }


        $view = str_replace('data-','s1s1w3',$view);
        $view = str_replace('data=','s1s1w4',$view);
        $view = str_replace('(data)','s1s1w5',$view);
        if(!empty($lang)){
            foreach($lang as $n=>$v){                
                $view = str_replace($n,$v,$view);
            }
        }
        $view = str_replace('s1s1w3','data-',$view);
        $view = str_replace('s1s1w4','data=',$view);
        $view = str_replace('s1s1w5','(data)',$view);
        $view = $this->traducirbd($view);*/
        return $view;
    }

    function traducirbd($view){
        //Reemplazar inputs
        //$_SESSION['lang'] = 'ca';
        /*$this->db = get_instance()->db;
        $dbname = $this->db->database;
        $tables = $this->db->query("SELECT t.TABLE_NAME AS name FROM INFORMATION_SCHEMA.TABLES as t WHERE t.TABLE_SCHEMA = '$dbname'");
        $lang = $_SESSION['lang'];
        if($lang!='ca'){
            foreach($tables->result() as $table){
                $fields = $this->db->field_data($table->name);
                foreach($fields as $field){
                    if($field->name=='idiomas'){
                        $values = $this->db->get($table->name);
                        foreach($values->result() as $v){
                            if(!empty($v->idiomas)){
                                $idiomas = json_decode($v->idiomas);
                                foreach($v as $n=>$vv){
                                    if($n!='id' && !is_numeric($vv)){
                                        if(!empty($idiomas->{$lang}->{$n})){
                                            $repl = $idiomas->{$lang}->{$n};
                                            $view = str_replace($vv,$repl,$view);
                                        }
                                    }
                                }
                            }
                        }
                    }
                }
            }
        }*/
        return $view;
    }
}    
