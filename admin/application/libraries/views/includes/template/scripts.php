<!-- JS Plugins -->
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery/dist/jquery.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/tether/dist/js/tether.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/bootstrap/dist/js/bootstrap.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/slick-carousel/slick/slick.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery.appear/jquery.appear.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery.scrollto/jquery.scrollTo.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery.localscroll/jquery.localScroll.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery-validation/dist/jquery.validate.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/jquery.mb.ytplayer/dist/jquery.mb.YTPlayer.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/twitter-fetcher/js/twitterFetcher_min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/skrollr/dist/skrollr.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/plugins/animsition/dist/js/animsition.min.js"></script>
<script src="<?= base_url() ?>theme/theme/assets/js/jquery.isotope.min.js"></script>
<script src="<?= base_url() ?>js/uikit/js/uikit.js"></script>
<!-- JS Core -->
<script src='https://www.google.com/recaptcha/api.js'></script>
<script type="text/javascript" src="https://maps.googleapis.com/maps/api/js?key=AIzaSyBfiIZSk_QC7UWgdGekKZLEUXUBi_mWPnE"></script>
<script src="<?= base_url() ?>theme/theme/assets/js/core.js"></script>
<script type="text/javascript">	
	$(document).ready(function(){
		$(".isotope-grid").isotope({
	    	/*filter:'.all'*/
		});
	});


	var captcha = '';
    function contacto(f){
        if(captcha===''){
            $(".g-recaptcha").show();
            captcha = 1;
        }else{
            var post_data = new FormData(f);           
            //Ajax post data to server
            $.ajax({
                url: URL+'paginas/frontend/contacto',
                data: post_data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response){ 
                    response = JSON.parse(response);                                           
                    $("#result").hide().html(response.text).slideDown();
                }
            })
        }

        return false;
    }

    var captcha2 = '';
    function subscribir(f){
        if(captcha2===''){
            $(".g-recaptcha").show();
            captcha2 = 1;
        }else{
            var post_data = new FormData(f);           
            //Ajax post data to server
            $.ajax({
                url: URL+'paginas/frontend/subscribir',
                data: post_data,
                context: document.body,
                cache: false,
                contentType: false,
                processData: false,
                type: 'POST',
                success:function(response){ 
                    response = JSON.parse(response);                                           
                    $("#result2").hide().html(response.msg).slideDown();
                }
            })
        }

        return false;
    }
	
</script>

