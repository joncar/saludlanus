import { NgModule } from '@angular/core';
import { PreloadAllModules, RouterModule, Routes } from '@angular/router';
let red = typeof(localStorage.user)=='undefined'?'home':'main/mapas';
const routes: Routes = [
  {
    path: '',
    redirectTo: red,
    pathMatch: 'full'
  },
  {
    path: 'home',
    loadChildren: './home/home.module#HomePageModule'
  },
  {
    path: 'registro',
    loadChildren: './registro/registro.module#RegistroPageModule'
  },
  {
    path: 'recover',
    loadChildren: './recover/recover.module#RecoverPageModule'
  },
  {
    path: 'main',
    loadChildren: () => import('./tabs/tabs.module').then(m => m.TabsPageModule),
  }
];

@NgModule({
  imports: [
    RouterModule.forRoot(routes, { preloadingStrategy: PreloadAllModules,
    useHash:true })
  ],
  exports: [RouterModule]
})
export class AppRoutingModule {}
