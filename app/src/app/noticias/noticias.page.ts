import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { ModalController} from '@ionic/angular';
import { NoticiasModal } from './noticias.modal.page';

@Component({
  selector: 'app-noticias',
  templateUrl: 'noticias.page.html',
  styleUrls: ['noticias.page.scss']
})
export class NoticiasPage { 
  listado:[]; 
  constructor(
	  	public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
	    public msj: MessagesService,	    
	    public user: UserService,
	    public modalController: ModalController	    
    ) {
  	this.list();	
  }

  list(){
		let l = this;
		this.api.get('posts/noticias',{},function(response){
			l.listado = response;	
			console.log(l.listado);		
		});
	}
	modal = undefined;
	async showModal(i) {
	    this.modal = await this.modalController.create({
	      component: NoticiasModal,
	      componentProps: {i:i}
	    });
	    let m = this.modal;
	    this.modal.present();
	}
}