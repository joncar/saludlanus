import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { NavController,ModalController,NavParams } from '@ionic/angular';
import { DomSanitizer } from '@angular/platform-browser';
@Component({
  selector: 'app-NoticiasModal',
  templateUrl: './noticias.modal.html',
})
export class NoticiasModal {   
  data:any; 
  constructor(
    public modal : ModalController,
    private navParams: NavParams ,
    public router: Router,   
    public modalController: ModalController, 
    public sanitizer: DomSanitizer   
  ) {    
  	this.data = typeof navParams.get('i') !='undefined'?navParams.get('i'):this.data;
    if(typeof this.data.meta.youtube  != 'undefined' && this.data.meta.youtube!=''){
      this.data.meta.youtube = this.sanitizer.bypassSecurityTrustResourceUrl(this.data.meta.youtube);
    }else{
      this.data.meta.youtube = '';
    }
  }
  
  modale = undefined;
  close(){
        this.modal.dismiss();
  }
}
