import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { ModalController} from '@ionic/angular';
import { PhotoViewer } from '@ionic-native/photo-viewer/ngx';

@Component({
  selector: 'app-consejos',
  templateUrl: 'consejos.page.html',
  styleUrls: ['consejos.page.scss']
})
export class ConsejosPage { 
  listado:[]; 
  constructor(
	  	public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
	    public msj: MessagesService,	    
	    public user: UserService,
	    public modalController: ModalController,
	    private photoViewer: PhotoViewer
    ) {
  	this.list();	
  }

  list(){
		let l = this;
		this.api.get('posts/consejos',{},function(response){
			l.listado = response;			
		});
	}

	showImage(url){
		this.photoViewer.show(url);
	}
}