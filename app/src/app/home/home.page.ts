import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage {
	data = {};
	constructor(
	    public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
	    public msj: MessagesService,
	    public user: UserService
	  ) {
      user.clean();       
	  }

  login(){
    let l = this;    
    this.api.get('login',this.data,function(response){
        if(response.success){
          //Usuario registrado
          l.user.save(response.msj);                    
          l.router.navigateByUrl('main/mapas');
        }else{
          //Mostrar mensaje de error
          l.msj.alert('ERROR',response.msj);
        }
    });
  }


}
