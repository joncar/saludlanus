import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { ModalController} from '@ionic/angular';
declare var google;
@Component({
  selector: 'app-prueba',
  templateUrl: 'prueba.page.html',
  styleUrls: ['prueba.page.scss']
})
export class PruebaPage { 
  datos = <any>{
  	temperatura:37, 
  	correo: this.user.user.email,
  	tos_dolor_garganta:false,
  	respiracion:false,
  	mayor_de_60:false,
  	embarazada:false,
  	cancer:false,
  	diabetes:false,
  	enfermedad_hepatica:false,
  	enfermedad_renal:false,
  	enfermedad_respiratoria:false,
  	enfermedad_cardiologica:false,
  	user_id:this.user.user.id  	
  }; 
  stateButton = 'NOTIFICAR'; 
  constructor(
	  	public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
	    public msj: MessagesService,	    
	    public user: UserService,
	    public modalController: ModalController	    
    ) {
  	this.list();	
  }

  ngAfterViewInit(){
      var input = document.getElementById('field-direccion'); 
      console.log(input);
      var autocomplete = new google.maps.places.Autocomplete(input,{types: ['geocode']});
  }

  list(){
		let l = this;
		let user = this.user.user.id;
		this.api.get('usermeta/'+user,{},function(response){
			for(var i in response){
				l.datos[response[i].meta_key] = response[i].meta_value;
        var bool = parseInt(l.datos[response[i].meta_key]);
        if(!isNaN(bool) && (bool==0 || bool == 1)){
          l.datos[response[i].meta_key] = bool==0?false:true;
        }
			}
		});
	}

  register(){
  	let l = this;
    //this.stateButton = 'CARGANDO...';
    l.msj.confirm('COVID-19','Este formulario tiene carácter de declaración jurada, hacer una falsa declaración puede considerarse una contravención grave',function(){
      l.msj.loading('Registrando su información');
      l.api.get('diagnosticar',l.datos,function(response){
        //l.stateButton = 'NOTIFICAR';
        if(response.success){
          //Usuario registrado
          l.msj.alert('COMPLETADO',response.msj);            
        }else{
          //Mostrar mensaje de error
          l.msj.alert('ERROR',response.msj);      
        }
        l.msj.closeLoading();        
      });
    });    
  }

  addTemperatura(val){
    switch(val){
      case '-':
        this.datos.temperatura = parseInt(this.datos.temperatura)-1;
      break;
      case '+':
        this.datos.temperatura = parseInt(this.datos.temperatura)+1;
      break;
    }
  }
}