import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-registro',
  templateUrl: 'registro.page.html',
  styleUrls: ['registro.page.scss']
})
export class RegistroPage {
  constructor(
    public router: Router, 
    public api: ApiService, 
    public msj: MessagesService,
    public user: UserService
  ) {

  }

  data = {id:null};

  register(){
    let l = this;    
    this.api.get('registro',this.data,function(response){
        if(response.success){
          //Usuario registrado
          l.data.id = response.primary_key;
          l.user.save(l.data);          
          l.router.navigateByUrl('main/mapas');
        }else{
          //Mostrar mensaje de error
          l.msj.alert('ERROR',response.msj);
        }
    });
  }
}
