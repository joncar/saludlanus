import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class AltasService {  
  data = {
  	patronos_id:'0',
  	nombre:'',
  	apellido:'',
  	apellido2:'',
	fecha_nacimiento:'',
	estado_nacimiento:'',
	nro_seg_social:'',
	domicilio:'',
	codigo_postal:'',
	curp:'',
	salario_base_diario:'',
	cant_dias_trabajados:'',
	plan_id:0,
	importe:0,
	intervalo:0,
                  tarjeta_numero:'',
	sexo:'',
                  tipoPago: 0,
                  email:''
  };
  constructor(){}
  clean(){
  	this.data = {
  	patronos_id:'0',
  	nombre:'',
  	apellido:'',
  	apellido2:'',
	fecha_nacimiento:'',
	estado_nacimiento:'',
	nro_seg_social:'',
	domicilio:'',
	codigo_postal:'',
	curp:'',
	salario_base_diario:'',
	cant_dias_trabajados:'',
	plan_id:0,
	importe:0,
	intervalo:0,
                 tarjeta_numero:'',
	sexo:'',
                  tipoPago: 0,
                  email:''
  };
  }
}
