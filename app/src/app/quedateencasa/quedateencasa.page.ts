import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { ModalController} from '@ionic/angular';
@Component({
  selector: 'app-quedateencasa',
  templateUrl: 'quedateencasa.page.html',
  styleUrls: ['quedateencasa.page.scss']
})
export class QuedateEnCasaPage { 
  listado:[]; 
  constructor(
      public router: Router,
      public app: AppComponent,
      public api: ApiService, 
      public msj: MessagesService,      
      public user: UserService,
      public modalController: ModalController      
    ) {
    this.list();  
  }

  list(){
    let l = this;
    this.api.get('posts/quedateencasa',{},function(response){
      l.listado = response;      
    });
  }
}