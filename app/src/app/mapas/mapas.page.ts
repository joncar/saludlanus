import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { Geolocation } from '@ionic-native/geolocation/ngx';
import {
  ToastController,
  Platform,
  LoadingController
} from '@ionic/angular';
import {
  GoogleMaps,
  GoogleMap,
  GoogleMapsEvent,
  Marker,
  GoogleMapsAnimation,
  MyLocation,
  Environment
} from '@ionic-native/google-maps';
declare var google;
@Component({
  selector: 'app-mapas',
  templateUrl: 'mapas.page.html',
  styleUrls: ['mapas.page.scss']
})
export class MapasPage {  
  map: any;
  marker:any;
  loading: any;  
  infowindow:any;
  lat = -34.7021091;
  lng = -58.4252566;
  contenedor = null;  
  constructor(
  	public loadingCtrl: LoadingController,
    public toastCtrl: ToastController,
    private platform: Platform,
    public router: Router,
    public app: AppComponent,
    public api: ApiService, 
    public msj: MessagesService,
    public user: UserService,
    private geolocation: Geolocation
  ) {}
  ngAfterViewInit(){      
      this.loadMap();
  }

  loadMap() {
    this.contenedor = document.getElementById('map_canvas');     
    var cent = new google.maps.LatLng(this.lat,this.lng);    
    var mapOptions = {
        zoom: 6,
        center: cent,
        mapTypeId: google.maps.MapTypeId.ROADMAP
    };        
    this.map = new google.maps.Map(this.contenedor,mapOptions);                            
    this.marker = new google.maps.Marker({
      position: new google.maps.LatLng(this.lat,this.lng),
      map: this.map,       
      title: 'tu posicion'
    });    
    let l = this;
    this.geolocation.getCurrentPosition().then((resp) => {
       l.moverMapa(resp.coords.latitude,resp.coords.longitude);
    }).catch((error) => {
      console.log('Error getting location', error);
    });

    this.infowindow = new google.maps.InfoWindow({
      content: 'Si te encuentras mal te buscaremos en esta dirección para atenderte.'
    });
    let map = this.map;
    let marker = this.marker;
    let infowindow = this.infowindow;
    this.marker.addListener('click', function() {
      infowindow.open(map, marker);
    });
  }

  moverMapa(lat,lng){
    this.api.get('addusermeta',{user_id:this.user.user.id,'ubicacion':(lat+','+lng)},function(data){
      console.log(data);
    });
    
    this.map.panTo(new google.maps.LatLng(lat,lng));
    this.map.setZoom(16);
    this.marker.setPosition(new google.maps.LatLng(lat,lng));
    this.infowindow.open(this.map, this.marker);
  }
}