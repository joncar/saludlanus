import { IonicModule } from '@ionic/angular';
import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { TabsPageRoutingModule } from './tabs-routing.module';

import { TabsPage } from './tabs.page';
import { NoticiasModal } from '../noticias/noticias.modal.page';


@NgModule({  
  entryComponents:[NoticiasModal],
  imports: [
    IonicModule,
    CommonModule,
    FormsModule,
    TabsPageRoutingModule
  ],
  declarations: [NoticiasModal,TabsPage]
})
export class TabsPageModule {}
