import { Component } from '@angular/core';
import { Platform,MenuController } from '@ionic/angular';
import { ApiService } from '../services/api.service';
import { UserService } from '../services/user.service';
import { Router } from '@angular/router';
import { MessagesService } from '../services/messages.service';
import { InAppBrowser } from '@ionic-native/in-app-browser/ngx';
@Component({
  selector: 'app-tabs',
  templateUrl: 'tabs.page.html',
  styleUrls: ['tabs.page.scss']
})
export class TabsPage {
  facebook = '';
  twitter = '';
  instagram = '';
  constructor(
  	public router: Router,
  	public user: UserService,
  	public api: ApiService,
  	private platform: Platform,
  	public msj: MessagesService,
  	private menu: MenuController,
    private iab: InAppBrowser
  ) {
  	this.platform.ready().then(() => {
    if(!user.isLogin()){
	      this.user.clean();
	      this.router.navigateByUrl('home');
	      return;
	    }else{
	      let l = this;
	      var notificationOpenedCallback = function(jsonData) {
	      	 l.getNotif(jsonData.payload.title,jsonData.payload.body);  
	      };
	      var notificacionReceiveCallback = function(jsonData){
	         l.getNotif(jsonData.payload.title,jsonData.payload.body);  	         
	      }
	      if(typeof(window["plugins"])!='undefined'){
	        window["plugins"].OneSignal
	        .startInit("6ab3d0c3-ab46-4db4-b762-a5c9f8b8ae99", "mif-app")
	        .handleNotificationOpened(notificationOpenedCallback)
	        .handleNotificationReceived(notificacionReceiveCallback)
	        .inFocusDisplaying(window["plugins"].OneSignal.OSInFocusDisplayOption.None)
	        .endInit(); 
	        window["plugins"].OneSignal.api = api;
	        window["plugins"].OneSignal.getIds(function(id){
	            window["plugins"].OneSignal.api.get('addusermeta',{'gcm':id.userId,'user_id':user.user.id},function(data){console.log(data);});
	        });
	      }
	    }
	});

    let l = this;
    this.api.get('ajustes',{},function(response){
      l.facebook = response.facebook;      
      l.twitter = response.twitter;      
      l.instagram = response.instagram;      
    });
  }

  getNotif(title,msj){
  	this.msj.alert(title,msj);
  }

  public selectedIndex = 0;
  public appPages = [
    {
      title: 'Facebook',
      url: 'facebook',
      icon: 'logo-facebook',
      color:'#4267b2'
    },
    {
      title: 'Instagram',
      url: 'instagram',
      icon: 'logo-instagram',
      color:'#d82b7d'
    },
    {
      title: 'Twitter',
      url: 'twitter',
      icon: 'logo-twitter',
      color:'#1da1f2'
    },
  ];
  public label = [
    {
      title:'Estadísticas',
      url:'/main/estadisticas',
      icon:'thermometer-outline'
    },
    {
      title: 'Cerrar sesión',
      url: '/home',
      icon: 'lock-closed-outline'
    }    
  ];
  ngOnInit() {
    const path = window.location.pathname.split('folder/')[1];
    if (path !== undefined) {
      this.selectedIndex = this.appPages.findIndex(page => page.title.toLowerCase() === path.toLowerCase());
    }
  }
  openRedSocial(type){
    switch(type){
      case 'facebook':
        this.openFacebook();
      break;
      case 'twitter':
        this.openTwitter();
      break;
      case 'instagram':
        this.openInstagram();
      break;
    }
  }
  openFacebook(){
    const browser = this.iab.create(this.facebook,'_system');
  }
  openTwitter(){
    const browser = this.iab.create(this.twitter,'_system');
  }
  openInstagram(){
    const browser = this.iab.create(this.instagram,'_system');
  }
}
