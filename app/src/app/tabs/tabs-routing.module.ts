import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { TabsPage } from './tabs.page';

const routes: Routes = [
  {
    path: '',
    component: TabsPage,
    children: [
      {
        path: 'mapas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../mapas/mapas.module').then(m => m.MapasPageModule)
          }
        ]
      },
      {
        path: 'noticias',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../noticias/noticias.module').then(m => m.NoticiasPageModule)
          }
        ]
      },
      {
        path: 'consejos',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../consejos/consejos.module').then(m => m.ConsejosPageModule)
          }
        ]
      },
      {
        path: 'prueba',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../prueba/prueba.module').then(m => m.PruebaPageModule)
          }
        ]
      },
      {
        path: 'quedateencasa',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../quedateencasa/quedateencasa.module').then(m => m.QuedateEnCasaPageModule)
          }
        ]
      },
      {
        path: 'estadisticas',
        children: [
          {
            path: '',
            loadChildren: () =>
              import('../estadisticas/estadisticas.module').then(m => m.EstadisticasPageModule)
          }
        ]
      },
      {
        path: '',
        redirectTo: '/main/mapas',
        pathMatch: 'full'
      }
    ]
  },
  {
    path: '',
    redirectTo: '/main/mapas',
    pathMatch: 'full'
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule]
})
export class TabsPageRoutingModule {}
