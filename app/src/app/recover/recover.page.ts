import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
@Component({
  selector: 'app-recover',
  templateUrl: 'recover.page.html',
})
export class RecoverPage {
  constructor(
    public router: Router, 
    public api: ApiService, 
    public msj: MessagesService    
  ) {

  }

  data = {};

  register(){
    let l = this;    
    this.api.get('recover',this.data,function(response){
        if(response.success){
          //Usuario registrado          
          l.msj.alert('SUCCESS',response.msj);
        }else{
          //Mostrar mensaje de error
          l.msj.alert('ERROR',response.msj);
        }
    });
  }
}
