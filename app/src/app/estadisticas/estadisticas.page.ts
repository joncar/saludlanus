import { Component } from '@angular/core';
import { Router } from '@angular/router';
import { AppComponent } from '../app.component';
import { ApiService } from '../services/api.service';
import { MessagesService } from '../services/messages.service';
import { UserService } from '../services/user.service';
import { ModalController} from '@ionic/angular';

@Component({
  selector: 'app-estadisticas',
  templateUrl: 'estadisticas.page.html',
  styleUrls: ['estadisticas.page.scss']
})
export class EstadisticasPage { 
  listado:[]; 
  activo = false;
  constructor(
	  	public router: Router,
	    public app: AppComponent,
	    public api: ApiService, 
	    public msj: MessagesService,	    
	    public user: UserService,
	    public modalController: ModalController,	    
    ) {
  	this.list();	
  }

  list(){
		let l = this;
		this.api.get('estadisticas',{},function(response){
			l.listado = response;	
			l.activo = l.listado.length>0?true:false;		
		});
	}
}